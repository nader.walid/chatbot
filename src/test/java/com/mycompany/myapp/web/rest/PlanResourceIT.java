package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Plan;
import com.mycompany.myapp.repository.PlanRepository;
import com.mycompany.myapp.repository.search.PlanSearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link PlanResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class PlanResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_MAX_USERS = 1L;
    private static final Long UPDATED_MAX_USERS = 2L;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/plans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/plans";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PlanRepository planRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.PlanSearchRepositoryMockConfiguration
     */
    @Autowired
    private PlanSearchRepository mockPlanSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Plan plan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plan createEntity(EntityManager em) {
        Plan plan = new Plan().name(DEFAULT_NAME).maxUsers(DEFAULT_MAX_USERS).createdAt(DEFAULT_CREATED_AT).updatedAt(DEFAULT_UPDATED_AT);
        return plan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plan createUpdatedEntity(EntityManager em) {
        Plan plan = new Plan().name(UPDATED_NAME).maxUsers(UPDATED_MAX_USERS).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);
        return plan;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Plan.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        plan = createEntity(em);
    }

    @Test
    void createPlan() throws Exception {
        int databaseSizeBeforeCreate = planRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockPlanSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Plan
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeCreate + 1);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlan.getMaxUsers()).isEqualTo(DEFAULT_MAX_USERS);
        assertThat(testPlan.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPlan.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(1)).save(testPlan);
    }

    @Test
    void createPlanWithExistingId() throws Exception {
        // Create the Plan with an existing ID
        plan.setId(1L);

        int databaseSizeBeforeCreate = planRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeCreate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = planRepository.findAll().collectList().block().size();
        // set the field null
        plan.setName(null);

        // Create the Plan, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkMaxUsersIsRequired() throws Exception {
        int databaseSizeBeforeTest = planRepository.findAll().collectList().block().size();
        // set the field null
        plan.setMaxUsers(null);

        // Create the Plan, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllPlansAsStream() {
        // Initialize the database
        planRepository.save(plan).block();

        List<Plan> planList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Plan.class)
            .getResponseBody()
            .filter(plan::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(planList).isNotNull();
        assertThat(planList).hasSize(1);
        Plan testPlan = planList.get(0);
        assertThat(testPlan.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlan.getMaxUsers()).isEqualTo(DEFAULT_MAX_USERS);
        assertThat(testPlan.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPlan.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllPlans() {
        // Initialize the database
        planRepository.save(plan).block();

        // Get all the planList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(plan.getId().intValue()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].maxUsers")
            .value(hasItem(DEFAULT_MAX_USERS.intValue()))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getPlan() {
        // Initialize the database
        planRepository.save(plan).block();

        // Get the plan
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, plan.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(plan.getId().intValue()))
            .jsonPath("$.name")
            .value(is(DEFAULT_NAME))
            .jsonPath("$.maxUsers")
            .value(is(DEFAULT_MAX_USERS.intValue()))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingPlan() {
        // Get the plan
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPlan() throws Exception {
        // Configure the mock search repository
        when(mockPlanSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        planRepository.save(plan).block();

        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();

        // Update the plan
        Plan updatedPlan = planRepository.findById(plan.getId()).block();
        updatedPlan.name(UPDATED_NAME).maxUsers(UPDATED_MAX_USERS).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPlan.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPlan))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPlan.getMaxUsers()).isEqualTo(UPDATED_MAX_USERS);
        assertThat(testPlan.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPlan.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository).save(testPlan);
    }

    @Test
    void putNonExistingPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();
        plan.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, plan.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void putWithIdMismatchPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();
        plan.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void putWithMissingIdPathParamPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();
        plan.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void partialUpdatePlanWithPatch() throws Exception {
        // Initialize the database
        planRepository.save(plan).block();

        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();

        // Update the plan using partial update
        Plan partialUpdatedPlan = new Plan();
        partialUpdatedPlan.setId(plan.getId());

        partialUpdatedPlan.maxUsers(UPDATED_MAX_USERS);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPlan.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPlan))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlan.getMaxUsers()).isEqualTo(UPDATED_MAX_USERS);
        assertThat(testPlan.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPlan.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdatePlanWithPatch() throws Exception {
        // Initialize the database
        planRepository.save(plan).block();

        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();

        // Update the plan using partial update
        Plan partialUpdatedPlan = new Plan();
        partialUpdatedPlan.setId(plan.getId());

        partialUpdatedPlan.name(UPDATED_NAME).maxUsers(UPDATED_MAX_USERS).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPlan.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPlan))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPlan.getMaxUsers()).isEqualTo(UPDATED_MAX_USERS);
        assertThat(testPlan.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPlan.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();
        plan.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, plan.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void patchWithIdMismatchPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();
        plan.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void patchWithMissingIdPathParamPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().collectList().block().size();
        plan.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(plan))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(0)).save(plan);
    }

    @Test
    void deletePlan() {
        // Configure the mock search repository
        when(mockPlanSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        planRepository.save(plan).block();

        int databaseSizeBeforeDelete = planRepository.findAll().collectList().block().size();

        // Delete the plan
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, plan.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Plan> planList = planRepository.findAll().collectList().block();
        assertThat(planList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Plan in Elasticsearch
        verify(mockPlanSearchRepository, times(1)).deleteById(plan.getId());
    }

    @Test
    void searchPlan() {
        // Configure the mock search repository
        when(mockPlanSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        planRepository.save(plan).block();
        when(mockPlanSearchRepository.search("id:" + plan.getId())).thenReturn(Flux.just(plan));

        // Search the plan
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + plan.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(plan.getId().intValue()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].maxUsers")
            .value(hasItem(DEFAULT_MAX_USERS.intValue()))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
