package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.CustomerInquiry;
import com.mycompany.myapp.repository.CustomerInquiryRepository;
import com.mycompany.myapp.repository.search.CustomerInquirySearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link CustomerInquiryResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class CustomerInquiryResourceIT {

    private static final String DEFAULT_BODY = "AAAAAAAAAA";
    private static final String UPDATED_BODY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CALL_DAY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CALL_DAY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CALL_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CALL_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CALL_PHONE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CALL_PHONE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_STATUS_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_NOTES = "BBBBBBBBBB";

    private static final Long DEFAULT_NEXT_ACTIVITY = 1L;
    private static final Long UPDATED_NEXT_ACTIVITY = 2L;

    private static final Long DEFAULT_LAST_ACTIVITY = 1L;
    private static final Long UPDATED_LAST_ACTIVITY = 2L;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/customer-inquiries";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/customer-inquiries";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CustomerInquiryRepository customerInquiryRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.CustomerInquirySearchRepositoryMockConfiguration
     */
    @Autowired
    private CustomerInquirySearchRepository mockCustomerInquirySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private CustomerInquiry customerInquiry;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerInquiry createEntity(EntityManager em) {
        CustomerInquiry customerInquiry = new CustomerInquiry()
            .body(DEFAULT_BODY)
            .status(DEFAULT_STATUS)
            .reason(DEFAULT_REASON)
            .callDay(DEFAULT_CALL_DAY)
            .callTime(DEFAULT_CALL_TIME)
            .callPhone(DEFAULT_CALL_PHONE)
            .statusNotes(DEFAULT_STATUS_NOTES)
            .nextActivity(DEFAULT_NEXT_ACTIVITY)
            .lastActivity(DEFAULT_LAST_ACTIVITY)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return customerInquiry;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerInquiry createUpdatedEntity(EntityManager em) {
        CustomerInquiry customerInquiry = new CustomerInquiry()
            .body(UPDATED_BODY)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .callDay(UPDATED_CALL_DAY)
            .callTime(UPDATED_CALL_TIME)
            .callPhone(UPDATED_CALL_PHONE)
            .statusNotes(UPDATED_STATUS_NOTES)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return customerInquiry;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(CustomerInquiry.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        customerInquiry = createEntity(em);
    }

    @Test
    void createCustomerInquiry() throws Exception {
        int databaseSizeBeforeCreate = customerInquiryRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockCustomerInquirySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the CustomerInquiry
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerInquiry testCustomerInquiry = customerInquiryList.get(customerInquiryList.size() - 1);
        assertThat(testCustomerInquiry.getBody()).isEqualTo(DEFAULT_BODY);
        assertThat(testCustomerInquiry.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCustomerInquiry.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testCustomerInquiry.getCallDay()).isEqualTo(DEFAULT_CALL_DAY);
        assertThat(testCustomerInquiry.getCallTime()).isEqualTo(DEFAULT_CALL_TIME);
        assertThat(testCustomerInquiry.getCallPhone()).isEqualTo(DEFAULT_CALL_PHONE);
        assertThat(testCustomerInquiry.getStatusNotes()).isEqualTo(DEFAULT_STATUS_NOTES);
        assertThat(testCustomerInquiry.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testCustomerInquiry.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testCustomerInquiry.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCustomerInquiry.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(1)).save(testCustomerInquiry);
    }

    @Test
    void createCustomerInquiryWithExistingId() throws Exception {
        // Create the CustomerInquiry with an existing ID
        customerInquiry.setId(1L);

        int databaseSizeBeforeCreate = customerInquiryRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeCreate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerInquiryRepository.findAll().collectList().block().size();
        // set the field null
        customerInquiry.setStatus(null);

        // Create the CustomerInquiry, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkReasonIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerInquiryRepository.findAll().collectList().block().size();
        // set the field null
        customerInquiry.setReason(null);

        // Create the CustomerInquiry, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllCustomerInquiriesAsStream() {
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        List<CustomerInquiry> customerInquiryList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(CustomerInquiry.class)
            .getResponseBody()
            .filter(customerInquiry::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(customerInquiryList).isNotNull();
        assertThat(customerInquiryList).hasSize(1);
        CustomerInquiry testCustomerInquiry = customerInquiryList.get(0);
        assertThat(testCustomerInquiry.getBody()).isEqualTo(DEFAULT_BODY);
        assertThat(testCustomerInquiry.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCustomerInquiry.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testCustomerInquiry.getCallDay()).isEqualTo(DEFAULT_CALL_DAY);
        assertThat(testCustomerInquiry.getCallTime()).isEqualTo(DEFAULT_CALL_TIME);
        assertThat(testCustomerInquiry.getCallPhone()).isEqualTo(DEFAULT_CALL_PHONE);
        assertThat(testCustomerInquiry.getStatusNotes()).isEqualTo(DEFAULT_STATUS_NOTES);
        assertThat(testCustomerInquiry.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testCustomerInquiry.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testCustomerInquiry.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCustomerInquiry.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllCustomerInquiries() {
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        // Get all the customerInquiryList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(customerInquiry.getId().intValue()))
            .jsonPath("$.[*].body")
            .value(hasItem(DEFAULT_BODY))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS))
            .jsonPath("$.[*].reason")
            .value(hasItem(DEFAULT_REASON))
            .jsonPath("$.[*].callDay")
            .value(hasItem(sameInstant(DEFAULT_CALL_DAY)))
            .jsonPath("$.[*].callTime")
            .value(hasItem(sameInstant(DEFAULT_CALL_TIME)))
            .jsonPath("$.[*].callPhone")
            .value(hasItem(sameInstant(DEFAULT_CALL_PHONE)))
            .jsonPath("$.[*].statusNotes")
            .value(hasItem(DEFAULT_STATUS_NOTES))
            .jsonPath("$.[*].nextActivity")
            .value(hasItem(DEFAULT_NEXT_ACTIVITY.intValue()))
            .jsonPath("$.[*].lastActivity")
            .value(hasItem(DEFAULT_LAST_ACTIVITY.intValue()))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getCustomerInquiry() {
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        // Get the customerInquiry
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, customerInquiry.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(customerInquiry.getId().intValue()))
            .jsonPath("$.body")
            .value(is(DEFAULT_BODY))
            .jsonPath("$.status")
            .value(is(DEFAULT_STATUS))
            .jsonPath("$.reason")
            .value(is(DEFAULT_REASON))
            .jsonPath("$.callDay")
            .value(is(sameInstant(DEFAULT_CALL_DAY)))
            .jsonPath("$.callTime")
            .value(is(sameInstant(DEFAULT_CALL_TIME)))
            .jsonPath("$.callPhone")
            .value(is(sameInstant(DEFAULT_CALL_PHONE)))
            .jsonPath("$.statusNotes")
            .value(is(DEFAULT_STATUS_NOTES))
            .jsonPath("$.nextActivity")
            .value(is(DEFAULT_NEXT_ACTIVITY.intValue()))
            .jsonPath("$.lastActivity")
            .value(is(DEFAULT_LAST_ACTIVITY.intValue()))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingCustomerInquiry() {
        // Get the customerInquiry
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCustomerInquiry() throws Exception {
        // Configure the mock search repository
        when(mockCustomerInquirySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();

        // Update the customerInquiry
        CustomerInquiry updatedCustomerInquiry = customerInquiryRepository.findById(customerInquiry.getId()).block();
        updatedCustomerInquiry
            .body(UPDATED_BODY)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .callDay(UPDATED_CALL_DAY)
            .callTime(UPDATED_CALL_TIME)
            .callPhone(UPDATED_CALL_PHONE)
            .statusNotes(UPDATED_STATUS_NOTES)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCustomerInquiry.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCustomerInquiry))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);
        CustomerInquiry testCustomerInquiry = customerInquiryList.get(customerInquiryList.size() - 1);
        assertThat(testCustomerInquiry.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testCustomerInquiry.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCustomerInquiry.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testCustomerInquiry.getCallDay()).isEqualTo(UPDATED_CALL_DAY);
        assertThat(testCustomerInquiry.getCallTime()).isEqualTo(UPDATED_CALL_TIME);
        assertThat(testCustomerInquiry.getCallPhone()).isEqualTo(UPDATED_CALL_PHONE);
        assertThat(testCustomerInquiry.getStatusNotes()).isEqualTo(UPDATED_STATUS_NOTES);
        assertThat(testCustomerInquiry.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testCustomerInquiry.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testCustomerInquiry.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCustomerInquiry.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository).save(testCustomerInquiry);
    }

    @Test
    void putNonExistingCustomerInquiry() throws Exception {
        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();
        customerInquiry.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, customerInquiry.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void putWithIdMismatchCustomerInquiry() throws Exception {
        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();
        customerInquiry.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void putWithMissingIdPathParamCustomerInquiry() throws Exception {
        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();
        customerInquiry.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void partialUpdateCustomerInquiryWithPatch() throws Exception {
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();

        // Update the customerInquiry using partial update
        CustomerInquiry partialUpdatedCustomerInquiry = new CustomerInquiry();
        partialUpdatedCustomerInquiry.setId(customerInquiry.getId());

        partialUpdatedCustomerInquiry
            .body(UPDATED_BODY)
            .status(UPDATED_STATUS)
            .callDay(UPDATED_CALL_DAY)
            .callPhone(UPDATED_CALL_PHONE)
            .createdAt(UPDATED_CREATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCustomerInquiry.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerInquiry))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);
        CustomerInquiry testCustomerInquiry = customerInquiryList.get(customerInquiryList.size() - 1);
        assertThat(testCustomerInquiry.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testCustomerInquiry.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCustomerInquiry.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testCustomerInquiry.getCallDay()).isEqualTo(UPDATED_CALL_DAY);
        assertThat(testCustomerInquiry.getCallTime()).isEqualTo(DEFAULT_CALL_TIME);
        assertThat(testCustomerInquiry.getCallPhone()).isEqualTo(UPDATED_CALL_PHONE);
        assertThat(testCustomerInquiry.getStatusNotes()).isEqualTo(DEFAULT_STATUS_NOTES);
        assertThat(testCustomerInquiry.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testCustomerInquiry.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testCustomerInquiry.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCustomerInquiry.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdateCustomerInquiryWithPatch() throws Exception {
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();

        // Update the customerInquiry using partial update
        CustomerInquiry partialUpdatedCustomerInquiry = new CustomerInquiry();
        partialUpdatedCustomerInquiry.setId(customerInquiry.getId());

        partialUpdatedCustomerInquiry
            .body(UPDATED_BODY)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .callDay(UPDATED_CALL_DAY)
            .callTime(UPDATED_CALL_TIME)
            .callPhone(UPDATED_CALL_PHONE)
            .statusNotes(UPDATED_STATUS_NOTES)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCustomerInquiry.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerInquiry))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);
        CustomerInquiry testCustomerInquiry = customerInquiryList.get(customerInquiryList.size() - 1);
        assertThat(testCustomerInquiry.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testCustomerInquiry.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCustomerInquiry.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testCustomerInquiry.getCallDay()).isEqualTo(UPDATED_CALL_DAY);
        assertThat(testCustomerInquiry.getCallTime()).isEqualTo(UPDATED_CALL_TIME);
        assertThat(testCustomerInquiry.getCallPhone()).isEqualTo(UPDATED_CALL_PHONE);
        assertThat(testCustomerInquiry.getStatusNotes()).isEqualTo(UPDATED_STATUS_NOTES);
        assertThat(testCustomerInquiry.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testCustomerInquiry.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testCustomerInquiry.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCustomerInquiry.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingCustomerInquiry() throws Exception {
        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();
        customerInquiry.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, customerInquiry.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void patchWithIdMismatchCustomerInquiry() throws Exception {
        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();
        customerInquiry.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void patchWithMissingIdPathParamCustomerInquiry() throws Exception {
        int databaseSizeBeforeUpdate = customerInquiryRepository.findAll().collectList().block().size();
        customerInquiry.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(customerInquiry))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the CustomerInquiry in the database
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(0)).save(customerInquiry);
    }

    @Test
    void deleteCustomerInquiry() {
        // Configure the mock search repository
        when(mockCustomerInquirySearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();

        int databaseSizeBeforeDelete = customerInquiryRepository.findAll().collectList().block().size();

        // Delete the customerInquiry
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, customerInquiry.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<CustomerInquiry> customerInquiryList = customerInquiryRepository.findAll().collectList().block();
        assertThat(customerInquiryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CustomerInquiry in Elasticsearch
        verify(mockCustomerInquirySearchRepository, times(1)).deleteById(customerInquiry.getId());
    }

    @Test
    void searchCustomerInquiry() {
        // Configure the mock search repository
        when(mockCustomerInquirySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        customerInquiryRepository.save(customerInquiry).block();
        when(mockCustomerInquirySearchRepository.search("id:" + customerInquiry.getId())).thenReturn(Flux.just(customerInquiry));

        // Search the customerInquiry
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + customerInquiry.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(customerInquiry.getId().intValue()))
            .jsonPath("$.[*].body")
            .value(hasItem(DEFAULT_BODY))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS))
            .jsonPath("$.[*].reason")
            .value(hasItem(DEFAULT_REASON))
            .jsonPath("$.[*].callDay")
            .value(hasItem(sameInstant(DEFAULT_CALL_DAY)))
            .jsonPath("$.[*].callTime")
            .value(hasItem(sameInstant(DEFAULT_CALL_TIME)))
            .jsonPath("$.[*].callPhone")
            .value(hasItem(sameInstant(DEFAULT_CALL_PHONE)))
            .jsonPath("$.[*].statusNotes")
            .value(hasItem(DEFAULT_STATUS_NOTES))
            .jsonPath("$.[*].nextActivity")
            .value(hasItem(DEFAULT_NEXT_ACTIVITY.intValue()))
            .jsonPath("$.[*].lastActivity")
            .value(hasItem(DEFAULT_LAST_ACTIVITY.intValue()))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
