package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.PaymentHistory;
import com.mycompany.myapp.repository.PaymentHistoryRepository;
import com.mycompany.myapp.repository.search.PaymentHistorySearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link PaymentHistoryResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class PaymentHistoryResourceIT {

    private static final ZonedDateTime DEFAULT_PLAN_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PLAN_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_PLAN_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PLAN_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_USED_SESSION = 1L;
    private static final Long UPDATED_USED_SESSION = 2L;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/payment-histories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/payment-histories";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PaymentHistoryRepository paymentHistoryRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.PaymentHistorySearchRepositoryMockConfiguration
     */
    @Autowired
    private PaymentHistorySearchRepository mockPaymentHistorySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private PaymentHistory paymentHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentHistory createEntity(EntityManager em) {
        PaymentHistory paymentHistory = new PaymentHistory()
            .planStartDate(DEFAULT_PLAN_START_DATE)
            .planEndDate(DEFAULT_PLAN_END_DATE)
            .usedSession(DEFAULT_USED_SESSION)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return paymentHistory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentHistory createUpdatedEntity(EntityManager em) {
        PaymentHistory paymentHistory = new PaymentHistory()
            .planStartDate(UPDATED_PLAN_START_DATE)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .usedSession(UPDATED_USED_SESSION)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return paymentHistory;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(PaymentHistory.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        paymentHistory = createEntity(em);
    }

    @Test
    void createPaymentHistory() throws Exception {
        int databaseSizeBeforeCreate = paymentHistoryRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockPaymentHistorySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the PaymentHistory
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getPlanStartDate()).isEqualTo(DEFAULT_PLAN_START_DATE);
        assertThat(testPaymentHistory.getPlanEndDate()).isEqualTo(DEFAULT_PLAN_END_DATE);
        assertThat(testPaymentHistory.getUsedSession()).isEqualTo(DEFAULT_USED_SESSION);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPaymentHistory.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(1)).save(testPaymentHistory);
    }

    @Test
    void createPaymentHistoryWithExistingId() throws Exception {
        // Create the PaymentHistory with an existing ID
        paymentHistory.setId(1L);

        int databaseSizeBeforeCreate = paymentHistoryRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeCreate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void checkPlanStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().collectList().block().size();
        // set the field null
        paymentHistory.setPlanStartDate(null);

        // Create the PaymentHistory, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkPlanEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().collectList().block().size();
        // set the field null
        paymentHistory.setPlanEndDate(null);

        // Create the PaymentHistory, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkUsedSessionIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentHistoryRepository.findAll().collectList().block().size();
        // set the field null
        paymentHistory.setUsedSession(null);

        // Create the PaymentHistory, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllPaymentHistoriesAsStream() {
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        List<PaymentHistory> paymentHistoryList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(PaymentHistory.class)
            .getResponseBody()
            .filter(paymentHistory::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(paymentHistoryList).isNotNull();
        assertThat(paymentHistoryList).hasSize(1);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(0);
        assertThat(testPaymentHistory.getPlanStartDate()).isEqualTo(DEFAULT_PLAN_START_DATE);
        assertThat(testPaymentHistory.getPlanEndDate()).isEqualTo(DEFAULT_PLAN_END_DATE);
        assertThat(testPaymentHistory.getUsedSession()).isEqualTo(DEFAULT_USED_SESSION);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPaymentHistory.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllPaymentHistories() {
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        // Get all the paymentHistoryList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(paymentHistory.getId().intValue()))
            .jsonPath("$.[*].planStartDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_START_DATE)))
            .jsonPath("$.[*].planEndDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_END_DATE)))
            .jsonPath("$.[*].usedSession")
            .value(hasItem(DEFAULT_USED_SESSION.intValue()))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getPaymentHistory() {
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        // Get the paymentHistory
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, paymentHistory.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(paymentHistory.getId().intValue()))
            .jsonPath("$.planStartDate")
            .value(is(sameInstant(DEFAULT_PLAN_START_DATE)))
            .jsonPath("$.planEndDate")
            .value(is(sameInstant(DEFAULT_PLAN_END_DATE)))
            .jsonPath("$.usedSession")
            .value(is(DEFAULT_USED_SESSION.intValue()))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingPaymentHistory() {
        // Get the paymentHistory
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPaymentHistory() throws Exception {
        // Configure the mock search repository
        when(mockPaymentHistorySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();

        // Update the paymentHistory
        PaymentHistory updatedPaymentHistory = paymentHistoryRepository.findById(paymentHistory.getId()).block();
        updatedPaymentHistory
            .planStartDate(UPDATED_PLAN_START_DATE)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .usedSession(UPDATED_USED_SESSION)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPaymentHistory.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPaymentHistory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getPlanStartDate()).isEqualTo(UPDATED_PLAN_START_DATE);
        assertThat(testPaymentHistory.getPlanEndDate()).isEqualTo(UPDATED_PLAN_END_DATE);
        assertThat(testPaymentHistory.getUsedSession()).isEqualTo(UPDATED_USED_SESSION);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPaymentHistory.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository).save(testPaymentHistory);
    }

    @Test
    void putNonExistingPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();
        paymentHistory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, paymentHistory.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void putWithIdMismatchPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();
        paymentHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void putWithMissingIdPathParamPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();
        paymentHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void partialUpdatePaymentHistoryWithPatch() throws Exception {
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();

        // Update the paymentHistory using partial update
        PaymentHistory partialUpdatedPaymentHistory = new PaymentHistory();
        partialUpdatedPaymentHistory.setId(paymentHistory.getId());

        partialUpdatedPaymentHistory
            .planEndDate(UPDATED_PLAN_END_DATE)
            .usedSession(UPDATED_USED_SESSION)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPaymentHistory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentHistory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getPlanStartDate()).isEqualTo(DEFAULT_PLAN_START_DATE);
        assertThat(testPaymentHistory.getPlanEndDate()).isEqualTo(UPDATED_PLAN_END_DATE);
        assertThat(testPaymentHistory.getUsedSession()).isEqualTo(UPDATED_USED_SESSION);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPaymentHistory.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void fullUpdatePaymentHistoryWithPatch() throws Exception {
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();

        // Update the paymentHistory using partial update
        PaymentHistory partialUpdatedPaymentHistory = new PaymentHistory();
        partialUpdatedPaymentHistory.setId(paymentHistory.getId());

        partialUpdatedPaymentHistory
            .planStartDate(UPDATED_PLAN_START_DATE)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .usedSession(UPDATED_USED_SESSION)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPaymentHistory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentHistory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);
        PaymentHistory testPaymentHistory = paymentHistoryList.get(paymentHistoryList.size() - 1);
        assertThat(testPaymentHistory.getPlanStartDate()).isEqualTo(UPDATED_PLAN_START_DATE);
        assertThat(testPaymentHistory.getPlanEndDate()).isEqualTo(UPDATED_PLAN_END_DATE);
        assertThat(testPaymentHistory.getUsedSession()).isEqualTo(UPDATED_USED_SESSION);
        assertThat(testPaymentHistory.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPaymentHistory.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();
        paymentHistory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, paymentHistory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void patchWithIdMismatchPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();
        paymentHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void patchWithMissingIdPathParamPaymentHistory() throws Exception {
        int databaseSizeBeforeUpdate = paymentHistoryRepository.findAll().collectList().block().size();
        paymentHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(paymentHistory))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the PaymentHistory in the database
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(0)).save(paymentHistory);
    }

    @Test
    void deletePaymentHistory() {
        // Configure the mock search repository
        when(mockPaymentHistorySearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();

        int databaseSizeBeforeDelete = paymentHistoryRepository.findAll().collectList().block().size();

        // Delete the paymentHistory
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, paymentHistory.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<PaymentHistory> paymentHistoryList = paymentHistoryRepository.findAll().collectList().block();
        assertThat(paymentHistoryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the PaymentHistory in Elasticsearch
        verify(mockPaymentHistorySearchRepository, times(1)).deleteById(paymentHistory.getId());
    }

    @Test
    void searchPaymentHistory() {
        // Configure the mock search repository
        when(mockPaymentHistorySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        paymentHistoryRepository.save(paymentHistory).block();
        when(mockPaymentHistorySearchRepository.search("id:" + paymentHistory.getId())).thenReturn(Flux.just(paymentHistory));

        // Search the paymentHistory
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + paymentHistory.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(paymentHistory.getId().intValue()))
            .jsonPath("$.[*].planStartDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_START_DATE)))
            .jsonPath("$.[*].planEndDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_END_DATE)))
            .jsonPath("$.[*].usedSession")
            .value(hasItem(DEFAULT_USED_SESSION.intValue()))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
