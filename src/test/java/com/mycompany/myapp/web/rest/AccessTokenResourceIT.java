package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.AccessToken;
import com.mycompany.myapp.repository.AccessTokenRepository;
import com.mycompany.myapp.repository.search.AccessTokenSearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link AccessTokenResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class AccessTokenResourceIT {

    private static final String DEFAULT_APP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_APP_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TOKEN_CREATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TOKEN_CREATION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TOKEN_EXPIRE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TOKEN_EXPIRE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_USER_ALIES = "AAAAAAAAAA";
    private static final String UPDATED_USER_ALIES = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/access-tokens";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/access-tokens";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.AccessTokenSearchRepositoryMockConfiguration
     */
    @Autowired
    private AccessTokenSearchRepository mockAccessTokenSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private AccessToken accessToken;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccessToken createEntity(EntityManager em) {
        AccessToken accessToken = new AccessToken()
            .appName(DEFAULT_APP_NAME)
            .token(DEFAULT_TOKEN)
            .tokenCreation(DEFAULT_TOKEN_CREATION)
            .tokenExpire(DEFAULT_TOKEN_EXPIRE)
            .userAlies(DEFAULT_USER_ALIES)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return accessToken;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccessToken createUpdatedEntity(EntityManager em) {
        AccessToken accessToken = new AccessToken()
            .appName(UPDATED_APP_NAME)
            .token(UPDATED_TOKEN)
            .tokenCreation(UPDATED_TOKEN_CREATION)
            .tokenExpire(UPDATED_TOKEN_EXPIRE)
            .userAlies(UPDATED_USER_ALIES)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return accessToken;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(AccessToken.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        accessToken = createEntity(em);
    }

    @Test
    void createAccessToken() throws Exception {
        int databaseSizeBeforeCreate = accessTokenRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockAccessTokenSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the AccessToken
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeCreate + 1);
        AccessToken testAccessToken = accessTokenList.get(accessTokenList.size() - 1);
        assertThat(testAccessToken.getAppName()).isEqualTo(DEFAULT_APP_NAME);
        assertThat(testAccessToken.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testAccessToken.getTokenCreation()).isEqualTo(DEFAULT_TOKEN_CREATION);
        assertThat(testAccessToken.getTokenExpire()).isEqualTo(DEFAULT_TOKEN_EXPIRE);
        assertThat(testAccessToken.getUserAlies()).isEqualTo(DEFAULT_USER_ALIES);
        assertThat(testAccessToken.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAccessToken.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(1)).save(testAccessToken);
    }

    @Test
    void createAccessTokenWithExistingId() throws Exception {
        // Create the AccessToken with an existing ID
        accessToken.setId(1L);

        int databaseSizeBeforeCreate = accessTokenRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeCreate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void checkAppNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = accessTokenRepository.findAll().collectList().block().size();
        // set the field null
        accessToken.setAppName(null);

        // Create the AccessToken, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllAccessTokensAsStream() {
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        List<AccessToken> accessTokenList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(AccessToken.class)
            .getResponseBody()
            .filter(accessToken::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(accessTokenList).isNotNull();
        assertThat(accessTokenList).hasSize(1);
        AccessToken testAccessToken = accessTokenList.get(0);
        assertThat(testAccessToken.getAppName()).isEqualTo(DEFAULT_APP_NAME);
        assertThat(testAccessToken.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testAccessToken.getTokenCreation()).isEqualTo(DEFAULT_TOKEN_CREATION);
        assertThat(testAccessToken.getTokenExpire()).isEqualTo(DEFAULT_TOKEN_EXPIRE);
        assertThat(testAccessToken.getUserAlies()).isEqualTo(DEFAULT_USER_ALIES);
        assertThat(testAccessToken.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAccessToken.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllAccessTokens() {
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        // Get all the accessTokenList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(accessToken.getId().intValue()))
            .jsonPath("$.[*].appName")
            .value(hasItem(DEFAULT_APP_NAME))
            .jsonPath("$.[*].token")
            .value(hasItem(DEFAULT_TOKEN))
            .jsonPath("$.[*].tokenCreation")
            .value(hasItem(sameInstant(DEFAULT_TOKEN_CREATION)))
            .jsonPath("$.[*].tokenExpire")
            .value(hasItem(sameInstant(DEFAULT_TOKEN_EXPIRE)))
            .jsonPath("$.[*].userAlies")
            .value(hasItem(DEFAULT_USER_ALIES))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getAccessToken() {
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        // Get the accessToken
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, accessToken.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(accessToken.getId().intValue()))
            .jsonPath("$.appName")
            .value(is(DEFAULT_APP_NAME))
            .jsonPath("$.token")
            .value(is(DEFAULT_TOKEN))
            .jsonPath("$.tokenCreation")
            .value(is(sameInstant(DEFAULT_TOKEN_CREATION)))
            .jsonPath("$.tokenExpire")
            .value(is(sameInstant(DEFAULT_TOKEN_EXPIRE)))
            .jsonPath("$.userAlies")
            .value(is(DEFAULT_USER_ALIES))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingAccessToken() {
        // Get the accessToken
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewAccessToken() throws Exception {
        // Configure the mock search repository
        when(mockAccessTokenSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();

        // Update the accessToken
        AccessToken updatedAccessToken = accessTokenRepository.findById(accessToken.getId()).block();
        updatedAccessToken
            .appName(UPDATED_APP_NAME)
            .token(UPDATED_TOKEN)
            .tokenCreation(UPDATED_TOKEN_CREATION)
            .tokenExpire(UPDATED_TOKEN_EXPIRE)
            .userAlies(UPDATED_USER_ALIES)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedAccessToken.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedAccessToken))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);
        AccessToken testAccessToken = accessTokenList.get(accessTokenList.size() - 1);
        assertThat(testAccessToken.getAppName()).isEqualTo(UPDATED_APP_NAME);
        assertThat(testAccessToken.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testAccessToken.getTokenCreation()).isEqualTo(UPDATED_TOKEN_CREATION);
        assertThat(testAccessToken.getTokenExpire()).isEqualTo(UPDATED_TOKEN_EXPIRE);
        assertThat(testAccessToken.getUserAlies()).isEqualTo(UPDATED_USER_ALIES);
        assertThat(testAccessToken.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAccessToken.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository).save(testAccessToken);
    }

    @Test
    void putNonExistingAccessToken() throws Exception {
        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();
        accessToken.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, accessToken.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void putWithIdMismatchAccessToken() throws Exception {
        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();
        accessToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void putWithMissingIdPathParamAccessToken() throws Exception {
        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();
        accessToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void partialUpdateAccessTokenWithPatch() throws Exception {
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();

        // Update the accessToken using partial update
        AccessToken partialUpdatedAccessToken = new AccessToken();
        partialUpdatedAccessToken.setId(accessToken.getId());

        partialUpdatedAccessToken
            .appName(UPDATED_APP_NAME)
            .token(UPDATED_TOKEN)
            .tokenExpire(UPDATED_TOKEN_EXPIRE)
            .userAlies(UPDATED_USER_ALIES);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAccessToken.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAccessToken))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);
        AccessToken testAccessToken = accessTokenList.get(accessTokenList.size() - 1);
        assertThat(testAccessToken.getAppName()).isEqualTo(UPDATED_APP_NAME);
        assertThat(testAccessToken.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testAccessToken.getTokenCreation()).isEqualTo(DEFAULT_TOKEN_CREATION);
        assertThat(testAccessToken.getTokenExpire()).isEqualTo(UPDATED_TOKEN_EXPIRE);
        assertThat(testAccessToken.getUserAlies()).isEqualTo(UPDATED_USER_ALIES);
        assertThat(testAccessToken.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAccessToken.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdateAccessTokenWithPatch() throws Exception {
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();

        // Update the accessToken using partial update
        AccessToken partialUpdatedAccessToken = new AccessToken();
        partialUpdatedAccessToken.setId(accessToken.getId());

        partialUpdatedAccessToken
            .appName(UPDATED_APP_NAME)
            .token(UPDATED_TOKEN)
            .tokenCreation(UPDATED_TOKEN_CREATION)
            .tokenExpire(UPDATED_TOKEN_EXPIRE)
            .userAlies(UPDATED_USER_ALIES)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedAccessToken.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedAccessToken))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);
        AccessToken testAccessToken = accessTokenList.get(accessTokenList.size() - 1);
        assertThat(testAccessToken.getAppName()).isEqualTo(UPDATED_APP_NAME);
        assertThat(testAccessToken.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testAccessToken.getTokenCreation()).isEqualTo(UPDATED_TOKEN_CREATION);
        assertThat(testAccessToken.getTokenExpire()).isEqualTo(UPDATED_TOKEN_EXPIRE);
        assertThat(testAccessToken.getUserAlies()).isEqualTo(UPDATED_USER_ALIES);
        assertThat(testAccessToken.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAccessToken.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingAccessToken() throws Exception {
        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();
        accessToken.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, accessToken.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void patchWithIdMismatchAccessToken() throws Exception {
        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();
        accessToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void patchWithMissingIdPathParamAccessToken() throws Exception {
        int databaseSizeBeforeUpdate = accessTokenRepository.findAll().collectList().block().size();
        accessToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(accessToken))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the AccessToken in the database
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(0)).save(accessToken);
    }

    @Test
    void deleteAccessToken() {
        // Configure the mock search repository
        when(mockAccessTokenSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        accessTokenRepository.save(accessToken).block();

        int databaseSizeBeforeDelete = accessTokenRepository.findAll().collectList().block().size();

        // Delete the accessToken
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, accessToken.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<AccessToken> accessTokenList = accessTokenRepository.findAll().collectList().block();
        assertThat(accessTokenList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the AccessToken in Elasticsearch
        verify(mockAccessTokenSearchRepository, times(1)).deleteById(accessToken.getId());
    }

    @Test
    void searchAccessToken() {
        // Configure the mock search repository
        when(mockAccessTokenSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        accessTokenRepository.save(accessToken).block();
        when(mockAccessTokenSearchRepository.search("id:" + accessToken.getId())).thenReturn(Flux.just(accessToken));

        // Search the accessToken
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + accessToken.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(accessToken.getId().intValue()))
            .jsonPath("$.[*].appName")
            .value(hasItem(DEFAULT_APP_NAME))
            .jsonPath("$.[*].token")
            .value(hasItem(DEFAULT_TOKEN))
            .jsonPath("$.[*].tokenCreation")
            .value(hasItem(sameInstant(DEFAULT_TOKEN_CREATION)))
            .jsonPath("$.[*].tokenExpire")
            .value(hasItem(sameInstant(DEFAULT_TOKEN_EXPIRE)))
            .jsonPath("$.[*].userAlies")
            .value(hasItem(DEFAULT_USER_ALIES))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
