package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.SettingOptions;
import com.mycompany.myapp.repository.SettingOptionsRepository;
import com.mycompany.myapp.repository.search.SettingOptionsSearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link SettingOptionsResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class SettingOptionsResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/setting-options";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/setting-options";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SettingOptionsRepository settingOptionsRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.SettingOptionsSearchRepositoryMockConfiguration
     */
    @Autowired
    private SettingOptionsSearchRepository mockSettingOptionsSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private SettingOptions settingOptions;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SettingOptions createEntity(EntityManager em) {
        SettingOptions settingOptions = new SettingOptions().name(DEFAULT_NAME).createdAt(DEFAULT_CREATED_AT).updatedAt(DEFAULT_UPDATED_AT);
        return settingOptions;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SettingOptions createUpdatedEntity(EntityManager em) {
        SettingOptions settingOptions = new SettingOptions().name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);
        return settingOptions;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(SettingOptions.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        settingOptions = createEntity(em);
    }

    @Test
    void createSettingOptions() throws Exception {
        int databaseSizeBeforeCreate = settingOptionsRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockSettingOptionsSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the SettingOptions
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeCreate + 1);
        SettingOptions testSettingOptions = settingOptionsList.get(settingOptionsList.size() - 1);
        assertThat(testSettingOptions.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSettingOptions.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testSettingOptions.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(1)).save(testSettingOptions);
    }

    @Test
    void createSettingOptionsWithExistingId() throws Exception {
        // Create the SettingOptions with an existing ID
        settingOptions.setId(1L);

        int databaseSizeBeforeCreate = settingOptionsRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeCreate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = settingOptionsRepository.findAll().collectList().block().size();
        // set the field null
        settingOptions.setName(null);

        // Create the SettingOptions, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllSettingOptionsAsStream() {
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        List<SettingOptions> settingOptionsList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(SettingOptions.class)
            .getResponseBody()
            .filter(settingOptions::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(settingOptionsList).isNotNull();
        assertThat(settingOptionsList).hasSize(1);
        SettingOptions testSettingOptions = settingOptionsList.get(0);
        assertThat(testSettingOptions.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSettingOptions.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testSettingOptions.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllSettingOptions() {
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        // Get all the settingOptionsList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(settingOptions.getId().intValue()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getSettingOptions() {
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        // Get the settingOptions
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, settingOptions.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(settingOptions.getId().intValue()))
            .jsonPath("$.name")
            .value(is(DEFAULT_NAME))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingSettingOptions() {
        // Get the settingOptions
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewSettingOptions() throws Exception {
        // Configure the mock search repository
        when(mockSettingOptionsSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();

        // Update the settingOptions
        SettingOptions updatedSettingOptions = settingOptionsRepository.findById(settingOptions.getId()).block();
        updatedSettingOptions.name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedSettingOptions.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedSettingOptions))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);
        SettingOptions testSettingOptions = settingOptionsList.get(settingOptionsList.size() - 1);
        assertThat(testSettingOptions.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSettingOptions.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testSettingOptions.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository).save(testSettingOptions);
    }

    @Test
    void putNonExistingSettingOptions() throws Exception {
        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();
        settingOptions.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, settingOptions.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void putWithIdMismatchSettingOptions() throws Exception {
        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();
        settingOptions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void putWithMissingIdPathParamSettingOptions() throws Exception {
        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();
        settingOptions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void partialUpdateSettingOptionsWithPatch() throws Exception {
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();

        // Update the settingOptions using partial update
        SettingOptions partialUpdatedSettingOptions = new SettingOptions();
        partialUpdatedSettingOptions.setId(settingOptions.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedSettingOptions.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedSettingOptions))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);
        SettingOptions testSettingOptions = settingOptionsList.get(settingOptionsList.size() - 1);
        assertThat(testSettingOptions.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSettingOptions.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testSettingOptions.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdateSettingOptionsWithPatch() throws Exception {
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();

        // Update the settingOptions using partial update
        SettingOptions partialUpdatedSettingOptions = new SettingOptions();
        partialUpdatedSettingOptions.setId(settingOptions.getId());

        partialUpdatedSettingOptions.name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedSettingOptions.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedSettingOptions))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);
        SettingOptions testSettingOptions = settingOptionsList.get(settingOptionsList.size() - 1);
        assertThat(testSettingOptions.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSettingOptions.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testSettingOptions.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingSettingOptions() throws Exception {
        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();
        settingOptions.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, settingOptions.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void patchWithIdMismatchSettingOptions() throws Exception {
        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();
        settingOptions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void patchWithMissingIdPathParamSettingOptions() throws Exception {
        int databaseSizeBeforeUpdate = settingOptionsRepository.findAll().collectList().block().size();
        settingOptions.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(settingOptions))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the SettingOptions in the database
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(0)).save(settingOptions);
    }

    @Test
    void deleteSettingOptions() {
        // Configure the mock search repository
        when(mockSettingOptionsSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();

        int databaseSizeBeforeDelete = settingOptionsRepository.findAll().collectList().block().size();

        // Delete the settingOptions
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, settingOptions.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<SettingOptions> settingOptionsList = settingOptionsRepository.findAll().collectList().block();
        assertThat(settingOptionsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the SettingOptions in Elasticsearch
        verify(mockSettingOptionsSearchRepository, times(1)).deleteById(settingOptions.getId());
    }

    @Test
    void searchSettingOptions() {
        // Configure the mock search repository
        when(mockSettingOptionsSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        settingOptionsRepository.save(settingOptions).block();
        when(mockSettingOptionsSearchRepository.search("id:" + settingOptions.getId())).thenReturn(Flux.just(settingOptions));

        // Search the settingOptions
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + settingOptions.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(settingOptions.getId().intValue()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
