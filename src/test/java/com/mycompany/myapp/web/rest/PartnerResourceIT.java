package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Partner;
import com.mycompany.myapp.repository.PartnerRepository;
import com.mycompany.myapp.repository.search.PartnerSearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link PartnerResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class PartnerResourceIT {

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final ZonedDateTime DEFAULT_NEXT_ACTIVITY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_NEXT_ACTIVITY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_LAST_ACTIVITY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_ACTIVITY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/partners";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/partners";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PartnerRepository partnerRepository;

    @Mock
    private PartnerRepository partnerRepositoryMock;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.PartnerSearchRepositoryMockConfiguration
     */
    @Autowired
    private PartnerSearchRepository mockPartnerSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Partner partner;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partner createEntity(EntityManager em) {
        Partner partner = new Partner()
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .nextActivity(DEFAULT_NEXT_ACTIVITY)
            .lastActivity(DEFAULT_LAST_ACTIVITY)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return partner;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partner createUpdatedEntity(EntityManager em) {
        Partner partner = new Partner()
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return partner;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll("rel_partner__company").block();
            em.deleteAll(Partner.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        partner = createEntity(em);
    }

    @Test
    void createPartner() throws Exception {
        int databaseSizeBeforeCreate = partnerRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockPartnerSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Partner
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeCreate + 1);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testPartner.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testPartner.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testPartner.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testPartner.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPartner.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(1)).save(testPartner);
    }

    @Test
    void createPartnerWithExistingId() throws Exception {
        // Create the Partner with an existing ID
        partner.setId(1L);

        int databaseSizeBeforeCreate = partnerRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeCreate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void getAllPartnersAsStream() {
        // Initialize the database
        partnerRepository.save(partner).block();

        List<Partner> partnerList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Partner.class)
            .getResponseBody()
            .filter(partner::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(partnerList).isNotNull();
        assertThat(partnerList).hasSize(1);
        Partner testPartner = partnerList.get(0);
        assertThat(testPartner.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testPartner.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testPartner.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testPartner.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testPartner.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPartner.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllPartners() {
        // Initialize the database
        partnerRepository.save(partner).block();

        // Get all the partnerList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(partner.getId().intValue()))
            .jsonPath("$.[*].imageContentType")
            .value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE))
            .jsonPath("$.[*].image")
            .value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .jsonPath("$.[*].nextActivity")
            .value(hasItem(sameInstant(DEFAULT_NEXT_ACTIVITY)))
            .jsonPath("$.[*].lastActivity")
            .value(hasItem(sameInstant(DEFAULT_LAST_ACTIVITY)))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPartnersWithEagerRelationshipsIsEnabled() {
        when(partnerRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(partnerRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPartnersWithEagerRelationshipsIsNotEnabled() {
        when(partnerRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(partnerRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getPartner() {
        // Initialize the database
        partnerRepository.save(partner).block();

        // Get the partner
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, partner.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(partner.getId().intValue()))
            .jsonPath("$.imageContentType")
            .value(is(DEFAULT_IMAGE_CONTENT_TYPE))
            .jsonPath("$.image")
            .value(is(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .jsonPath("$.nextActivity")
            .value(is(sameInstant(DEFAULT_NEXT_ACTIVITY)))
            .jsonPath("$.lastActivity")
            .value(is(sameInstant(DEFAULT_LAST_ACTIVITY)))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingPartner() {
        // Get the partner
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPartner() throws Exception {
        // Configure the mock search repository
        when(mockPartnerSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        partnerRepository.save(partner).block();

        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();

        // Update the partner
        Partner updatedPartner = partnerRepository.findById(partner.getId()).block();
        updatedPartner
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPartner.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPartner))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testPartner.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testPartner.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testPartner.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testPartner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPartner.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository).save(testPartner);
    }

    @Test
    void putNonExistingPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();
        partner.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, partner.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void putWithIdMismatchPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();
        partner.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void putWithMissingIdPathParamPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();
        partner.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void partialUpdatePartnerWithPatch() throws Exception {
        // Initialize the database
        partnerRepository.save(partner).block();

        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();

        // Update the partner using partial update
        Partner partialUpdatedPartner = new Partner();
        partialUpdatedPartner.setId(partner.getId());

        partialUpdatedPartner.nextActivity(UPDATED_NEXT_ACTIVITY).lastActivity(UPDATED_LAST_ACTIVITY).createdAt(UPDATED_CREATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPartner.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPartner))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testPartner.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testPartner.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testPartner.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testPartner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPartner.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdatePartnerWithPatch() throws Exception {
        // Initialize the database
        partnerRepository.save(partner).block();

        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();

        // Update the partner using partial update
        Partner partialUpdatedPartner = new Partner();
        partialUpdatedPartner.setId(partner.getId());

        partialUpdatedPartner
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPartner.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPartner))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testPartner.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testPartner.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testPartner.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testPartner.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPartner.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();
        partner.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partner.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void patchWithIdMismatchPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();
        partner.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void patchWithMissingIdPathParamPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().collectList().block().size();
        partner.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partner))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(0)).save(partner);
    }

    @Test
    void deletePartner() {
        // Configure the mock search repository
        when(mockPartnerSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        partnerRepository.save(partner).block();

        int databaseSizeBeforeDelete = partnerRepository.findAll().collectList().block().size();

        // Delete the partner
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, partner.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Partner> partnerList = partnerRepository.findAll().collectList().block();
        assertThat(partnerList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Partner in Elasticsearch
        verify(mockPartnerSearchRepository, times(1)).deleteById(partner.getId());
    }

    @Test
    void searchPartner() {
        // Configure the mock search repository
        when(mockPartnerSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        partnerRepository.save(partner).block();
        when(mockPartnerSearchRepository.search("id:" + partner.getId())).thenReturn(Flux.just(partner));

        // Search the partner
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + partner.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(partner.getId().intValue()))
            .jsonPath("$.[*].imageContentType")
            .value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE))
            .jsonPath("$.[*].image")
            .value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .jsonPath("$.[*].nextActivity")
            .value(hasItem(sameInstant(DEFAULT_NEXT_ACTIVITY)))
            .jsonPath("$.[*].lastActivity")
            .value(hasItem(sameInstant(DEFAULT_LAST_ACTIVITY)))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
