package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Channel;
import com.mycompany.myapp.repository.ChannelRepository;
import com.mycompany.myapp.repository.search.ChannelSearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link ChannelResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class ChannelResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/channels";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/channels";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChannelRepository channelRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.ChannelSearchRepositoryMockConfiguration
     */
    @Autowired
    private ChannelSearchRepository mockChannelSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Channel channel;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Channel createEntity(EntityManager em) {
        Channel channel = new Channel().name(DEFAULT_NAME).createdAt(DEFAULT_CREATED_AT).updatedAt(DEFAULT_UPDATED_AT);
        return channel;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Channel createUpdatedEntity(EntityManager em) {
        Channel channel = new Channel().name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);
        return channel;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Channel.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        channel = createEntity(em);
    }

    @Test
    void createChannel() throws Exception {
        int databaseSizeBeforeCreate = channelRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockChannelSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Channel
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeCreate + 1);
        Channel testChannel = channelList.get(channelList.size() - 1);
        assertThat(testChannel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testChannel.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testChannel.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(1)).save(testChannel);
    }

    @Test
    void createChannelWithExistingId() throws Exception {
        // Create the Channel with an existing ID
        channel.setId(1L);

        int databaseSizeBeforeCreate = channelRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeCreate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = channelRepository.findAll().collectList().block().size();
        // set the field null
        channel.setName(null);

        // Create the Channel, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllChannelsAsStream() {
        // Initialize the database
        channelRepository.save(channel).block();

        List<Channel> channelList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Channel.class)
            .getResponseBody()
            .filter(channel::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(channelList).isNotNull();
        assertThat(channelList).hasSize(1);
        Channel testChannel = channelList.get(0);
        assertThat(testChannel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testChannel.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testChannel.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllChannels() {
        // Initialize the database
        channelRepository.save(channel).block();

        // Get all the channelList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(channel.getId().intValue()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getChannel() {
        // Initialize the database
        channelRepository.save(channel).block();

        // Get the channel
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, channel.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(channel.getId().intValue()))
            .jsonPath("$.name")
            .value(is(DEFAULT_NAME))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingChannel() {
        // Get the channel
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewChannel() throws Exception {
        // Configure the mock search repository
        when(mockChannelSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        channelRepository.save(channel).block();

        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();

        // Update the channel
        Channel updatedChannel = channelRepository.findById(channel.getId()).block();
        updatedChannel.name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedChannel.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedChannel))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);
        Channel testChannel = channelList.get(channelList.size() - 1);
        assertThat(testChannel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChannel.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testChannel.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository).save(testChannel);
    }

    @Test
    void putNonExistingChannel() throws Exception {
        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();
        channel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, channel.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void putWithIdMismatchChannel() throws Exception {
        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();
        channel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void putWithMissingIdPathParamChannel() throws Exception {
        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();
        channel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void partialUpdateChannelWithPatch() throws Exception {
        // Initialize the database
        channelRepository.save(channel).block();

        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();

        // Update the channel using partial update
        Channel partialUpdatedChannel = new Channel();
        partialUpdatedChannel.setId(channel.getId());

        partialUpdatedChannel.name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedChannel.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedChannel))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);
        Channel testChannel = channelList.get(channelList.size() - 1);
        assertThat(testChannel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChannel.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testChannel.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdateChannelWithPatch() throws Exception {
        // Initialize the database
        channelRepository.save(channel).block();

        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();

        // Update the channel using partial update
        Channel partialUpdatedChannel = new Channel();
        partialUpdatedChannel.setId(channel.getId());

        partialUpdatedChannel.name(UPDATED_NAME).createdAt(UPDATED_CREATED_AT).updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedChannel.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedChannel))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);
        Channel testChannel = channelList.get(channelList.size() - 1);
        assertThat(testChannel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChannel.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testChannel.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingChannel() throws Exception {
        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();
        channel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, channel.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void patchWithIdMismatchChannel() throws Exception {
        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();
        channel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void patchWithMissingIdPathParamChannel() throws Exception {
        int databaseSizeBeforeUpdate = channelRepository.findAll().collectList().block().size();
        channel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(channel))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Channel in the database
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(0)).save(channel);
    }

    @Test
    void deleteChannel() {
        // Configure the mock search repository
        when(mockChannelSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        channelRepository.save(channel).block();

        int databaseSizeBeforeDelete = channelRepository.findAll().collectList().block().size();

        // Delete the channel
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, channel.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Channel> channelList = channelRepository.findAll().collectList().block();
        assertThat(channelList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Channel in Elasticsearch
        verify(mockChannelSearchRepository, times(1)).deleteById(channel.getId());
    }

    @Test
    void searchChannel() {
        // Configure the mock search repository
        when(mockChannelSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        channelRepository.save(channel).block();
        when(mockChannelSearchRepository.search("id:" + channel.getId())).thenReturn(Flux.just(channel));

        // Search the channel
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + channel.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(channel.getId().intValue()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
