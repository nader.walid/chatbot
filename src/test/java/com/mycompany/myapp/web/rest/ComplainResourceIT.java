package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Complain;
import com.mycompany.myapp.repository.ComplainRepository;
import com.mycompany.myapp.repository.search.ComplainSearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link ComplainResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class ComplainResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_DATA = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_CALL_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_CALL_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_SALES_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_SALES_NOTES = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_NEXT_ACTIVITY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_NEXT_ACTIVITY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_LAST_ACTIVITY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_ACTIVITY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/complains";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/complains";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ComplainRepository complainRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.ComplainSearchRepositoryMockConfiguration
     */
    @Autowired
    private ComplainSearchRepository mockComplainSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Complain complain;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Complain createEntity(EntityManager em) {
        Complain complain = new Complain()
            .title(DEFAULT_TITLE)
            .data(DEFAULT_DATA)
            .status(DEFAULT_STATUS)
            .callPhone(DEFAULT_CALL_PHONE)
            .salesNotes(DEFAULT_SALES_NOTES)
            .nextActivity(DEFAULT_NEXT_ACTIVITY)
            .lastActivity(DEFAULT_LAST_ACTIVITY)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return complain;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Complain createUpdatedEntity(EntityManager em) {
        Complain complain = new Complain()
            .title(UPDATED_TITLE)
            .data(UPDATED_DATA)
            .status(UPDATED_STATUS)
            .callPhone(UPDATED_CALL_PHONE)
            .salesNotes(UPDATED_SALES_NOTES)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return complain;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Complain.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        complain = createEntity(em);
    }

    @Test
    void createComplain() throws Exception {
        int databaseSizeBeforeCreate = complainRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockComplainSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Complain
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeCreate + 1);
        Complain testComplain = complainList.get(complainList.size() - 1);
        assertThat(testComplain.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testComplain.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testComplain.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testComplain.getCallPhone()).isEqualTo(DEFAULT_CALL_PHONE);
        assertThat(testComplain.getSalesNotes()).isEqualTo(DEFAULT_SALES_NOTES);
        assertThat(testComplain.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testComplain.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testComplain.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testComplain.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(1)).save(testComplain);
    }

    @Test
    void createComplainWithExistingId() throws Exception {
        // Create the Complain with an existing ID
        complain.setId(1L);

        int databaseSizeBeforeCreate = complainRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeCreate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = complainRepository.findAll().collectList().block().size();
        // set the field null
        complain.setTitle(null);

        // Create the Complain, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = complainRepository.findAll().collectList().block().size();
        // set the field null
        complain.setStatus(null);

        // Create the Complain, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllComplainsAsStream() {
        // Initialize the database
        complainRepository.save(complain).block();

        List<Complain> complainList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Complain.class)
            .getResponseBody()
            .filter(complain::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(complainList).isNotNull();
        assertThat(complainList).hasSize(1);
        Complain testComplain = complainList.get(0);
        assertThat(testComplain.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testComplain.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testComplain.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testComplain.getCallPhone()).isEqualTo(DEFAULT_CALL_PHONE);
        assertThat(testComplain.getSalesNotes()).isEqualTo(DEFAULT_SALES_NOTES);
        assertThat(testComplain.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testComplain.getLastActivity()).isEqualTo(DEFAULT_LAST_ACTIVITY);
        assertThat(testComplain.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testComplain.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllComplains() {
        // Initialize the database
        complainRepository.save(complain).block();

        // Get all the complainList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(complain.getId().intValue()))
            .jsonPath("$.[*].title")
            .value(hasItem(DEFAULT_TITLE))
            .jsonPath("$.[*].data")
            .value(hasItem(DEFAULT_DATA))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS))
            .jsonPath("$.[*].callPhone")
            .value(hasItem(DEFAULT_CALL_PHONE))
            .jsonPath("$.[*].salesNotes")
            .value(hasItem(DEFAULT_SALES_NOTES))
            .jsonPath("$.[*].nextActivity")
            .value(hasItem(sameInstant(DEFAULT_NEXT_ACTIVITY)))
            .jsonPath("$.[*].lastActivity")
            .value(hasItem(sameInstant(DEFAULT_LAST_ACTIVITY)))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getComplain() {
        // Initialize the database
        complainRepository.save(complain).block();

        // Get the complain
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, complain.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(complain.getId().intValue()))
            .jsonPath("$.title")
            .value(is(DEFAULT_TITLE))
            .jsonPath("$.data")
            .value(is(DEFAULT_DATA))
            .jsonPath("$.status")
            .value(is(DEFAULT_STATUS))
            .jsonPath("$.callPhone")
            .value(is(DEFAULT_CALL_PHONE))
            .jsonPath("$.salesNotes")
            .value(is(DEFAULT_SALES_NOTES))
            .jsonPath("$.nextActivity")
            .value(is(sameInstant(DEFAULT_NEXT_ACTIVITY)))
            .jsonPath("$.lastActivity")
            .value(is(sameInstant(DEFAULT_LAST_ACTIVITY)))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingComplain() {
        // Get the complain
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewComplain() throws Exception {
        // Configure the mock search repository
        when(mockComplainSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        complainRepository.save(complain).block();

        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();

        // Update the complain
        Complain updatedComplain = complainRepository.findById(complain.getId()).block();
        updatedComplain
            .title(UPDATED_TITLE)
            .data(UPDATED_DATA)
            .status(UPDATED_STATUS)
            .callPhone(UPDATED_CALL_PHONE)
            .salesNotes(UPDATED_SALES_NOTES)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedComplain.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedComplain))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);
        Complain testComplain = complainList.get(complainList.size() - 1);
        assertThat(testComplain.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testComplain.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testComplain.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testComplain.getCallPhone()).isEqualTo(UPDATED_CALL_PHONE);
        assertThat(testComplain.getSalesNotes()).isEqualTo(UPDATED_SALES_NOTES);
        assertThat(testComplain.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testComplain.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testComplain.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testComplain.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository).save(testComplain);
    }

    @Test
    void putNonExistingComplain() throws Exception {
        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();
        complain.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, complain.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void putWithIdMismatchComplain() throws Exception {
        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();
        complain.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void putWithMissingIdPathParamComplain() throws Exception {
        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();
        complain.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void partialUpdateComplainWithPatch() throws Exception {
        // Initialize the database
        complainRepository.save(complain).block();

        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();

        // Update the complain using partial update
        Complain partialUpdatedComplain = new Complain();
        partialUpdatedComplain.setId(complain.getId());

        partialUpdatedComplain
            .title(UPDATED_TITLE)
            .data(UPDATED_DATA)
            .status(UPDATED_STATUS)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedComplain.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedComplain))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);
        Complain testComplain = complainList.get(complainList.size() - 1);
        assertThat(testComplain.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testComplain.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testComplain.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testComplain.getCallPhone()).isEqualTo(DEFAULT_CALL_PHONE);
        assertThat(testComplain.getSalesNotes()).isEqualTo(DEFAULT_SALES_NOTES);
        assertThat(testComplain.getNextActivity()).isEqualTo(DEFAULT_NEXT_ACTIVITY);
        assertThat(testComplain.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testComplain.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testComplain.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void fullUpdateComplainWithPatch() throws Exception {
        // Initialize the database
        complainRepository.save(complain).block();

        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();

        // Update the complain using partial update
        Complain partialUpdatedComplain = new Complain();
        partialUpdatedComplain.setId(complain.getId());

        partialUpdatedComplain
            .title(UPDATED_TITLE)
            .data(UPDATED_DATA)
            .status(UPDATED_STATUS)
            .callPhone(UPDATED_CALL_PHONE)
            .salesNotes(UPDATED_SALES_NOTES)
            .nextActivity(UPDATED_NEXT_ACTIVITY)
            .lastActivity(UPDATED_LAST_ACTIVITY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedComplain.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedComplain))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);
        Complain testComplain = complainList.get(complainList.size() - 1);
        assertThat(testComplain.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testComplain.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testComplain.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testComplain.getCallPhone()).isEqualTo(UPDATED_CALL_PHONE);
        assertThat(testComplain.getSalesNotes()).isEqualTo(UPDATED_SALES_NOTES);
        assertThat(testComplain.getNextActivity()).isEqualTo(UPDATED_NEXT_ACTIVITY);
        assertThat(testComplain.getLastActivity()).isEqualTo(UPDATED_LAST_ACTIVITY);
        assertThat(testComplain.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testComplain.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingComplain() throws Exception {
        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();
        complain.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, complain.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void patchWithIdMismatchComplain() throws Exception {
        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();
        complain.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void patchWithMissingIdPathParamComplain() throws Exception {
        int databaseSizeBeforeUpdate = complainRepository.findAll().collectList().block().size();
        complain.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(complain))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Complain in the database
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(0)).save(complain);
    }

    @Test
    void deleteComplain() {
        // Configure the mock search repository
        when(mockComplainSearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        complainRepository.save(complain).block();

        int databaseSizeBeforeDelete = complainRepository.findAll().collectList().block().size();

        // Delete the complain
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, complain.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Complain> complainList = complainRepository.findAll().collectList().block();
        assertThat(complainList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Complain in Elasticsearch
        verify(mockComplainSearchRepository, times(1)).deleteById(complain.getId());
    }

    @Test
    void searchComplain() {
        // Configure the mock search repository
        when(mockComplainSearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        complainRepository.save(complain).block();
        when(mockComplainSearchRepository.search("id:" + complain.getId())).thenReturn(Flux.just(complain));

        // Search the complain
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + complain.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(complain.getId().intValue()))
            .jsonPath("$.[*].title")
            .value(hasItem(DEFAULT_TITLE))
            .jsonPath("$.[*].data")
            .value(hasItem(DEFAULT_DATA))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS))
            .jsonPath("$.[*].callPhone")
            .value(hasItem(DEFAULT_CALL_PHONE))
            .jsonPath("$.[*].salesNotes")
            .value(hasItem(DEFAULT_SALES_NOTES))
            .jsonPath("$.[*].nextActivity")
            .value(hasItem(sameInstant(DEFAULT_NEXT_ACTIVITY)))
            .jsonPath("$.[*].lastActivity")
            .value(hasItem(sameInstant(DEFAULT_LAST_ACTIVITY)))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
