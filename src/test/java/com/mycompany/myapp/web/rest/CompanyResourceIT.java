package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Company;
import com.mycompany.myapp.repository.CompanyRepository;
import com.mycompany.myapp.repository.search.CompanySearchRepository;
import com.mycompany.myapp.service.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link CompanyResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class CompanyResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_WELCOME_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_WELCOME_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_WEBSITE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PARTNER_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_PARTNER_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCTS_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCTS_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_ABOUT = "AAAAAAAAAA";
    private static final String UPDATED_ABOUT = "BBBBBBBBBB";

    private static final Long DEFAULT_USED_SESSIONS = 1L;
    private static final Long UPDATED_USED_SESSIONS = 2L;

    private static final ZonedDateTime DEFAULT_PLAN_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PLAN_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_PLAN_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PLAN_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/companies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/companies";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CompanyRepository companyRepository;

    @Mock
    private CompanyRepository companyRepositoryMock;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.CompanySearchRepositoryMockConfiguration
     */
    @Autowired
    private CompanySearchRepository mockCompanySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Company company;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Company createEntity(EntityManager em) {
        Company company = new Company()
            .email(DEFAULT_EMAIL)
            .welcomeMessage(DEFAULT_WELCOME_MESSAGE)
            .website(DEFAULT_WEBSITE)
            .name(DEFAULT_NAME)
            .partnerText(DEFAULT_PARTNER_TEXT)
            .productsText(DEFAULT_PRODUCTS_TEXT)
            .about(DEFAULT_ABOUT)
            .usedSessions(DEFAULT_USED_SESSIONS)
            .planStartDate(DEFAULT_PLAN_START_DATE)
            .planEndDate(DEFAULT_PLAN_END_DATE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return company;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Company createUpdatedEntity(EntityManager em) {
        Company company = new Company()
            .email(UPDATED_EMAIL)
            .welcomeMessage(UPDATED_WELCOME_MESSAGE)
            .website(UPDATED_WEBSITE)
            .name(UPDATED_NAME)
            .partnerText(UPDATED_PARTNER_TEXT)
            .productsText(UPDATED_PRODUCTS_TEXT)
            .about(UPDATED_ABOUT)
            .usedSessions(UPDATED_USED_SESSIONS)
            .planStartDate(UPDATED_PLAN_START_DATE)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return company;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll("rel_company__category").block();
            em.deleteAll("rel_company__setting_options").block();
            em.deleteAll(Company.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        company = createEntity(em);
    }

    @Test
    void createCompany() throws Exception {
        int databaseSizeBeforeCreate = companyRepository.findAll().collectList().block().size();
        // Configure the mock search repository
        when(mockCompanySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Create the Company
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeCreate + 1);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCompany.getWelcomeMessage()).isEqualTo(DEFAULT_WELCOME_MESSAGE);
        assertThat(testCompany.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
        assertThat(testCompany.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCompany.getPartnerText()).isEqualTo(DEFAULT_PARTNER_TEXT);
        assertThat(testCompany.getProductsText()).isEqualTo(DEFAULT_PRODUCTS_TEXT);
        assertThat(testCompany.getAbout()).isEqualTo(DEFAULT_ABOUT);
        assertThat(testCompany.getUsedSessions()).isEqualTo(DEFAULT_USED_SESSIONS);
        assertThat(testCompany.getPlanStartDate()).isEqualTo(DEFAULT_PLAN_START_DATE);
        assertThat(testCompany.getPlanEndDate()).isEqualTo(DEFAULT_PLAN_END_DATE);
        assertThat(testCompany.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCompany.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(1)).save(testCompany);
    }

    @Test
    void createCompanyWithExistingId() throws Exception {
        // Create the Company with an existing ID
        company.setId(1L);

        int databaseSizeBeforeCreate = companyRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeCreate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().collectList().block().size();
        // set the field null
        company.setEmail(null);

        // Create the Company, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().collectList().block().size();
        // set the field null
        company.setName(null);

        // Create the Company, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllCompaniesAsStream() {
        // Initialize the database
        companyRepository.save(company).block();

        List<Company> companyList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Company.class)
            .getResponseBody()
            .filter(company::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(companyList).isNotNull();
        assertThat(companyList).hasSize(1);
        Company testCompany = companyList.get(0);
        assertThat(testCompany.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCompany.getWelcomeMessage()).isEqualTo(DEFAULT_WELCOME_MESSAGE);
        assertThat(testCompany.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
        assertThat(testCompany.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCompany.getPartnerText()).isEqualTo(DEFAULT_PARTNER_TEXT);
        assertThat(testCompany.getProductsText()).isEqualTo(DEFAULT_PRODUCTS_TEXT);
        assertThat(testCompany.getAbout()).isEqualTo(DEFAULT_ABOUT);
        assertThat(testCompany.getUsedSessions()).isEqualTo(DEFAULT_USED_SESSIONS);
        assertThat(testCompany.getPlanStartDate()).isEqualTo(DEFAULT_PLAN_START_DATE);
        assertThat(testCompany.getPlanEndDate()).isEqualTo(DEFAULT_PLAN_END_DATE);
        assertThat(testCompany.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCompany.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void getAllCompanies() {
        // Initialize the database
        companyRepository.save(company).block();

        // Get all the companyList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(company.getId().intValue()))
            .jsonPath("$.[*].email")
            .value(hasItem(DEFAULT_EMAIL))
            .jsonPath("$.[*].welcomeMessage")
            .value(hasItem(DEFAULT_WELCOME_MESSAGE))
            .jsonPath("$.[*].website")
            .value(hasItem(DEFAULT_WEBSITE))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].partnerText")
            .value(hasItem(DEFAULT_PARTNER_TEXT))
            .jsonPath("$.[*].productsText")
            .value(hasItem(DEFAULT_PRODUCTS_TEXT))
            .jsonPath("$.[*].about")
            .value(hasItem(DEFAULT_ABOUT))
            .jsonPath("$.[*].usedSessions")
            .value(hasItem(DEFAULT_USED_SESSIONS.intValue()))
            .jsonPath("$.[*].planStartDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_START_DATE)))
            .jsonPath("$.[*].planEndDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_END_DATE)))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCompaniesWithEagerRelationshipsIsEnabled() {
        when(companyRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(companyRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCompaniesWithEagerRelationshipsIsNotEnabled() {
        when(companyRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(companyRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getCompany() {
        // Initialize the database
        companyRepository.save(company).block();

        // Get the company
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, company.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(company.getId().intValue()))
            .jsonPath("$.email")
            .value(is(DEFAULT_EMAIL))
            .jsonPath("$.welcomeMessage")
            .value(is(DEFAULT_WELCOME_MESSAGE))
            .jsonPath("$.website")
            .value(is(DEFAULT_WEBSITE))
            .jsonPath("$.name")
            .value(is(DEFAULT_NAME))
            .jsonPath("$.partnerText")
            .value(is(DEFAULT_PARTNER_TEXT))
            .jsonPath("$.productsText")
            .value(is(DEFAULT_PRODUCTS_TEXT))
            .jsonPath("$.about")
            .value(is(DEFAULT_ABOUT))
            .jsonPath("$.usedSessions")
            .value(is(DEFAULT_USED_SESSIONS.intValue()))
            .jsonPath("$.planStartDate")
            .value(is(sameInstant(DEFAULT_PLAN_START_DATE)))
            .jsonPath("$.planEndDate")
            .value(is(sameInstant(DEFAULT_PLAN_END_DATE)))
            .jsonPath("$.createdAt")
            .value(is(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.updatedAt")
            .value(is(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    void getNonExistingCompany() {
        // Get the company
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCompany() throws Exception {
        // Configure the mock search repository
        when(mockCompanySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        companyRepository.save(company).block();

        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();

        // Update the company
        Company updatedCompany = companyRepository.findById(company.getId()).block();
        updatedCompany
            .email(UPDATED_EMAIL)
            .welcomeMessage(UPDATED_WELCOME_MESSAGE)
            .website(UPDATED_WEBSITE)
            .name(UPDATED_NAME)
            .partnerText(UPDATED_PARTNER_TEXT)
            .productsText(UPDATED_PRODUCTS_TEXT)
            .about(UPDATED_ABOUT)
            .usedSessions(UPDATED_USED_SESSIONS)
            .planStartDate(UPDATED_PLAN_START_DATE)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCompany.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCompany))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCompany.getWelcomeMessage()).isEqualTo(UPDATED_WELCOME_MESSAGE);
        assertThat(testCompany.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testCompany.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCompany.getPartnerText()).isEqualTo(UPDATED_PARTNER_TEXT);
        assertThat(testCompany.getProductsText()).isEqualTo(UPDATED_PRODUCTS_TEXT);
        assertThat(testCompany.getAbout()).isEqualTo(UPDATED_ABOUT);
        assertThat(testCompany.getUsedSessions()).isEqualTo(UPDATED_USED_SESSIONS);
        assertThat(testCompany.getPlanStartDate()).isEqualTo(UPDATED_PLAN_START_DATE);
        assertThat(testCompany.getPlanEndDate()).isEqualTo(UPDATED_PLAN_END_DATE);
        assertThat(testCompany.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCompany.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository).save(testCompany);
    }

    @Test
    void putNonExistingCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();
        company.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, company.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void putWithIdMismatchCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();
        company.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void putWithMissingIdPathParamCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();
        company.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void partialUpdateCompanyWithPatch() throws Exception {
        // Initialize the database
        companyRepository.save(company).block();

        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();

        // Update the company using partial update
        Company partialUpdatedCompany = new Company();
        partialUpdatedCompany.setId(company.getId());

        partialUpdatedCompany
            .welcomeMessage(UPDATED_WELCOME_MESSAGE)
            .website(UPDATED_WEBSITE)
            .partnerText(UPDATED_PARTNER_TEXT)
            .about(UPDATED_ABOUT)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .createdAt(UPDATED_CREATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCompany.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCompany))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCompany.getWelcomeMessage()).isEqualTo(UPDATED_WELCOME_MESSAGE);
        assertThat(testCompany.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testCompany.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCompany.getPartnerText()).isEqualTo(UPDATED_PARTNER_TEXT);
        assertThat(testCompany.getProductsText()).isEqualTo(DEFAULT_PRODUCTS_TEXT);
        assertThat(testCompany.getAbout()).isEqualTo(UPDATED_ABOUT);
        assertThat(testCompany.getUsedSessions()).isEqualTo(DEFAULT_USED_SESSIONS);
        assertThat(testCompany.getPlanStartDate()).isEqualTo(DEFAULT_PLAN_START_DATE);
        assertThat(testCompany.getPlanEndDate()).isEqualTo(UPDATED_PLAN_END_DATE);
        assertThat(testCompany.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCompany.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    void fullUpdateCompanyWithPatch() throws Exception {
        // Initialize the database
        companyRepository.save(company).block();

        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();

        // Update the company using partial update
        Company partialUpdatedCompany = new Company();
        partialUpdatedCompany.setId(company.getId());

        partialUpdatedCompany
            .email(UPDATED_EMAIL)
            .welcomeMessage(UPDATED_WELCOME_MESSAGE)
            .website(UPDATED_WEBSITE)
            .name(UPDATED_NAME)
            .partnerText(UPDATED_PARTNER_TEXT)
            .productsText(UPDATED_PRODUCTS_TEXT)
            .about(UPDATED_ABOUT)
            .usedSessions(UPDATED_USED_SESSIONS)
            .planStartDate(UPDATED_PLAN_START_DATE)
            .planEndDate(UPDATED_PLAN_END_DATE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCompany.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCompany))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCompany.getWelcomeMessage()).isEqualTo(UPDATED_WELCOME_MESSAGE);
        assertThat(testCompany.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testCompany.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCompany.getPartnerText()).isEqualTo(UPDATED_PARTNER_TEXT);
        assertThat(testCompany.getProductsText()).isEqualTo(UPDATED_PRODUCTS_TEXT);
        assertThat(testCompany.getAbout()).isEqualTo(UPDATED_ABOUT);
        assertThat(testCompany.getUsedSessions()).isEqualTo(UPDATED_USED_SESSIONS);
        assertThat(testCompany.getPlanStartDate()).isEqualTo(UPDATED_PLAN_START_DATE);
        assertThat(testCompany.getPlanEndDate()).isEqualTo(UPDATED_PLAN_END_DATE);
        assertThat(testCompany.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCompany.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    void patchNonExistingCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();
        company.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, company.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void patchWithIdMismatchCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();
        company.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void patchWithMissingIdPathParamCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().collectList().block().size();
        company.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(company))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(0)).save(company);
    }

    @Test
    void deleteCompany() {
        // Configure the mock search repository
        when(mockCompanySearchRepository.deleteById(anyLong())).thenReturn(Mono.empty());
        // Initialize the database
        companyRepository.save(company).block();

        int databaseSizeBeforeDelete = companyRepository.findAll().collectList().block().size();

        // Delete the company
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, company.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Company> companyList = companyRepository.findAll().collectList().block();
        assertThat(companyList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Company in Elasticsearch
        verify(mockCompanySearchRepository, times(1)).deleteById(company.getId());
    }

    @Test
    void searchCompany() {
        // Configure the mock search repository
        when(mockCompanySearchRepository.save(any())).thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
        // Initialize the database
        companyRepository.save(company).block();
        when(mockCompanySearchRepository.search("id:" + company.getId())).thenReturn(Flux.just(company));

        // Search the company
        webTestClient
            .get()
            .uri(ENTITY_SEARCH_API_URL + "?query=id:" + company.getId())
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(company.getId().intValue()))
            .jsonPath("$.[*].email")
            .value(hasItem(DEFAULT_EMAIL))
            .jsonPath("$.[*].welcomeMessage")
            .value(hasItem(DEFAULT_WELCOME_MESSAGE))
            .jsonPath("$.[*].website")
            .value(hasItem(DEFAULT_WEBSITE))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].partnerText")
            .value(hasItem(DEFAULT_PARTNER_TEXT))
            .jsonPath("$.[*].productsText")
            .value(hasItem(DEFAULT_PRODUCTS_TEXT))
            .jsonPath("$.[*].about")
            .value(hasItem(DEFAULT_ABOUT))
            .jsonPath("$.[*].usedSessions")
            .value(hasItem(DEFAULT_USED_SESSIONS.intValue()))
            .jsonPath("$.[*].planStartDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_START_DATE)))
            .jsonPath("$.[*].planEndDate")
            .value(hasItem(sameInstant(DEFAULT_PLAN_END_DATE)))
            .jsonPath("$.[*].createdAt")
            .value(hasItem(sameInstant(DEFAULT_CREATED_AT)))
            .jsonPath("$.[*].updatedAt")
            .value(hasItem(sameInstant(DEFAULT_UPDATED_AT)));
    }
}
