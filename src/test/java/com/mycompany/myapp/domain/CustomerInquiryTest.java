package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CustomerInquiryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerInquiry.class);
        CustomerInquiry customerInquiry1 = new CustomerInquiry();
        customerInquiry1.setId(1L);
        CustomerInquiry customerInquiry2 = new CustomerInquiry();
        customerInquiry2.setId(customerInquiry1.getId());
        assertThat(customerInquiry1).isEqualTo(customerInquiry2);
        customerInquiry2.setId(2L);
        assertThat(customerInquiry1).isNotEqualTo(customerInquiry2);
        customerInquiry1.setId(null);
        assertThat(customerInquiry1).isNotEqualTo(customerInquiry2);
    }
}
