package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PaymentHistoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentHistory.class);
        PaymentHistory paymentHistory1 = new PaymentHistory();
        paymentHistory1.setId(1L);
        PaymentHistory paymentHistory2 = new PaymentHistory();
        paymentHistory2.setId(paymentHistory1.getId());
        assertThat(paymentHistory1).isEqualTo(paymentHistory2);
        paymentHistory2.setId(2L);
        assertThat(paymentHistory1).isNotEqualTo(paymentHistory2);
        paymentHistory1.setId(null);
        assertThat(paymentHistory1).isNotEqualTo(paymentHistory2);
    }
}
