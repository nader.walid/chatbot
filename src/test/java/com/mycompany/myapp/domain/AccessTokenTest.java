package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AccessTokenTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccessToken.class);
        AccessToken accessToken1 = new AccessToken();
        accessToken1.setId(1L);
        AccessToken accessToken2 = new AccessToken();
        accessToken2.setId(accessToken1.getId());
        assertThat(accessToken1).isEqualTo(accessToken2);
        accessToken2.setId(2L);
        assertThat(accessToken1).isNotEqualTo(accessToken2);
        accessToken1.setId(null);
        assertThat(accessToken1).isNotEqualTo(accessToken2);
    }
}
