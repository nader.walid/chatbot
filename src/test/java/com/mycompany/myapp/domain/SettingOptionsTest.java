package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SettingOptionsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SettingOptions.class);
        SettingOptions settingOptions1 = new SettingOptions();
        settingOptions1.setId(1L);
        SettingOptions settingOptions2 = new SettingOptions();
        settingOptions2.setId(settingOptions1.getId());
        assertThat(settingOptions1).isEqualTo(settingOptions2);
        settingOptions2.setId(2L);
        assertThat(settingOptions1).isNotEqualTo(settingOptions2);
        settingOptions1.setId(null);
        assertThat(settingOptions1).isNotEqualTo(settingOptions2);
    }
}
