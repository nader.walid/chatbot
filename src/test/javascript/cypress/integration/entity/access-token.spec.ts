import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('AccessToken e2e test', () => {
  const accessTokenPageUrl = '/access-token';
  const accessTokenPageUrlPattern = new RegExp('/access-token(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/access-tokens+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/access-tokens').as('postEntityRequest');
    cy.intercept('DELETE', '/api/access-tokens/*').as('deleteEntityRequest');
  });

  it('should load AccessTokens', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('access-token');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('AccessToken').should('exist');
    cy.url().should('match', accessTokenPageUrlPattern);
  });

  it('should load details AccessToken page', function () {
    cy.visit(accessTokenPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('accessToken');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', accessTokenPageUrlPattern);
  });

  it('should load create AccessToken page', () => {
    cy.visit(accessTokenPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('AccessToken');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', accessTokenPageUrlPattern);
  });

  it('should load edit AccessToken page', function () {
    cy.visit(accessTokenPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('AccessToken');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', accessTokenPageUrlPattern);
  });

  it('should create an instance of AccessToken', () => {
    cy.visit(accessTokenPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('AccessToken');

    cy.get(`[data-cy="appName"]`).type('Phased SDD').should('have.value', 'Phased SDD');

    cy.get(`[data-cy="token"]`).type('Liaison').should('have.value', 'Liaison');

    cy.get(`[data-cy="tokenCreation"]`).type('2021-09-07T23:43').should('have.value', '2021-09-07T23:43');

    cy.get(`[data-cy="tokenExpire"]`).type('2021-09-08T03:41').should('have.value', '2021-09-08T03:41');

    cy.get(`[data-cy="userAlies"]`).type('compressing').should('have.value', 'compressing');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-08T02:07').should('have.value', '2021-09-08T02:07');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-08T02:33').should('have.value', '2021-09-08T02:33');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', accessTokenPageUrlPattern);
  });

  it('should delete last instance of AccessToken', function () {
    cy.intercept('GET', '/api/access-tokens/*').as('dialogDeleteRequest');
    cy.visit(accessTokenPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('accessToken').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', accessTokenPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
