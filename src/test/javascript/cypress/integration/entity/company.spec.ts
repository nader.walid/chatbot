import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Company e2e test', () => {
  const companyPageUrl = '/company';
  const companyPageUrlPattern = new RegExp('/company(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/companies+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/companies').as('postEntityRequest');
    cy.intercept('DELETE', '/api/companies/*').as('deleteEntityRequest');
  });

  it('should load Companies', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('company');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Company').should('exist');
    cy.url().should('match', companyPageUrlPattern);
  });

  it('should load details Company page', function () {
    cy.visit(companyPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('company');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', companyPageUrlPattern);
  });

  it('should load create Company page', () => {
    cy.visit(companyPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Company');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', companyPageUrlPattern);
  });

  it('should load edit Company page', function () {
    cy.visit(companyPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('Company');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', companyPageUrlPattern);
  });

  it('should create an instance of Company', () => {
    cy.visit(companyPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Company');

    cy.get(`[data-cy="email"]`).type('Darren33@hotmail.com').should('have.value', 'Darren33@hotmail.com');

    cy.get(`[data-cy="welcomeMessage"]`).type('silver').should('have.value', 'silver');

    cy.get(`[data-cy="website"]`).type('systemic').should('have.value', 'systemic');

    cy.get(`[data-cy="name"]`).type('connect Interface').should('have.value', 'connect Interface');

    cy.get(`[data-cy="partnerText"]`).type('content-based revolutionary').should('have.value', 'content-based revolutionary');

    cy.get(`[data-cy="productsText"]`).type('Square cutting-edge').should('have.value', 'Square cutting-edge');

    cy.get(`[data-cy="about"]`).type('Wooden').should('have.value', 'Wooden');

    cy.get(`[data-cy="usedSessions"]`).type('58643').should('have.value', '58643');

    cy.get(`[data-cy="planStartDate"]`).type('2021-09-08T01:44').should('have.value', '2021-09-08T01:44');

    cy.get(`[data-cy="planEndDate"]`).type('2021-09-08T01:14').should('have.value', '2021-09-08T01:14');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-08T03:25').should('have.value', '2021-09-08T03:25');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-08T05:33').should('have.value', '2021-09-08T05:33');

    cy.setFieldSelectToLastOfEntity('category');

    cy.setFieldSelectToLastOfEntity('settingOptions');

    cy.setFieldSelectToLastOfEntity('appUser');

    cy.setFieldSelectToLastOfEntity('plan');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', companyPageUrlPattern);
  });

  it('should delete last instance of Company', function () {
    cy.intercept('GET', '/api/companies/*').as('dialogDeleteRequest');
    cy.visit(companyPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('company').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', companyPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
