import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CustomerInquiry e2e test', () => {
  const customerInquiryPageUrl = '/customer-inquiry';
  const customerInquiryPageUrlPattern = new RegExp('/customer-inquiry(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/customer-inquiries+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/customer-inquiries').as('postEntityRequest');
    cy.intercept('DELETE', '/api/customer-inquiries/*').as('deleteEntityRequest');
  });

  it('should load CustomerInquiries', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('customer-inquiry');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CustomerInquiry').should('exist');
    cy.url().should('match', customerInquiryPageUrlPattern);
  });

  it('should load details CustomerInquiry page', function () {
    cy.visit(customerInquiryPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('customerInquiry');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerInquiryPageUrlPattern);
  });

  it('should load create CustomerInquiry page', () => {
    cy.visit(customerInquiryPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('CustomerInquiry');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerInquiryPageUrlPattern);
  });

  it('should load edit CustomerInquiry page', function () {
    cy.visit(customerInquiryPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('CustomerInquiry');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerInquiryPageUrlPattern);
  });

  it('should create an instance of CustomerInquiry', () => {
    cy.visit(customerInquiryPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('CustomerInquiry');

    cy.get(`[data-cy="body"]`).type('Home').should('have.value', 'Home');

    cy.get(`[data-cy="status"]`).type('alarm').should('have.value', 'alarm');

    cy.get(`[data-cy="reason"]`).type('Tasty').should('have.value', 'Tasty');

    cy.get(`[data-cy="callDay"]`).type('2021-09-07T13:54').should('have.value', '2021-09-07T13:54');

    cy.get(`[data-cy="callTime"]`).type('2021-09-08T02:29').should('have.value', '2021-09-08T02:29');

    cy.get(`[data-cy="callPhone"]`).type('2021-09-08T02:23').should('have.value', '2021-09-08T02:23');

    cy.get(`[data-cy="statusNotes"]`).type('turn-key teal').should('have.value', 'turn-key teal');

    cy.get(`[data-cy="nextActivity"]`).type('91002').should('have.value', '91002');

    cy.get(`[data-cy="lastActivity"]`).type('50941').should('have.value', '50941');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-07T14:23').should('have.value', '2021-09-07T14:23');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-08T08:07').should('have.value', '2021-09-08T08:07');

    cy.setFieldSelectToLastOfEntity('customer');

    cy.setFieldSelectToLastOfEntity('company');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerInquiryPageUrlPattern);
  });

  it('should delete last instance of CustomerInquiry', function () {
    cy.intercept('GET', '/api/customer-inquiries/*').as('dialogDeleteRequest');
    cy.visit(customerInquiryPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('customerInquiry').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', customerInquiryPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
