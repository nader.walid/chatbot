import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('SettingOptions e2e test', () => {
  const settingOptionsPageUrl = '/setting-options';
  const settingOptionsPageUrlPattern = new RegExp('/setting-options(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/setting-options+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/setting-options').as('postEntityRequest');
    cy.intercept('DELETE', '/api/setting-options/*').as('deleteEntityRequest');
  });

  it('should load SettingOptions', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('setting-options');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('SettingOptions').should('exist');
    cy.url().should('match', settingOptionsPageUrlPattern);
  });

  it('should load details SettingOptions page', function () {
    cy.visit(settingOptionsPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('settingOptions');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', settingOptionsPageUrlPattern);
  });

  it('should load create SettingOptions page', () => {
    cy.visit(settingOptionsPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('SettingOptions');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', settingOptionsPageUrlPattern);
  });

  it('should load edit SettingOptions page', function () {
    cy.visit(settingOptionsPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('SettingOptions');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', settingOptionsPageUrlPattern);
  });

  it('should create an instance of SettingOptions', () => {
    cy.visit(settingOptionsPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('SettingOptions');

    cy.get(`[data-cy="name"]`).type('Swiss deploy').should('have.value', 'Swiss deploy');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-07T23:26').should('have.value', '2021-09-07T23:26');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-08T04:02').should('have.value', '2021-09-08T04:02');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', settingOptionsPageUrlPattern);
  });

  it('should delete last instance of SettingOptions', function () {
    cy.intercept('GET', '/api/setting-options/*').as('dialogDeleteRequest');
    cy.visit(settingOptionsPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('settingOptions').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', settingOptionsPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
