import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Complain e2e test', () => {
  const complainPageUrl = '/complain';
  const complainPageUrlPattern = new RegExp('/complain(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/complains+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/complains').as('postEntityRequest');
    cy.intercept('DELETE', '/api/complains/*').as('deleteEntityRequest');
  });

  it('should load Complains', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('complain');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Complain').should('exist');
    cy.url().should('match', complainPageUrlPattern);
  });

  it('should load details Complain page', function () {
    cy.visit(complainPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('complain');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', complainPageUrlPattern);
  });

  it('should load create Complain page', () => {
    cy.visit(complainPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Complain');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', complainPageUrlPattern);
  });

  it('should load edit Complain page', function () {
    cy.visit(complainPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('Complain');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', complainPageUrlPattern);
  });

  it('should create an instance of Complain', () => {
    cy.visit(complainPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Complain');

    cy.get(`[data-cy="title"]`).type('high-level Soft').should('have.value', 'high-level Soft');

    cy.get(`[data-cy="data"]`).type('virtual Steel Practical').should('have.value', 'virtual Steel Practical');

    cy.get(`[data-cy="status"]`).type('intuitive up withdrawal').should('have.value', 'intuitive up withdrawal');

    cy.get(`[data-cy="callPhone"]`).type('iterate robust').should('have.value', 'iterate robust');

    cy.get(`[data-cy="salesNotes"]`).type('multi-state azure calculate').should('have.value', 'multi-state azure calculate');

    cy.get(`[data-cy="nextActivity"]`).type('2021-09-07T22:09').should('have.value', '2021-09-07T22:09');

    cy.get(`[data-cy="lastActivity"]`).type('2021-09-08T02:24').should('have.value', '2021-09-08T02:24');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-07T13:19').should('have.value', '2021-09-07T13:19');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-07T12:22').should('have.value', '2021-09-07T12:22');

    cy.setFieldSelectToLastOfEntity('customer');

    cy.setFieldSelectToLastOfEntity('company');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', complainPageUrlPattern);
  });

  it('should delete last instance of Complain', function () {
    cy.intercept('GET', '/api/complains/*').as('dialogDeleteRequest');
    cy.visit(complainPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('complain').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', complainPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
