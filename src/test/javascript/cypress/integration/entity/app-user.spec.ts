import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('AppUser e2e test', () => {
  const appUserPageUrl = '/app-user';
  const appUserPageUrlPattern = new RegExp('/app-user(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/app-users+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/app-users').as('postEntityRequest');
    cy.intercept('DELETE', '/api/app-users/*').as('deleteEntityRequest');
  });

  it('should load AppUsers', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('app-user');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('AppUser').should('exist');
    cy.url().should('match', appUserPageUrlPattern);
  });

  it('should load details AppUser page', function () {
    cy.visit(appUserPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('appUser');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', appUserPageUrlPattern);
  });

  it('should load create AppUser page', () => {
    cy.visit(appUserPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('AppUser');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', appUserPageUrlPattern);
  });

  it('should load edit AppUser page', function () {
    cy.visit(appUserPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('AppUser');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', appUserPageUrlPattern);
  });

  it('should create an instance of AppUser', () => {
    cy.visit(appUserPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('AppUser');

    cy.get(`[data-cy="name"]`).type('primary').should('have.value', 'primary');

    cy.get(`[data-cy="email"]`).type('Marc_Gibson@yahoo.com').should('have.value', 'Marc_Gibson@yahoo.com');

    cy.get(`[data-cy="password"]`).type('calculating').should('have.value', 'calculating');

    cy.setFieldImageAsBytesOfEntity('avatar', 'integration-test.png', 'image/png');

    cy.get(`[data-cy="emailVerifiedAt"]`).type('2021-09-08T06:31').should('have.value', '2021-09-08T06:31');

    cy.get(`[data-cy="rememberToken"]`).type('deposit Factors Pants').should('have.value', 'deposit Factors Pants');

    cy.get(`[data-cy="setting"]`).type('blue').should('have.value', 'blue');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-07T12:11').should('have.value', '2021-09-07T12:11');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-07T16:24').should('have.value', '2021-09-07T16:24');

    cy.setFieldSelectToLastOfEntity('accessToken');

    cy.setFieldSelectToLastOfEntity('role');

    // since cypress clicks submit too fast before the blob fields are validated
    cy.wait(200); // eslint-disable-line cypress/no-unnecessary-waiting
    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', appUserPageUrlPattern);
  });

  it('should delete last instance of AppUser', function () {
    cy.intercept('GET', '/api/app-users/*').as('dialogDeleteRequest');
    cy.visit(appUserPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('appUser').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', appUserPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
