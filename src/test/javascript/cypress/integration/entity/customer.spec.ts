import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Customer e2e test', () => {
  const customerPageUrl = '/customer';
  const customerPageUrlPattern = new RegExp('/customer(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/customers+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/customers').as('postEntityRequest');
    cy.intercept('DELETE', '/api/customers/*').as('deleteEntityRequest');
  });

  it('should load Customers', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('customer');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Customer').should('exist');
    cy.url().should('match', customerPageUrlPattern);
  });

  it('should load details Customer page', function () {
    cy.visit(customerPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('customer');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerPageUrlPattern);
  });

  it('should load create Customer page', () => {
    cy.visit(customerPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Customer');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerPageUrlPattern);
  });

  it('should load edit Customer page', function () {
    cy.visit(customerPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('Customer');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerPageUrlPattern);
  });

  it('should create an instance of Customer', () => {
    cy.visit(customerPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Customer');

    cy.get(`[data-cy="name"]`).type('redundant 1080p').should('have.value', 'redundant 1080p');

    cy.get(`[data-cy="phone"]`).type('607-816-3688').should('have.value', '607-816-3688');

    cy.get(`[data-cy="phone2"]`).type('Optimized Chicken virtual').should('have.value', 'Optimized Chicken virtual');

    cy.get(`[data-cy="phone3"]`).type('Hat Movies').should('have.value', 'Hat Movies');

    cy.get(`[data-cy="email"]`).type('Chaim_Walter@gmail.com').should('have.value', 'Chaim_Walter@gmail.com');

    cy.get(`[data-cy="company"]`).type('leading backing SSL').should('have.value', 'leading backing SSL');

    cy.get(`[data-cy="jobTitle"]`).type('Legacy Communications Technician').should('have.value', 'Legacy Communications Technician');

    cy.get(`[data-cy="nextActivity"]`).type('2021-09-07T19:41').should('have.value', '2021-09-07T19:41');

    cy.get(`[data-cy="lastActivity"]`).type('2021-09-07T12:27').should('have.value', '2021-09-07T12:27');

    cy.get(`[data-cy="salesNotes"]`).type('cross-platform').should('have.value', 'cross-platform');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-07T14:31').should('have.value', '2021-09-07T14:31');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-07T21:38').should('have.value', '2021-09-07T21:38');

    cy.setFieldSelectToLastOfEntity('channel');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', customerPageUrlPattern);
  });

  it('should delete last instance of Customer', function () {
    cy.intercept('GET', '/api/customers/*').as('dialogDeleteRequest');
    cy.visit(customerPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('customer').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', customerPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
