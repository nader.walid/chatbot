import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('PaymentHistory e2e test', () => {
  const paymentHistoryPageUrl = '/payment-history';
  const paymentHistoryPageUrlPattern = new RegExp('/payment-history(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/payment-histories+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/payment-histories').as('postEntityRequest');
    cy.intercept('DELETE', '/api/payment-histories/*').as('deleteEntityRequest');
  });

  it('should load PaymentHistories', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('payment-history');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('PaymentHistory').should('exist');
    cy.url().should('match', paymentHistoryPageUrlPattern);
  });

  it('should load details PaymentHistory page', function () {
    cy.visit(paymentHistoryPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityDetailsButtonSelector).first().click({ force: true });
    cy.getEntityDetailsHeading('paymentHistory');
    cy.get(entityDetailsBackButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', paymentHistoryPageUrlPattern);
  });

  it('should load create PaymentHistory page', () => {
    cy.visit(paymentHistoryPageUrl);
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('PaymentHistory');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', paymentHistoryPageUrlPattern);
  });

  it('should load edit PaymentHistory page', function () {
    cy.visit(paymentHistoryPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        this.skip();
      }
    });
    cy.get(entityEditButtonSelector).first().click({ force: true });
    cy.getEntityCreateUpdateHeading('PaymentHistory');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.get(entityCreateCancelButtonSelector).click({ force: true });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', paymentHistoryPageUrlPattern);
  });

  it('should create an instance of PaymentHistory', () => {
    cy.visit(paymentHistoryPageUrl);
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('PaymentHistory');

    cy.get(`[data-cy="planStartDate"]`).type('2021-09-07T21:20').should('have.value', '2021-09-07T21:20');

    cy.get(`[data-cy="planEndDate"]`).type('2021-09-07T22:25').should('have.value', '2021-09-07T22:25');

    cy.get(`[data-cy="usedSession"]`).type('364').should('have.value', '364');

    cy.get(`[data-cy="createdAt"]`).type('2021-09-08T07:27').should('have.value', '2021-09-08T07:27');

    cy.get(`[data-cy="updatedAt"]`).type('2021-09-07T14:02').should('have.value', '2021-09-07T14:02');

    cy.setFieldSelectToLastOfEntity('company');

    cy.setFieldSelectToLastOfEntity('plan');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.wait('@postEntityRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(201);
    });
    cy.wait('@entitiesRequest').then(({ response }) => {
      expect(response.statusCode).to.equal(200);
    });
    cy.url().should('match', paymentHistoryPageUrlPattern);
  });

  it('should delete last instance of PaymentHistory', function () {
    cy.intercept('GET', '/api/payment-histories/*').as('dialogDeleteRequest');
    cy.visit(paymentHistoryPageUrl);
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', response.body.length);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('paymentHistory').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', paymentHistoryPageUrlPattern);
      } else {
        this.skip();
      }
    });
  });
});
