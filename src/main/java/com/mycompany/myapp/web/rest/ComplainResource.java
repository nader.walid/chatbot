package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Complain;
import com.mycompany.myapp.repository.ComplainRepository;
import com.mycompany.myapp.repository.search.ComplainSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Complain}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ComplainResource {

    private final Logger log = LoggerFactory.getLogger(ComplainResource.class);

    private static final String ENTITY_NAME = "complain";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ComplainRepository complainRepository;

    private final ComplainSearchRepository complainSearchRepository;

    public ComplainResource(ComplainRepository complainRepository, ComplainSearchRepository complainSearchRepository) {
        this.complainRepository = complainRepository;
        this.complainSearchRepository = complainSearchRepository;
    }

    /**
     * {@code POST  /complains} : Create a new complain.
     *
     * @param complain the complain to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new complain, or with status {@code 400 (Bad Request)} if the complain has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/complains")
    public Mono<ResponseEntity<Complain>> createComplain(@Valid @RequestBody Complain complain) throws URISyntaxException {
        log.debug("REST request to save Complain : {}", complain);
        if (complain.getId() != null) {
            throw new BadRequestAlertException("A new complain cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return complainRepository
            .save(complain)
            .flatMap(complainSearchRepository::save)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/complains/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /complains/:id} : Updates an existing complain.
     *
     * @param id the id of the complain to save.
     * @param complain the complain to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated complain,
     * or with status {@code 400 (Bad Request)} if the complain is not valid,
     * or with status {@code 500 (Internal Server Error)} if the complain couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/complains/{id}")
    public Mono<ResponseEntity<Complain>> updateComplain(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Complain complain
    ) throws URISyntaxException {
        log.debug("REST request to update Complain : {}, {}", id, complain);
        if (complain.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, complain.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return complainRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return complainRepository
                        .save(complain)
                        .flatMap(complainSearchRepository::save)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /complains/:id} : Partial updates given fields of an existing complain, field will ignore if it is null
     *
     * @param id the id of the complain to save.
     * @param complain the complain to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated complain,
     * or with status {@code 400 (Bad Request)} if the complain is not valid,
     * or with status {@code 404 (Not Found)} if the complain is not found,
     * or with status {@code 500 (Internal Server Error)} if the complain couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/complains/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Complain>> partialUpdateComplain(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Complain complain
    ) throws URISyntaxException {
        log.debug("REST request to partial update Complain partially : {}, {}", id, complain);
        if (complain.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, complain.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return complainRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Complain> result = complainRepository
                        .findById(complain.getId())
                        .map(
                            existingComplain -> {
                                if (complain.getTitle() != null) {
                                    existingComplain.setTitle(complain.getTitle());
                                }
                                if (complain.getData() != null) {
                                    existingComplain.setData(complain.getData());
                                }
                                if (complain.getStatus() != null) {
                                    existingComplain.setStatus(complain.getStatus());
                                }
                                if (complain.getCallPhone() != null) {
                                    existingComplain.setCallPhone(complain.getCallPhone());
                                }
                                if (complain.getSalesNotes() != null) {
                                    existingComplain.setSalesNotes(complain.getSalesNotes());
                                }
                                if (complain.getNextActivity() != null) {
                                    existingComplain.setNextActivity(complain.getNextActivity());
                                }
                                if (complain.getLastActivity() != null) {
                                    existingComplain.setLastActivity(complain.getLastActivity());
                                }
                                if (complain.getCreatedAt() != null) {
                                    existingComplain.setCreatedAt(complain.getCreatedAt());
                                }
                                if (complain.getUpdatedAt() != null) {
                                    existingComplain.setUpdatedAt(complain.getUpdatedAt());
                                }

                                return existingComplain;
                            }
                        )
                        .flatMap(complainRepository::save)
                        .flatMap(
                            savedComplain -> {
                                complainSearchRepository.save(savedComplain);

                                return Mono.just(savedComplain);
                            }
                        );

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /complains} : get all the complains.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of complains in body.
     */
    @GetMapping("/complains")
    public Mono<List<Complain>> getAllComplains() {
        log.debug("REST request to get all Complains");
        return complainRepository.findAll().collectList();
    }

    /**
     * {@code GET  /complains} : get all the complains as a stream.
     * @return the {@link Flux} of complains.
     */
    @GetMapping(value = "/complains", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Complain> getAllComplainsAsStream() {
        log.debug("REST request to get all Complains as a stream");
        return complainRepository.findAll();
    }

    /**
     * {@code GET  /complains/:id} : get the "id" complain.
     *
     * @param id the id of the complain to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the complain, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/complains/{id}")
    public Mono<ResponseEntity<Complain>> getComplain(@PathVariable Long id) {
        log.debug("REST request to get Complain : {}", id);
        Mono<Complain> complain = complainRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(complain);
    }

    /**
     * {@code DELETE  /complains/:id} : delete the "id" complain.
     *
     * @param id the id of the complain to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/complains/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteComplain(@PathVariable Long id) {
        log.debug("REST request to delete Complain : {}", id);
        return complainRepository
            .deleteById(id)
            .then(complainSearchRepository.deleteById(id))
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/complains?query=:query} : search for the complain corresponding
     * to the query.
     *
     * @param query the query of the complain search.
     * @return the result of the search.
     */
    @GetMapping("/_search/complains")
    public Mono<List<Complain>> searchComplains(@RequestParam String query) {
        log.debug("REST request to search Complains for query {}", query);
        return complainSearchRepository.search(query).collectList();
    }
}
