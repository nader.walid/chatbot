package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.AccessToken;
import com.mycompany.myapp.repository.AccessTokenRepository;
import com.mycompany.myapp.repository.search.AccessTokenSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.AccessToken}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AccessTokenResource {

    private final Logger log = LoggerFactory.getLogger(AccessTokenResource.class);

    private static final String ENTITY_NAME = "accessToken";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AccessTokenRepository accessTokenRepository;

    private final AccessTokenSearchRepository accessTokenSearchRepository;

    public AccessTokenResource(AccessTokenRepository accessTokenRepository, AccessTokenSearchRepository accessTokenSearchRepository) {
        this.accessTokenRepository = accessTokenRepository;
        this.accessTokenSearchRepository = accessTokenSearchRepository;
    }

    /**
     * {@code POST  /access-tokens} : Create a new accessToken.
     *
     * @param accessToken the accessToken to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new accessToken, or with status {@code 400 (Bad Request)} if the accessToken has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/access-tokens")
    public Mono<ResponseEntity<AccessToken>> createAccessToken(@Valid @RequestBody AccessToken accessToken) throws URISyntaxException {
        log.debug("REST request to save AccessToken : {}", accessToken);
        if (accessToken.getId() != null) {
            throw new BadRequestAlertException("A new accessToken cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return accessTokenRepository
            .save(accessToken)
            .flatMap(accessTokenSearchRepository::save)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/access-tokens/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /access-tokens/:id} : Updates an existing accessToken.
     *
     * @param id the id of the accessToken to save.
     * @param accessToken the accessToken to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accessToken,
     * or with status {@code 400 (Bad Request)} if the accessToken is not valid,
     * or with status {@code 500 (Internal Server Error)} if the accessToken couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/access-tokens/{id}")
    public Mono<ResponseEntity<AccessToken>> updateAccessToken(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody AccessToken accessToken
    ) throws URISyntaxException {
        log.debug("REST request to update AccessToken : {}, {}", id, accessToken);
        if (accessToken.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, accessToken.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return accessTokenRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return accessTokenRepository
                        .save(accessToken)
                        .flatMap(accessTokenSearchRepository::save)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /access-tokens/:id} : Partial updates given fields of an existing accessToken, field will ignore if it is null
     *
     * @param id the id of the accessToken to save.
     * @param accessToken the accessToken to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accessToken,
     * or with status {@code 400 (Bad Request)} if the accessToken is not valid,
     * or with status {@code 404 (Not Found)} if the accessToken is not found,
     * or with status {@code 500 (Internal Server Error)} if the accessToken couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/access-tokens/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<AccessToken>> partialUpdateAccessToken(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody AccessToken accessToken
    ) throws URISyntaxException {
        log.debug("REST request to partial update AccessToken partially : {}, {}", id, accessToken);
        if (accessToken.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, accessToken.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return accessTokenRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<AccessToken> result = accessTokenRepository
                        .findById(accessToken.getId())
                        .map(
                            existingAccessToken -> {
                                if (accessToken.getAppName() != null) {
                                    existingAccessToken.setAppName(accessToken.getAppName());
                                }
                                if (accessToken.getToken() != null) {
                                    existingAccessToken.setToken(accessToken.getToken());
                                }
                                if (accessToken.getTokenCreation() != null) {
                                    existingAccessToken.setTokenCreation(accessToken.getTokenCreation());
                                }
                                if (accessToken.getTokenExpire() != null) {
                                    existingAccessToken.setTokenExpire(accessToken.getTokenExpire());
                                }
                                if (accessToken.getUserAlies() != null) {
                                    existingAccessToken.setUserAlies(accessToken.getUserAlies());
                                }
                                if (accessToken.getCreatedAt() != null) {
                                    existingAccessToken.setCreatedAt(accessToken.getCreatedAt());
                                }
                                if (accessToken.getUpdatedAt() != null) {
                                    existingAccessToken.setUpdatedAt(accessToken.getUpdatedAt());
                                }

                                return existingAccessToken;
                            }
                        )
                        .flatMap(accessTokenRepository::save)
                        .flatMap(
                            savedAccessToken -> {
                                accessTokenSearchRepository.save(savedAccessToken);

                                return Mono.just(savedAccessToken);
                            }
                        );

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /access-tokens} : get all the accessTokens.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of accessTokens in body.
     */
    @GetMapping("/access-tokens")
    public Mono<List<AccessToken>> getAllAccessTokens(@RequestParam(required = false) String filter) {
        if ("appuser-is-null".equals(filter)) {
            log.debug("REST request to get all AccessTokens where appUser is null");
            return accessTokenRepository.findAllWhereAppUserIsNull().collectList();
        }
        log.debug("REST request to get all AccessTokens");
        return accessTokenRepository.findAll().collectList();
    }

    /**
     * {@code GET  /access-tokens} : get all the accessTokens as a stream.
     * @return the {@link Flux} of accessTokens.
     */
    @GetMapping(value = "/access-tokens", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<AccessToken> getAllAccessTokensAsStream() {
        log.debug("REST request to get all AccessTokens as a stream");
        return accessTokenRepository.findAll();
    }

    /**
     * {@code GET  /access-tokens/:id} : get the "id" accessToken.
     *
     * @param id the id of the accessToken to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the accessToken, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/access-tokens/{id}")
    public Mono<ResponseEntity<AccessToken>> getAccessToken(@PathVariable Long id) {
        log.debug("REST request to get AccessToken : {}", id);
        Mono<AccessToken> accessToken = accessTokenRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(accessToken);
    }

    /**
     * {@code DELETE  /access-tokens/:id} : delete the "id" accessToken.
     *
     * @param id the id of the accessToken to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/access-tokens/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteAccessToken(@PathVariable Long id) {
        log.debug("REST request to delete AccessToken : {}", id);
        return accessTokenRepository
            .deleteById(id)
            .then(accessTokenSearchRepository.deleteById(id))
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/access-tokens?query=:query} : search for the accessToken corresponding
     * to the query.
     *
     * @param query the query of the accessToken search.
     * @return the result of the search.
     */
    @GetMapping("/_search/access-tokens")
    public Mono<List<AccessToken>> searchAccessTokens(@RequestParam String query) {
        log.debug("REST request to search AccessTokens for query {}", query);
        return accessTokenSearchRepository.search(query).collectList();
    }
}
