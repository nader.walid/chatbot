package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Partner;
import com.mycompany.myapp.repository.PartnerRepository;
import com.mycompany.myapp.repository.search.PartnerSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Partner}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PartnerResource {

    private final Logger log = LoggerFactory.getLogger(PartnerResource.class);

    private static final String ENTITY_NAME = "partner";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PartnerRepository partnerRepository;

    private final PartnerSearchRepository partnerSearchRepository;

    public PartnerResource(PartnerRepository partnerRepository, PartnerSearchRepository partnerSearchRepository) {
        this.partnerRepository = partnerRepository;
        this.partnerSearchRepository = partnerSearchRepository;
    }

    /**
     * {@code POST  /partners} : Create a new partner.
     *
     * @param partner the partner to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new partner, or with status {@code 400 (Bad Request)} if the partner has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/partners")
    public Mono<ResponseEntity<Partner>> createPartner(@Valid @RequestBody Partner partner) throws URISyntaxException {
        log.debug("REST request to save Partner : {}", partner);
        if (partner.getId() != null) {
            throw new BadRequestAlertException("A new partner cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return partnerRepository
            .save(partner)
            .flatMap(partnerSearchRepository::save)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/partners/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /partners/:id} : Updates an existing partner.
     *
     * @param id the id of the partner to save.
     * @param partner the partner to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partner,
     * or with status {@code 400 (Bad Request)} if the partner is not valid,
     * or with status {@code 500 (Internal Server Error)} if the partner couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/partners/{id}")
    public Mono<ResponseEntity<Partner>> updatePartner(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Partner partner
    ) throws URISyntaxException {
        log.debug("REST request to update Partner : {}, {}", id, partner);
        if (partner.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, partner.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return partnerRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return partnerRepository
                        .save(partner)
                        .flatMap(partnerSearchRepository::save)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /partners/:id} : Partial updates given fields of an existing partner, field will ignore if it is null
     *
     * @param id the id of the partner to save.
     * @param partner the partner to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partner,
     * or with status {@code 400 (Bad Request)} if the partner is not valid,
     * or with status {@code 404 (Not Found)} if the partner is not found,
     * or with status {@code 500 (Internal Server Error)} if the partner couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/partners/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Partner>> partialUpdatePartner(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Partner partner
    ) throws URISyntaxException {
        log.debug("REST request to partial update Partner partially : {}, {}", id, partner);
        if (partner.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, partner.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return partnerRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Partner> result = partnerRepository
                        .findById(partner.getId())
                        .map(
                            existingPartner -> {
                                if (partner.getImage() != null) {
                                    existingPartner.setImage(partner.getImage());
                                }
                                if (partner.getImageContentType() != null) {
                                    existingPartner.setImageContentType(partner.getImageContentType());
                                }
                                if (partner.getNextActivity() != null) {
                                    existingPartner.setNextActivity(partner.getNextActivity());
                                }
                                if (partner.getLastActivity() != null) {
                                    existingPartner.setLastActivity(partner.getLastActivity());
                                }
                                if (partner.getCreatedAt() != null) {
                                    existingPartner.setCreatedAt(partner.getCreatedAt());
                                }
                                if (partner.getUpdatedAt() != null) {
                                    existingPartner.setUpdatedAt(partner.getUpdatedAt());
                                }

                                return existingPartner;
                            }
                        )
                        .flatMap(partnerRepository::save)
                        .flatMap(
                            savedPartner -> {
                                partnerSearchRepository.save(savedPartner);

                                return Mono.just(savedPartner);
                            }
                        );

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /partners} : get all the partners.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of partners in body.
     */
    @GetMapping("/partners")
    public Mono<List<Partner>> getAllPartners(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Partners");
        return partnerRepository.findAllWithEagerRelationships().collectList();
    }

    /**
     * {@code GET  /partners} : get all the partners as a stream.
     * @return the {@link Flux} of partners.
     */
    @GetMapping(value = "/partners", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Partner> getAllPartnersAsStream() {
        log.debug("REST request to get all Partners as a stream");
        return partnerRepository.findAll();
    }

    /**
     * {@code GET  /partners/:id} : get the "id" partner.
     *
     * @param id the id of the partner to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the partner, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/partners/{id}")
    public Mono<ResponseEntity<Partner>> getPartner(@PathVariable Long id) {
        log.debug("REST request to get Partner : {}", id);
        Mono<Partner> partner = partnerRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(partner);
    }

    /**
     * {@code DELETE  /partners/:id} : delete the "id" partner.
     *
     * @param id the id of the partner to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/partners/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePartner(@PathVariable Long id) {
        log.debug("REST request to delete Partner : {}", id);
        return partnerRepository
            .deleteById(id)
            .then(partnerSearchRepository.deleteById(id))
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/partners?query=:query} : search for the partner corresponding
     * to the query.
     *
     * @param query the query of the partner search.
     * @return the result of the search.
     */
    @GetMapping("/_search/partners")
    public Mono<List<Partner>> searchPartners(@RequestParam String query) {
        log.debug("REST request to search Partners for query {}", query);
        return partnerSearchRepository.search(query).collectList();
    }
}
