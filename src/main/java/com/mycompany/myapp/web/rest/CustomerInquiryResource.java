package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.CustomerInquiry;
import com.mycompany.myapp.repository.CustomerInquiryRepository;
import com.mycompany.myapp.repository.search.CustomerInquirySearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.CustomerInquiry}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CustomerInquiryResource {

    private final Logger log = LoggerFactory.getLogger(CustomerInquiryResource.class);

    private static final String ENTITY_NAME = "customerInquiry";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerInquiryRepository customerInquiryRepository;

    private final CustomerInquirySearchRepository customerInquirySearchRepository;

    public CustomerInquiryResource(
        CustomerInquiryRepository customerInquiryRepository,
        CustomerInquirySearchRepository customerInquirySearchRepository
    ) {
        this.customerInquiryRepository = customerInquiryRepository;
        this.customerInquirySearchRepository = customerInquirySearchRepository;
    }

    /**
     * {@code POST  /customer-inquiries} : Create a new customerInquiry.
     *
     * @param customerInquiry the customerInquiry to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerInquiry, or with status {@code 400 (Bad Request)} if the customerInquiry has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customer-inquiries")
    public Mono<ResponseEntity<CustomerInquiry>> createCustomerInquiry(@Valid @RequestBody CustomerInquiry customerInquiry)
        throws URISyntaxException {
        log.debug("REST request to save CustomerInquiry : {}", customerInquiry);
        if (customerInquiry.getId() != null) {
            throw new BadRequestAlertException("A new customerInquiry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return customerInquiryRepository
            .save(customerInquiry)
            .flatMap(customerInquirySearchRepository::save)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/customer-inquiries/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /customer-inquiries/:id} : Updates an existing customerInquiry.
     *
     * @param id the id of the customerInquiry to save.
     * @param customerInquiry the customerInquiry to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerInquiry,
     * or with status {@code 400 (Bad Request)} if the customerInquiry is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerInquiry couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/customer-inquiries/{id}")
    public Mono<ResponseEntity<CustomerInquiry>> updateCustomerInquiry(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CustomerInquiry customerInquiry
    ) throws URISyntaxException {
        log.debug("REST request to update CustomerInquiry : {}, {}", id, customerInquiry);
        if (customerInquiry.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerInquiry.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return customerInquiryRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return customerInquiryRepository
                        .save(customerInquiry)
                        .flatMap(customerInquirySearchRepository::save)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /customer-inquiries/:id} : Partial updates given fields of an existing customerInquiry, field will ignore if it is null
     *
     * @param id the id of the customerInquiry to save.
     * @param customerInquiry the customerInquiry to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerInquiry,
     * or with status {@code 400 (Bad Request)} if the customerInquiry is not valid,
     * or with status {@code 404 (Not Found)} if the customerInquiry is not found,
     * or with status {@code 500 (Internal Server Error)} if the customerInquiry couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/customer-inquiries/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<CustomerInquiry>> partialUpdateCustomerInquiry(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CustomerInquiry customerInquiry
    ) throws URISyntaxException {
        log.debug("REST request to partial update CustomerInquiry partially : {}, {}", id, customerInquiry);
        if (customerInquiry.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerInquiry.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return customerInquiryRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<CustomerInquiry> result = customerInquiryRepository
                        .findById(customerInquiry.getId())
                        .map(
                            existingCustomerInquiry -> {
                                if (customerInquiry.getBody() != null) {
                                    existingCustomerInquiry.setBody(customerInquiry.getBody());
                                }
                                if (customerInquiry.getStatus() != null) {
                                    existingCustomerInquiry.setStatus(customerInquiry.getStatus());
                                }
                                if (customerInquiry.getReason() != null) {
                                    existingCustomerInquiry.setReason(customerInquiry.getReason());
                                }
                                if (customerInquiry.getCallDay() != null) {
                                    existingCustomerInquiry.setCallDay(customerInquiry.getCallDay());
                                }
                                if (customerInquiry.getCallTime() != null) {
                                    existingCustomerInquiry.setCallTime(customerInquiry.getCallTime());
                                }
                                if (customerInquiry.getCallPhone() != null) {
                                    existingCustomerInquiry.setCallPhone(customerInquiry.getCallPhone());
                                }
                                if (customerInquiry.getStatusNotes() != null) {
                                    existingCustomerInquiry.setStatusNotes(customerInquiry.getStatusNotes());
                                }
                                if (customerInquiry.getNextActivity() != null) {
                                    existingCustomerInquiry.setNextActivity(customerInquiry.getNextActivity());
                                }
                                if (customerInquiry.getLastActivity() != null) {
                                    existingCustomerInquiry.setLastActivity(customerInquiry.getLastActivity());
                                }
                                if (customerInquiry.getCreatedAt() != null) {
                                    existingCustomerInquiry.setCreatedAt(customerInquiry.getCreatedAt());
                                }
                                if (customerInquiry.getUpdatedAt() != null) {
                                    existingCustomerInquiry.setUpdatedAt(customerInquiry.getUpdatedAt());
                                }

                                return existingCustomerInquiry;
                            }
                        )
                        .flatMap(customerInquiryRepository::save)
                        .flatMap(
                            savedCustomerInquiry -> {
                                customerInquirySearchRepository.save(savedCustomerInquiry);

                                return Mono.just(savedCustomerInquiry);
                            }
                        );

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /customer-inquiries} : get all the customerInquiries.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerInquiries in body.
     */
    @GetMapping("/customer-inquiries")
    public Mono<List<CustomerInquiry>> getAllCustomerInquiries() {
        log.debug("REST request to get all CustomerInquiries");
        return customerInquiryRepository.findAll().collectList();
    }

    /**
     * {@code GET  /customer-inquiries} : get all the customerInquiries as a stream.
     * @return the {@link Flux} of customerInquiries.
     */
    @GetMapping(value = "/customer-inquiries", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<CustomerInquiry> getAllCustomerInquiriesAsStream() {
        log.debug("REST request to get all CustomerInquiries as a stream");
        return customerInquiryRepository.findAll();
    }

    /**
     * {@code GET  /customer-inquiries/:id} : get the "id" customerInquiry.
     *
     * @param id the id of the customerInquiry to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerInquiry, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/customer-inquiries/{id}")
    public Mono<ResponseEntity<CustomerInquiry>> getCustomerInquiry(@PathVariable Long id) {
        log.debug("REST request to get CustomerInquiry : {}", id);
        Mono<CustomerInquiry> customerInquiry = customerInquiryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(customerInquiry);
    }

    /**
     * {@code DELETE  /customer-inquiries/:id} : delete the "id" customerInquiry.
     *
     * @param id the id of the customerInquiry to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customer-inquiries/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCustomerInquiry(@PathVariable Long id) {
        log.debug("REST request to delete CustomerInquiry : {}", id);
        return customerInquiryRepository
            .deleteById(id)
            .then(customerInquirySearchRepository.deleteById(id))
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/customer-inquiries?query=:query} : search for the customerInquiry corresponding
     * to the query.
     *
     * @param query the query of the customerInquiry search.
     * @return the result of the search.
     */
    @GetMapping("/_search/customer-inquiries")
    public Mono<List<CustomerInquiry>> searchCustomerInquiries(@RequestParam String query) {
        log.debug("REST request to search CustomerInquiries for query {}", query);
        return customerInquirySearchRepository.search(query).collectList();
    }
}
