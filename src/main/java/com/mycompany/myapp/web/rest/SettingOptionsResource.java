package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.SettingOptions;
import com.mycompany.myapp.repository.SettingOptionsRepository;
import com.mycompany.myapp.repository.search.SettingOptionsSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.SettingOptions}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SettingOptionsResource {

    private final Logger log = LoggerFactory.getLogger(SettingOptionsResource.class);

    private static final String ENTITY_NAME = "settingOptions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SettingOptionsRepository settingOptionsRepository;

    private final SettingOptionsSearchRepository settingOptionsSearchRepository;

    public SettingOptionsResource(
        SettingOptionsRepository settingOptionsRepository,
        SettingOptionsSearchRepository settingOptionsSearchRepository
    ) {
        this.settingOptionsRepository = settingOptionsRepository;
        this.settingOptionsSearchRepository = settingOptionsSearchRepository;
    }

    /**
     * {@code POST  /setting-options} : Create a new settingOptions.
     *
     * @param settingOptions the settingOptions to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new settingOptions, or with status {@code 400 (Bad Request)} if the settingOptions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/setting-options")
    public Mono<ResponseEntity<SettingOptions>> createSettingOptions(@Valid @RequestBody SettingOptions settingOptions)
        throws URISyntaxException {
        log.debug("REST request to save SettingOptions : {}", settingOptions);
        if (settingOptions.getId() != null) {
            throw new BadRequestAlertException("A new settingOptions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return settingOptionsRepository
            .save(settingOptions)
            .flatMap(settingOptionsSearchRepository::save)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/setting-options/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /setting-options/:id} : Updates an existing settingOptions.
     *
     * @param id the id of the settingOptions to save.
     * @param settingOptions the settingOptions to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated settingOptions,
     * or with status {@code 400 (Bad Request)} if the settingOptions is not valid,
     * or with status {@code 500 (Internal Server Error)} if the settingOptions couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/setting-options/{id}")
    public Mono<ResponseEntity<SettingOptions>> updateSettingOptions(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody SettingOptions settingOptions
    ) throws URISyntaxException {
        log.debug("REST request to update SettingOptions : {}, {}", id, settingOptions);
        if (settingOptions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, settingOptions.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return settingOptionsRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return settingOptionsRepository
                        .save(settingOptions)
                        .flatMap(settingOptionsSearchRepository::save)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /setting-options/:id} : Partial updates given fields of an existing settingOptions, field will ignore if it is null
     *
     * @param id the id of the settingOptions to save.
     * @param settingOptions the settingOptions to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated settingOptions,
     * or with status {@code 400 (Bad Request)} if the settingOptions is not valid,
     * or with status {@code 404 (Not Found)} if the settingOptions is not found,
     * or with status {@code 500 (Internal Server Error)} if the settingOptions couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/setting-options/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<SettingOptions>> partialUpdateSettingOptions(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody SettingOptions settingOptions
    ) throws URISyntaxException {
        log.debug("REST request to partial update SettingOptions partially : {}, {}", id, settingOptions);
        if (settingOptions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, settingOptions.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return settingOptionsRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<SettingOptions> result = settingOptionsRepository
                        .findById(settingOptions.getId())
                        .map(
                            existingSettingOptions -> {
                                if (settingOptions.getName() != null) {
                                    existingSettingOptions.setName(settingOptions.getName());
                                }
                                if (settingOptions.getCreatedAt() != null) {
                                    existingSettingOptions.setCreatedAt(settingOptions.getCreatedAt());
                                }
                                if (settingOptions.getUpdatedAt() != null) {
                                    existingSettingOptions.setUpdatedAt(settingOptions.getUpdatedAt());
                                }

                                return existingSettingOptions;
                            }
                        )
                        .flatMap(settingOptionsRepository::save)
                        .flatMap(
                            savedSettingOptions -> {
                                settingOptionsSearchRepository.save(savedSettingOptions);

                                return Mono.just(savedSettingOptions);
                            }
                        );

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /setting-options} : get all the settingOptions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of settingOptions in body.
     */
    @GetMapping("/setting-options")
    public Mono<List<SettingOptions>> getAllSettingOptions() {
        log.debug("REST request to get all SettingOptions");
        return settingOptionsRepository.findAll().collectList();
    }

    /**
     * {@code GET  /setting-options} : get all the settingOptions as a stream.
     * @return the {@link Flux} of settingOptions.
     */
    @GetMapping(value = "/setting-options", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<SettingOptions> getAllSettingOptionsAsStream() {
        log.debug("REST request to get all SettingOptions as a stream");
        return settingOptionsRepository.findAll();
    }

    /**
     * {@code GET  /setting-options/:id} : get the "id" settingOptions.
     *
     * @param id the id of the settingOptions to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the settingOptions, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/setting-options/{id}")
    public Mono<ResponseEntity<SettingOptions>> getSettingOptions(@PathVariable Long id) {
        log.debug("REST request to get SettingOptions : {}", id);
        Mono<SettingOptions> settingOptions = settingOptionsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(settingOptions);
    }

    /**
     * {@code DELETE  /setting-options/:id} : delete the "id" settingOptions.
     *
     * @param id the id of the settingOptions to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/setting-options/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteSettingOptions(@PathVariable Long id) {
        log.debug("REST request to delete SettingOptions : {}", id);
        return settingOptionsRepository
            .deleteById(id)
            .then(settingOptionsSearchRepository.deleteById(id))
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/setting-options?query=:query} : search for the settingOptions corresponding
     * to the query.
     *
     * @param query the query of the settingOptions search.
     * @return the result of the search.
     */
    @GetMapping("/_search/setting-options")
    public Mono<List<SettingOptions>> searchSettingOptions(@RequestParam String query) {
        log.debug("REST request to search SettingOptions for query {}", query);
        return settingOptionsSearchRepository.search(query).collectList();
    }
}
