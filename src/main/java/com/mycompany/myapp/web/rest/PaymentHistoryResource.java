package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.PaymentHistory;
import com.mycompany.myapp.repository.PaymentHistoryRepository;
import com.mycompany.myapp.repository.search.PaymentHistorySearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.PaymentHistory}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PaymentHistoryResource {

    private final Logger log = LoggerFactory.getLogger(PaymentHistoryResource.class);

    private static final String ENTITY_NAME = "paymentHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentHistoryRepository paymentHistoryRepository;

    private final PaymentHistorySearchRepository paymentHistorySearchRepository;

    public PaymentHistoryResource(
        PaymentHistoryRepository paymentHistoryRepository,
        PaymentHistorySearchRepository paymentHistorySearchRepository
    ) {
        this.paymentHistoryRepository = paymentHistoryRepository;
        this.paymentHistorySearchRepository = paymentHistorySearchRepository;
    }

    /**
     * {@code POST  /payment-histories} : Create a new paymentHistory.
     *
     * @param paymentHistory the paymentHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentHistory, or with status {@code 400 (Bad Request)} if the paymentHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/payment-histories")
    public Mono<ResponseEntity<PaymentHistory>> createPaymentHistory(@Valid @RequestBody PaymentHistory paymentHistory)
        throws URISyntaxException {
        log.debug("REST request to save PaymentHistory : {}", paymentHistory);
        if (paymentHistory.getId() != null) {
            throw new BadRequestAlertException("A new paymentHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return paymentHistoryRepository
            .save(paymentHistory)
            .flatMap(paymentHistorySearchRepository::save)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/payment-histories/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /payment-histories/:id} : Updates an existing paymentHistory.
     *
     * @param id the id of the paymentHistory to save.
     * @param paymentHistory the paymentHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentHistory,
     * or with status {@code 400 (Bad Request)} if the paymentHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/payment-histories/{id}")
    public Mono<ResponseEntity<PaymentHistory>> updatePaymentHistory(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PaymentHistory paymentHistory
    ) throws URISyntaxException {
        log.debug("REST request to update PaymentHistory : {}, {}", id, paymentHistory);
        if (paymentHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentHistory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return paymentHistoryRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return paymentHistoryRepository
                        .save(paymentHistory)
                        .flatMap(paymentHistorySearchRepository::save)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /payment-histories/:id} : Partial updates given fields of an existing paymentHistory, field will ignore if it is null
     *
     * @param id the id of the paymentHistory to save.
     * @param paymentHistory the paymentHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentHistory,
     * or with status {@code 400 (Bad Request)} if the paymentHistory is not valid,
     * or with status {@code 404 (Not Found)} if the paymentHistory is not found,
     * or with status {@code 500 (Internal Server Error)} if the paymentHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/payment-histories/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<PaymentHistory>> partialUpdatePaymentHistory(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PaymentHistory paymentHistory
    ) throws URISyntaxException {
        log.debug("REST request to partial update PaymentHistory partially : {}, {}", id, paymentHistory);
        if (paymentHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentHistory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return paymentHistoryRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<PaymentHistory> result = paymentHistoryRepository
                        .findById(paymentHistory.getId())
                        .map(
                            existingPaymentHistory -> {
                                if (paymentHistory.getPlanStartDate() != null) {
                                    existingPaymentHistory.setPlanStartDate(paymentHistory.getPlanStartDate());
                                }
                                if (paymentHistory.getPlanEndDate() != null) {
                                    existingPaymentHistory.setPlanEndDate(paymentHistory.getPlanEndDate());
                                }
                                if (paymentHistory.getUsedSession() != null) {
                                    existingPaymentHistory.setUsedSession(paymentHistory.getUsedSession());
                                }
                                if (paymentHistory.getCreatedAt() != null) {
                                    existingPaymentHistory.setCreatedAt(paymentHistory.getCreatedAt());
                                }
                                if (paymentHistory.getUpdatedAt() != null) {
                                    existingPaymentHistory.setUpdatedAt(paymentHistory.getUpdatedAt());
                                }

                                return existingPaymentHistory;
                            }
                        )
                        .flatMap(paymentHistoryRepository::save)
                        .flatMap(
                            savedPaymentHistory -> {
                                paymentHistorySearchRepository.save(savedPaymentHistory);

                                return Mono.just(savedPaymentHistory);
                            }
                        );

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /payment-histories} : get all the paymentHistories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentHistories in body.
     */
    @GetMapping("/payment-histories")
    public Mono<List<PaymentHistory>> getAllPaymentHistories() {
        log.debug("REST request to get all PaymentHistories");
        return paymentHistoryRepository.findAll().collectList();
    }

    /**
     * {@code GET  /payment-histories} : get all the paymentHistories as a stream.
     * @return the {@link Flux} of paymentHistories.
     */
    @GetMapping(value = "/payment-histories", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<PaymentHistory> getAllPaymentHistoriesAsStream() {
        log.debug("REST request to get all PaymentHistories as a stream");
        return paymentHistoryRepository.findAll();
    }

    /**
     * {@code GET  /payment-histories/:id} : get the "id" paymentHistory.
     *
     * @param id the id of the paymentHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payment-histories/{id}")
    public Mono<ResponseEntity<PaymentHistory>> getPaymentHistory(@PathVariable Long id) {
        log.debug("REST request to get PaymentHistory : {}", id);
        Mono<PaymentHistory> paymentHistory = paymentHistoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(paymentHistory);
    }

    /**
     * {@code DELETE  /payment-histories/:id} : delete the "id" paymentHistory.
     *
     * @param id the id of the paymentHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payment-histories/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePaymentHistory(@PathVariable Long id) {
        log.debug("REST request to delete PaymentHistory : {}", id);
        return paymentHistoryRepository
            .deleteById(id)
            .then(paymentHistorySearchRepository.deleteById(id))
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }

    /**
     * {@code SEARCH  /_search/payment-histories?query=:query} : search for the paymentHistory corresponding
     * to the query.
     *
     * @param query the query of the paymentHistory search.
     * @return the result of the search.
     */
    @GetMapping("/_search/payment-histories")
    public Mono<List<PaymentHistory>> searchPaymentHistories(@RequestParam String query) {
        log.debug("REST request to search PaymentHistories for query {}", query);
        return paymentHistorySearchRepository.search(query).collectList();
    }
}
