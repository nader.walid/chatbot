package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.AccessToken;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the AccessToken entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccessTokenRepository extends R2dbcRepository<AccessToken, Long>, AccessTokenRepositoryInternal {
    @Query("SELECT * FROM access_token entity WHERE entity.id not in (select access_token_id from app_user)")
    Flux<AccessToken> findAllWhereAppUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<AccessToken> findAll();

    @Override
    Mono<AccessToken> findById(Long id);

    @Override
    <S extends AccessToken> Mono<S> save(S entity);
}

interface AccessTokenRepositoryInternal {
    <S extends AccessToken> Mono<S> insert(S entity);
    <S extends AccessToken> Mono<S> save(S entity);
    Mono<Integer> update(AccessToken entity);

    Flux<AccessToken> findAll();
    Mono<AccessToken> findById(Long id);
    Flux<AccessToken> findAllBy(Pageable pageable);
    Flux<AccessToken> findAllBy(Pageable pageable, Criteria criteria);
}
