package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Complain;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Complain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComplainRepository extends R2dbcRepository<Complain, Long>, ComplainRepositoryInternal {
    @Query("SELECT * FROM complain entity WHERE entity.customer_id = :id")
    Flux<Complain> findByCustomer(Long id);

    @Query("SELECT * FROM complain entity WHERE entity.customer_id IS NULL")
    Flux<Complain> findAllWhereCustomerIsNull();

    @Query("SELECT * FROM complain entity WHERE entity.company_id = :id")
    Flux<Complain> findByCompany(Long id);

    @Query("SELECT * FROM complain entity WHERE entity.company_id IS NULL")
    Flux<Complain> findAllWhereCompanyIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Complain> findAll();

    @Override
    Mono<Complain> findById(Long id);

    @Override
    <S extends Complain> Mono<S> save(S entity);
}

interface ComplainRepositoryInternal {
    <S extends Complain> Mono<S> insert(S entity);
    <S extends Complain> Mono<S> save(S entity);
    Mono<Integer> update(Complain entity);

    Flux<Complain> findAll();
    Mono<Complain> findById(Long id);
    Flux<Complain> findAllBy(Pageable pageable);
    Flux<Complain> findAllBy(Pageable pageable, Criteria criteria);
}
