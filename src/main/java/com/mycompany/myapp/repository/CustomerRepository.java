package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Customer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Customer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerRepository extends R2dbcRepository<Customer, Long>, CustomerRepositoryInternal {
    @Query("SELECT * FROM customer entity WHERE entity.channel_id = :id")
    Flux<Customer> findByChannel(Long id);

    @Query("SELECT * FROM customer entity WHERE entity.channel_id IS NULL")
    Flux<Customer> findAllWhereChannelIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Customer> findAll();

    @Override
    Mono<Customer> findById(Long id);

    @Override
    <S extends Customer> Mono<S> save(S entity);
}

interface CustomerRepositoryInternal {
    <S extends Customer> Mono<S> insert(S entity);
    <S extends Customer> Mono<S> save(S entity);
    Mono<Integer> update(Customer entity);

    Flux<Customer> findAll();
    Mono<Customer> findById(Long id);
    Flux<Customer> findAllBy(Pageable pageable);
    Flux<Customer> findAllBy(Pageable pageable, Criteria criteria);
}
