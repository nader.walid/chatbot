package com.mycompany.myapp.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.Company;
import com.mycompany.myapp.domain.SettingOptions;
import com.mycompany.myapp.repository.rowmapper.AppUserRowMapper;
import com.mycompany.myapp.repository.rowmapper.CompanyRowMapper;
import com.mycompany.myapp.repository.rowmapper.PlanRowMapper;
import com.mycompany.myapp.service.EntityManager;
import com.mycompany.myapp.service.EntityManager.LinkTable;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Company entity.
 */
@SuppressWarnings("unused")
class CompanyRepositoryInternalImpl implements CompanyRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final AppUserRowMapper appuserMapper;
    private final PlanRowMapper planMapper;
    private final CompanyRowMapper companyMapper;

    private static final Table entityTable = Table.aliased("company", EntityManager.ENTITY_ALIAS);
    private static final Table appUserTable = Table.aliased("app_user", "appUser");
    private static final Table planTable = Table.aliased("plan", "e_plan");

    private static final EntityManager.LinkTable categoryLink = new LinkTable("rel_company__category", "company_id", "category_id");
    private static final EntityManager.LinkTable settingOptionsLink = new LinkTable(
        "rel_company__setting_options",
        "company_id",
        "setting_options_id"
    );

    public CompanyRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        AppUserRowMapper appuserMapper,
        PlanRowMapper planMapper,
        CompanyRowMapper companyMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.appuserMapper = appuserMapper;
        this.planMapper = planMapper;
        this.companyMapper = companyMapper;
    }

    @Override
    public Flux<Company> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Company> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Company> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = CompanySqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(AppUserSqlHelper.getColumns(appUserTable, "appUser"));
        columns.addAll(PlanSqlHelper.getColumns(planTable, "plan"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(appUserTable)
            .on(Column.create("app_user_id", entityTable))
            .equals(Column.create("id", appUserTable))
            .leftOuterJoin(planTable)
            .on(Column.create("plan_id", entityTable))
            .equals(Column.create("id", planTable));

        String select = entityManager.createSelect(selectFrom, Company.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Company> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Company> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    @Override
    public Mono<Company> findOneWithEagerRelationships(Long id) {
        return findById(id);
    }

    @Override
    public Flux<Company> findAllWithEagerRelationships() {
        return findAll();
    }

    @Override
    public Flux<Company> findAllWithEagerRelationships(Pageable page) {
        return findAllBy(page);
    }

    private Company process(Row row, RowMetadata metadata) {
        Company entity = companyMapper.apply(row, "e");
        entity.setAppUser(appuserMapper.apply(row, "appUser"));
        entity.setPlan(planMapper.apply(row, "plan"));
        return entity;
    }

    @Override
    public <S extends Company> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Company> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity).flatMap(savedEntity -> updateRelations(savedEntity));
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update Company with id = " + entity.getId());
                        }
                        return entity;
                    }
                )
                .then(updateRelations(entity));
        }
    }

    @Override
    public Mono<Integer> update(Company entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }

    @Override
    public Mono<Void> deleteById(Long entityId) {
        return deleteRelations(entityId)
            .then(r2dbcEntityTemplate.delete(Company.class).matching(query(where("id").is(entityId))).all().then());
    }

    protected <S extends Company> Mono<S> updateRelations(S entity) {
        Mono<Void> result = entityManager
            .updateLinkTable(categoryLink, entity.getId(), entity.getCategories().stream().map(Category::getId))
            .then();
        result =
            result.and(
                entityManager.updateLinkTable(
                    settingOptionsLink,
                    entity.getId(),
                    entity.getSettingOptions().stream().map(SettingOptions::getId)
                )
            );
        return result.thenReturn(entity);
    }

    protected Mono<Void> deleteRelations(Long entityId) {
        return entityManager
            .deleteFromLinkTable(categoryLink, entityId)
            .and(entityManager.deleteFromLinkTable(settingOptionsLink, entityId));
    }
}

class CompanySqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("email", table, columnPrefix + "_email"));
        columns.add(Column.aliased("welcome_message", table, columnPrefix + "_welcome_message"));
        columns.add(Column.aliased("website", table, columnPrefix + "_website"));
        columns.add(Column.aliased("name", table, columnPrefix + "_name"));
        columns.add(Column.aliased("partner_text", table, columnPrefix + "_partner_text"));
        columns.add(Column.aliased("products_text", table, columnPrefix + "_products_text"));
        columns.add(Column.aliased("about", table, columnPrefix + "_about"));
        columns.add(Column.aliased("used_sessions", table, columnPrefix + "_used_sessions"));
        columns.add(Column.aliased("plan_start_date", table, columnPrefix + "_plan_start_date"));
        columns.add(Column.aliased("plan_end_date", table, columnPrefix + "_plan_end_date"));
        columns.add(Column.aliased("created_at", table, columnPrefix + "_created_at"));
        columns.add(Column.aliased("updated_at", table, columnPrefix + "_updated_at"));

        columns.add(Column.aliased("app_user_id", table, columnPrefix + "_app_user_id"));
        columns.add(Column.aliased("plan_id", table, columnPrefix + "_plan_id"));
        return columns;
    }
}
