package com.mycompany.myapp.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.mycompany.myapp.domain.AppUser;
import com.mycompany.myapp.domain.Role;
import com.mycompany.myapp.repository.rowmapper.AccessTokenRowMapper;
import com.mycompany.myapp.repository.rowmapper.AppUserRowMapper;
import com.mycompany.myapp.service.EntityManager;
import com.mycompany.myapp.service.EntityManager.LinkTable;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the AppUser entity.
 */
@SuppressWarnings("unused")
class AppUserRepositoryInternalImpl implements AppUserRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final AccessTokenRowMapper accesstokenMapper;
    private final AppUserRowMapper appuserMapper;

    private static final Table entityTable = Table.aliased("app_user", EntityManager.ENTITY_ALIAS);
    private static final Table accessTokenTable = Table.aliased("access_token", "accessToken");

    private static final EntityManager.LinkTable roleLink = new LinkTable("rel_app_user__role", "app_user_id", "role_id");

    public AppUserRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        AccessTokenRowMapper accesstokenMapper,
        AppUserRowMapper appuserMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.accesstokenMapper = accesstokenMapper;
        this.appuserMapper = appuserMapper;
    }

    @Override
    public Flux<AppUser> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<AppUser> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<AppUser> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = AppUserSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(AccessTokenSqlHelper.getColumns(accessTokenTable, "accessToken"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(accessTokenTable)
            .on(Column.create("access_token_id", entityTable))
            .equals(Column.create("id", accessTokenTable));

        String select = entityManager.createSelect(selectFrom, AppUser.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<AppUser> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<AppUser> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    @Override
    public Mono<AppUser> findOneWithEagerRelationships(Long id) {
        return findById(id);
    }

    @Override
    public Flux<AppUser> findAllWithEagerRelationships() {
        return findAll();
    }

    @Override
    public Flux<AppUser> findAllWithEagerRelationships(Pageable page) {
        return findAllBy(page);
    }

    private AppUser process(Row row, RowMetadata metadata) {
        AppUser entity = appuserMapper.apply(row, "e");
        entity.setAccessToken(accesstokenMapper.apply(row, "accessToken"));
        return entity;
    }

    @Override
    public <S extends AppUser> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends AppUser> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity).flatMap(savedEntity -> updateRelations(savedEntity));
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update AppUser with id = " + entity.getId());
                        }
                        return entity;
                    }
                )
                .then(updateRelations(entity));
        }
    }

    @Override
    public Mono<Integer> update(AppUser entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }

    @Override
    public Mono<Void> deleteById(Long entityId) {
        return deleteRelations(entityId)
            .then(r2dbcEntityTemplate.delete(AppUser.class).matching(query(where("id").is(entityId))).all().then());
    }

    protected <S extends AppUser> Mono<S> updateRelations(S entity) {
        Mono<Void> result = entityManager.updateLinkTable(roleLink, entity.getId(), entity.getRoles().stream().map(Role::getId)).then();
        return result.thenReturn(entity);
    }

    protected Mono<Void> deleteRelations(Long entityId) {
        return entityManager.deleteFromLinkTable(roleLink, entityId);
    }
}

class AppUserSqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("name", table, columnPrefix + "_name"));
        columns.add(Column.aliased("email", table, columnPrefix + "_email"));
        columns.add(Column.aliased("password", table, columnPrefix + "_password"));
        columns.add(Column.aliased("avatar", table, columnPrefix + "_avatar"));
        columns.add(Column.aliased("avatar_content_type", table, columnPrefix + "_avatar_content_type"));
        columns.add(Column.aliased("email_verified_at", table, columnPrefix + "_email_verified_at"));
        columns.add(Column.aliased("remember_token", table, columnPrefix + "_remember_token"));
        columns.add(Column.aliased("setting", table, columnPrefix + "_setting"));
        columns.add(Column.aliased("created_at", table, columnPrefix + "_created_at"));
        columns.add(Column.aliased("updated_at", table, columnPrefix + "_updated_at"));

        columns.add(Column.aliased("access_token_id", table, columnPrefix + "_access_token_id"));
        return columns;
    }
}
