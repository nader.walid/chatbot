package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Partner;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Partner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartnerRepository extends R2dbcRepository<Partner, Long>, PartnerRepositoryInternal {
    @Override
    Mono<Partner> findOneWithEagerRelationships(Long id);

    @Override
    Flux<Partner> findAllWithEagerRelationships();

    @Override
    Flux<Partner> findAllWithEagerRelationships(Pageable page);

    @Override
    Mono<Void> deleteById(Long id);

    @Query(
        "SELECT entity.* FROM partner entity JOIN rel_partner__company joinTable ON entity.id = joinTable.partner_id WHERE joinTable.company_id = :id"
    )
    Flux<Partner> findByCompany(Long id);

    // just to avoid having unambigous methods
    @Override
    Flux<Partner> findAll();

    @Override
    Mono<Partner> findById(Long id);

    @Override
    <S extends Partner> Mono<S> save(S entity);
}

interface PartnerRepositoryInternal {
    <S extends Partner> Mono<S> insert(S entity);
    <S extends Partner> Mono<S> save(S entity);
    Mono<Integer> update(Partner entity);

    Flux<Partner> findAll();
    Mono<Partner> findById(Long id);
    Flux<Partner> findAllBy(Pageable pageable);
    Flux<Partner> findAllBy(Pageable pageable, Criteria criteria);

    Mono<Partner> findOneWithEagerRelationships(Long id);

    Flux<Partner> findAllWithEagerRelationships();

    Flux<Partner> findAllWithEagerRelationships(Pageable page);

    Mono<Void> deleteById(Long id);
}
