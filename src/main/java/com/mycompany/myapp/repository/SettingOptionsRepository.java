package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.SettingOptions;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the SettingOptions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SettingOptionsRepository extends R2dbcRepository<SettingOptions, Long>, SettingOptionsRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<SettingOptions> findAll();

    @Override
    Mono<SettingOptions> findById(Long id);

    @Override
    <S extends SettingOptions> Mono<S> save(S entity);
}

interface SettingOptionsRepositoryInternal {
    <S extends SettingOptions> Mono<S> insert(S entity);
    <S extends SettingOptions> Mono<S> save(S entity);
    Mono<Integer> update(SettingOptions entity);

    Flux<SettingOptions> findAll();
    Mono<SettingOptions> findById(Long id);
    Flux<SettingOptions> findAllBy(Pageable pageable);
    Flux<SettingOptions> findAllBy(Pageable pageable, Criteria criteria);
}
