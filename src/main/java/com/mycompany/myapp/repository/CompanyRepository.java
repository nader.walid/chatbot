package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Company;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Company entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyRepository extends R2dbcRepository<Company, Long>, CompanyRepositoryInternal {
    @Override
    Mono<Company> findOneWithEagerRelationships(Long id);

    @Override
    Flux<Company> findAllWithEagerRelationships();

    @Override
    Flux<Company> findAllWithEagerRelationships(Pageable page);

    @Override
    Mono<Void> deleteById(Long id);

    @Query(
        "SELECT entity.* FROM company entity JOIN rel_company__category joinTable ON entity.id = joinTable.company_id WHERE joinTable.category_id = :id"
    )
    Flux<Company> findByCategory(Long id);

    @Query(
        "SELECT entity.* FROM company entity JOIN rel_company__setting_options joinTable ON entity.id = joinTable.company_id WHERE joinTable.setting_options_id = :id"
    )
    Flux<Company> findBySettingOptions(Long id);

    @Query("SELECT * FROM company entity WHERE entity.app_user_id = :id")
    Flux<Company> findByAppUser(Long id);

    @Query("SELECT * FROM company entity WHERE entity.app_user_id IS NULL")
    Flux<Company> findAllWhereAppUserIsNull();

    @Query("SELECT * FROM company entity WHERE entity.plan_id = :id")
    Flux<Company> findByPlan(Long id);

    @Query("SELECT * FROM company entity WHERE entity.plan_id IS NULL")
    Flux<Company> findAllWherePlanIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Company> findAll();

    @Override
    Mono<Company> findById(Long id);

    @Override
    <S extends Company> Mono<S> save(S entity);
}

interface CompanyRepositoryInternal {
    <S extends Company> Mono<S> insert(S entity);
    <S extends Company> Mono<S> save(S entity);
    Mono<Integer> update(Company entity);

    Flux<Company> findAll();
    Mono<Company> findById(Long id);
    Flux<Company> findAllBy(Pageable pageable);
    Flux<Company> findAllBy(Pageable pageable, Criteria criteria);

    Mono<Company> findOneWithEagerRelationships(Long id);

    Flux<Company> findAllWithEagerRelationships();

    Flux<Company> findAllWithEagerRelationships(Pageable page);

    Mono<Void> deleteById(Long id);
}
