package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.PaymentHistory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the PaymentHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentHistoryRepository extends R2dbcRepository<PaymentHistory, Long>, PaymentHistoryRepositoryInternal {
    @Query("SELECT * FROM payment_history entity WHERE entity.company_id = :id")
    Flux<PaymentHistory> findByCompany(Long id);

    @Query("SELECT * FROM payment_history entity WHERE entity.company_id IS NULL")
    Flux<PaymentHistory> findAllWhereCompanyIsNull();

    @Query("SELECT * FROM payment_history entity WHERE entity.plan_id = :id")
    Flux<PaymentHistory> findByPlan(Long id);

    @Query("SELECT * FROM payment_history entity WHERE entity.plan_id IS NULL")
    Flux<PaymentHistory> findAllWherePlanIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<PaymentHistory> findAll();

    @Override
    Mono<PaymentHistory> findById(Long id);

    @Override
    <S extends PaymentHistory> Mono<S> save(S entity);
}

interface PaymentHistoryRepositoryInternal {
    <S extends PaymentHistory> Mono<S> insert(S entity);
    <S extends PaymentHistory> Mono<S> save(S entity);
    Mono<Integer> update(PaymentHistory entity);

    Flux<PaymentHistory> findAll();
    Mono<PaymentHistory> findById(Long id);
    Flux<PaymentHistory> findAllBy(Pageable pageable);
    Flux<PaymentHistory> findAllBy(Pageable pageable, Criteria criteria);
}
