package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.AppUser;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the AppUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppUserRepository extends R2dbcRepository<AppUser, Long>, AppUserRepositoryInternal {
    @Override
    Mono<AppUser> findOneWithEagerRelationships(Long id);

    @Override
    Flux<AppUser> findAllWithEagerRelationships();

    @Override
    Flux<AppUser> findAllWithEagerRelationships(Pageable page);

    @Override
    Mono<Void> deleteById(Long id);

    @Query("SELECT * FROM app_user entity WHERE entity.access_token_id = :id")
    Flux<AppUser> findByAccessToken(Long id);

    @Query("SELECT * FROM app_user entity WHERE entity.access_token_id IS NULL")
    Flux<AppUser> findAllWhereAccessTokenIsNull();

    @Query(
        "SELECT entity.* FROM app_user entity JOIN rel_app_user__role joinTable ON entity.id = joinTable.app_user_id WHERE joinTable.role_id = :id"
    )
    Flux<AppUser> findByRole(Long id);

    // just to avoid having unambigous methods
    @Override
    Flux<AppUser> findAll();

    @Override
    Mono<AppUser> findById(Long id);

    @Override
    <S extends AppUser> Mono<S> save(S entity);
}

interface AppUserRepositoryInternal {
    <S extends AppUser> Mono<S> insert(S entity);
    <S extends AppUser> Mono<S> save(S entity);
    Mono<Integer> update(AppUser entity);

    Flux<AppUser> findAll();
    Mono<AppUser> findById(Long id);
    Flux<AppUser> findAllBy(Pageable pageable);
    Flux<AppUser> findAllBy(Pageable pageable, Criteria criteria);

    Mono<AppUser> findOneWithEagerRelationships(Long id);

    Flux<AppUser> findAllWithEagerRelationships();

    Flux<AppUser> findAllWithEagerRelationships(Pageable page);

    Mono<Void> deleteById(Long id);
}
