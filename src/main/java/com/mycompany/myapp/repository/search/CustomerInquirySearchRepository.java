package com.mycompany.myapp.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import com.mycompany.myapp.domain.CustomerInquiry;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Flux;

/**
 * Spring Data Elasticsearch repository for the {@link CustomerInquiry} entity.
 */
public interface CustomerInquirySearchRepository
    extends ReactiveElasticsearchRepository<CustomerInquiry, Long>, CustomerInquirySearchRepositoryInternal {}

interface CustomerInquirySearchRepositoryInternal {
    Flux<CustomerInquiry> search(String query);
}

class CustomerInquirySearchRepositoryInternalImpl implements CustomerInquirySearchRepositoryInternal {

    private final ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    CustomerInquirySearchRepositoryInternalImpl(ReactiveElasticsearchTemplate reactiveElasticsearchTemplate) {
        this.reactiveElasticsearchTemplate = reactiveElasticsearchTemplate;
    }

    @Override
    public Flux<CustomerInquiry> search(String query) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        return reactiveElasticsearchTemplate.search(nativeSearchQuery, CustomerInquiry.class).map(SearchHit::getContent);
    }
}
