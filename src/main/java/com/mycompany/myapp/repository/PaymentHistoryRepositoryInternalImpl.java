package com.mycompany.myapp.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.mycompany.myapp.domain.PaymentHistory;
import com.mycompany.myapp.repository.rowmapper.CompanyRowMapper;
import com.mycompany.myapp.repository.rowmapper.PaymentHistoryRowMapper;
import com.mycompany.myapp.repository.rowmapper.PlanRowMapper;
import com.mycompany.myapp.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the PaymentHistory entity.
 */
@SuppressWarnings("unused")
class PaymentHistoryRepositoryInternalImpl implements PaymentHistoryRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final CompanyRowMapper companyMapper;
    private final PlanRowMapper planMapper;
    private final PaymentHistoryRowMapper paymenthistoryMapper;

    private static final Table entityTable = Table.aliased("payment_history", EntityManager.ENTITY_ALIAS);
    private static final Table companyTable = Table.aliased("company", "company");
    private static final Table planTable = Table.aliased("plan", "e_plan");

    public PaymentHistoryRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        CompanyRowMapper companyMapper,
        PlanRowMapper planMapper,
        PaymentHistoryRowMapper paymenthistoryMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.companyMapper = companyMapper;
        this.planMapper = planMapper;
        this.paymenthistoryMapper = paymenthistoryMapper;
    }

    @Override
    public Flux<PaymentHistory> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<PaymentHistory> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<PaymentHistory> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = PaymentHistorySqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(CompanySqlHelper.getColumns(companyTable, "company"));
        columns.addAll(PlanSqlHelper.getColumns(planTable, "plan"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(companyTable)
            .on(Column.create("company_id", entityTable))
            .equals(Column.create("id", companyTable))
            .leftOuterJoin(planTable)
            .on(Column.create("plan_id", entityTable))
            .equals(Column.create("id", planTable));

        String select = entityManager.createSelect(selectFrom, PaymentHistory.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<PaymentHistory> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<PaymentHistory> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private PaymentHistory process(Row row, RowMetadata metadata) {
        PaymentHistory entity = paymenthistoryMapper.apply(row, "e");
        entity.setCompany(companyMapper.apply(row, "company"));
        entity.setPlan(planMapper.apply(row, "plan"));
        return entity;
    }

    @Override
    public <S extends PaymentHistory> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends PaymentHistory> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update PaymentHistory with id = " + entity.getId());
                        }
                        return entity;
                    }
                );
        }
    }

    @Override
    public Mono<Integer> update(PaymentHistory entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}

class PaymentHistorySqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("plan_start_date", table, columnPrefix + "_plan_start_date"));
        columns.add(Column.aliased("plan_end_date", table, columnPrefix + "_plan_end_date"));
        columns.add(Column.aliased("used_session", table, columnPrefix + "_used_session"));
        columns.add(Column.aliased("created_at", table, columnPrefix + "_created_at"));
        columns.add(Column.aliased("updated_at", table, columnPrefix + "_updated_at"));

        columns.add(Column.aliased("company_id", table, columnPrefix + "_company_id"));
        columns.add(Column.aliased("plan_id", table, columnPrefix + "_plan_id"));
        return columns;
    }
}
