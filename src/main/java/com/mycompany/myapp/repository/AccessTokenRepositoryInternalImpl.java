package com.mycompany.myapp.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.mycompany.myapp.domain.AccessToken;
import com.mycompany.myapp.repository.rowmapper.AccessTokenRowMapper;
import com.mycompany.myapp.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoin;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the AccessToken entity.
 */
@SuppressWarnings("unused")
class AccessTokenRepositoryInternalImpl implements AccessTokenRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final AccessTokenRowMapper accesstokenMapper;

    private static final Table entityTable = Table.aliased("access_token", EntityManager.ENTITY_ALIAS);

    public AccessTokenRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        AccessTokenRowMapper accesstokenMapper
    ) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.accesstokenMapper = accesstokenMapper;
    }

    @Override
    public Flux<AccessToken> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<AccessToken> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<AccessToken> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = AccessTokenSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        SelectFromAndJoin selectFrom = Select.builder().select(columns).from(entityTable);

        String select = entityManager.createSelect(selectFrom, AccessToken.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<AccessToken> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<AccessToken> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private AccessToken process(Row row, RowMetadata metadata) {
        AccessToken entity = accesstokenMapper.apply(row, "e");
        return entity;
    }

    @Override
    public <S extends AccessToken> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends AccessToken> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update AccessToken with id = " + entity.getId());
                        }
                        return entity;
                    }
                );
        }
    }

    @Override
    public Mono<Integer> update(AccessToken entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}

class AccessTokenSqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("app_name", table, columnPrefix + "_app_name"));
        columns.add(Column.aliased("token", table, columnPrefix + "_token"));
        columns.add(Column.aliased("token_creation", table, columnPrefix + "_token_creation"));
        columns.add(Column.aliased("token_expire", table, columnPrefix + "_token_expire"));
        columns.add(Column.aliased("user_alies", table, columnPrefix + "_user_alies"));
        columns.add(Column.aliased("created_at", table, columnPrefix + "_created_at"));
        columns.add(Column.aliased("updated_at", table, columnPrefix + "_updated_at"));

        return columns;
    }
}
