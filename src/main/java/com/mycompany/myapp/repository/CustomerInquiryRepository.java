package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CustomerInquiry;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the CustomerInquiry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerInquiryRepository extends R2dbcRepository<CustomerInquiry, Long>, CustomerInquiryRepositoryInternal {
    @Query("SELECT * FROM customer_inquiry entity WHERE entity.customer_id = :id")
    Flux<CustomerInquiry> findByCustomer(Long id);

    @Query("SELECT * FROM customer_inquiry entity WHERE entity.customer_id IS NULL")
    Flux<CustomerInquiry> findAllWhereCustomerIsNull();

    @Query("SELECT * FROM customer_inquiry entity WHERE entity.company_id = :id")
    Flux<CustomerInquiry> findByCompany(Long id);

    @Query("SELECT * FROM customer_inquiry entity WHERE entity.company_id IS NULL")
    Flux<CustomerInquiry> findAllWhereCompanyIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<CustomerInquiry> findAll();

    @Override
    Mono<CustomerInquiry> findById(Long id);

    @Override
    <S extends CustomerInquiry> Mono<S> save(S entity);
}

interface CustomerInquiryRepositoryInternal {
    <S extends CustomerInquiry> Mono<S> insert(S entity);
    <S extends CustomerInquiry> Mono<S> save(S entity);
    Mono<Integer> update(CustomerInquiry entity);

    Flux<CustomerInquiry> findAll();
    Mono<CustomerInquiry> findById(Long id);
    Flux<CustomerInquiry> findAllBy(Pageable pageable);
    Flux<CustomerInquiry> findAllBy(Pageable pageable, Criteria criteria);
}
