package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Channel;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Channel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChannelRepository extends R2dbcRepository<Channel, Long>, ChannelRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<Channel> findAll();

    @Override
    Mono<Channel> findById(Long id);

    @Override
    <S extends Channel> Mono<S> save(S entity);
}

interface ChannelRepositoryInternal {
    <S extends Channel> Mono<S> insert(S entity);
    <S extends Channel> Mono<S> save(S entity);
    Mono<Integer> update(Channel entity);

    Flux<Channel> findAll();
    Mono<Channel> findById(Long id);
    Flux<Channel> findAllBy(Pageable pageable);
    Flux<Channel> findAllBy(Pageable pageable, Criteria criteria);
}
