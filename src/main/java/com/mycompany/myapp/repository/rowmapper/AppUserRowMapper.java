package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.AppUser;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link AppUser}, with proper type conversions.
 */
@Service
public class AppUserRowMapper implements BiFunction<Row, String, AppUser> {

    private final ColumnConverter converter;

    public AppUserRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link AppUser} stored in the database.
     */
    @Override
    public AppUser apply(Row row, String prefix) {
        AppUser entity = new AppUser();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setEmail(converter.fromRow(row, prefix + "_email", String.class));
        entity.setPassword(converter.fromRow(row, prefix + "_password", String.class));
        entity.setAvatarContentType(converter.fromRow(row, prefix + "_avatar_content_type", String.class));
        entity.setAvatar(converter.fromRow(row, prefix + "_avatar", byte[].class));
        entity.setEmailVerifiedAt(converter.fromRow(row, prefix + "_email_verified_at", ZonedDateTime.class));
        entity.setRememberToken(converter.fromRow(row, prefix + "_remember_token", String.class));
        entity.setSetting(converter.fromRow(row, prefix + "_setting", String.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        entity.setAccessTokenId(converter.fromRow(row, prefix + "_access_token_id", Long.class));
        return entity;
    }
}
