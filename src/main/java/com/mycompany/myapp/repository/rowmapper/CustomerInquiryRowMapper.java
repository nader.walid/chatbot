package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.CustomerInquiry;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link CustomerInquiry}, with proper type conversions.
 */
@Service
public class CustomerInquiryRowMapper implements BiFunction<Row, String, CustomerInquiry> {

    private final ColumnConverter converter;

    public CustomerInquiryRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link CustomerInquiry} stored in the database.
     */
    @Override
    public CustomerInquiry apply(Row row, String prefix) {
        CustomerInquiry entity = new CustomerInquiry();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setBody(converter.fromRow(row, prefix + "_body", String.class));
        entity.setStatus(converter.fromRow(row, prefix + "_status", String.class));
        entity.setReason(converter.fromRow(row, prefix + "_reason", String.class));
        entity.setCallDay(converter.fromRow(row, prefix + "_call_day", ZonedDateTime.class));
        entity.setCallTime(converter.fromRow(row, prefix + "_call_time", ZonedDateTime.class));
        entity.setCallPhone(converter.fromRow(row, prefix + "_call_phone", ZonedDateTime.class));
        entity.setStatusNotes(converter.fromRow(row, prefix + "_status_notes", String.class));
        entity.setNextActivity(converter.fromRow(row, prefix + "_next_activity", Long.class));
        entity.setLastActivity(converter.fromRow(row, prefix + "_last_activity", Long.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        entity.setCustomerId(converter.fromRow(row, prefix + "_customer_id", Long.class));
        entity.setCompanyId(converter.fromRow(row, prefix + "_company_id", Long.class));
        return entity;
    }
}
