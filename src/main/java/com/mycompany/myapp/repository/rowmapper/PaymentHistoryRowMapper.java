package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.PaymentHistory;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link PaymentHistory}, with proper type conversions.
 */
@Service
public class PaymentHistoryRowMapper implements BiFunction<Row, String, PaymentHistory> {

    private final ColumnConverter converter;

    public PaymentHistoryRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link PaymentHistory} stored in the database.
     */
    @Override
    public PaymentHistory apply(Row row, String prefix) {
        PaymentHistory entity = new PaymentHistory();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setPlanStartDate(converter.fromRow(row, prefix + "_plan_start_date", ZonedDateTime.class));
        entity.setPlanEndDate(converter.fromRow(row, prefix + "_plan_end_date", ZonedDateTime.class));
        entity.setUsedSession(converter.fromRow(row, prefix + "_used_session", Long.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        entity.setCompanyId(converter.fromRow(row, prefix + "_company_id", Long.class));
        entity.setPlanId(converter.fromRow(row, prefix + "_plan_id", Long.class));
        return entity;
    }
}
