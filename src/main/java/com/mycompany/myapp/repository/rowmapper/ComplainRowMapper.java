package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.Complain;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Complain}, with proper type conversions.
 */
@Service
public class ComplainRowMapper implements BiFunction<Row, String, Complain> {

    private final ColumnConverter converter;

    public ComplainRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Complain} stored in the database.
     */
    @Override
    public Complain apply(Row row, String prefix) {
        Complain entity = new Complain();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setTitle(converter.fromRow(row, prefix + "_title", String.class));
        entity.setData(converter.fromRow(row, prefix + "_data", String.class));
        entity.setStatus(converter.fromRow(row, prefix + "_status", String.class));
        entity.setCallPhone(converter.fromRow(row, prefix + "_call_phone", String.class));
        entity.setSalesNotes(converter.fromRow(row, prefix + "_sales_notes", String.class));
        entity.setNextActivity(converter.fromRow(row, prefix + "_next_activity", ZonedDateTime.class));
        entity.setLastActivity(converter.fromRow(row, prefix + "_last_activity", ZonedDateTime.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        entity.setCustomerId(converter.fromRow(row, prefix + "_customer_id", Long.class));
        entity.setCompanyId(converter.fromRow(row, prefix + "_company_id", Long.class));
        return entity;
    }
}
