package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.Company;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Company}, with proper type conversions.
 */
@Service
public class CompanyRowMapper implements BiFunction<Row, String, Company> {

    private final ColumnConverter converter;

    public CompanyRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Company} stored in the database.
     */
    @Override
    public Company apply(Row row, String prefix) {
        Company entity = new Company();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setEmail(converter.fromRow(row, prefix + "_email", String.class));
        entity.setWelcomeMessage(converter.fromRow(row, prefix + "_welcome_message", String.class));
        entity.setWebsite(converter.fromRow(row, prefix + "_website", String.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setPartnerText(converter.fromRow(row, prefix + "_partner_text", String.class));
        entity.setProductsText(converter.fromRow(row, prefix + "_products_text", String.class));
        entity.setAbout(converter.fromRow(row, prefix + "_about", String.class));
        entity.setUsedSessions(converter.fromRow(row, prefix + "_used_sessions", Long.class));
        entity.setPlanStartDate(converter.fromRow(row, prefix + "_plan_start_date", ZonedDateTime.class));
        entity.setPlanEndDate(converter.fromRow(row, prefix + "_plan_end_date", ZonedDateTime.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        entity.setAppUserId(converter.fromRow(row, prefix + "_app_user_id", Long.class));
        entity.setPlanId(converter.fromRow(row, prefix + "_plan_id", Long.class));
        return entity;
    }
}
