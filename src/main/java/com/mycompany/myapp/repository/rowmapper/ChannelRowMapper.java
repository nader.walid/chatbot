package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.Channel;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Channel}, with proper type conversions.
 */
@Service
public class ChannelRowMapper implements BiFunction<Row, String, Channel> {

    private final ColumnConverter converter;

    public ChannelRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Channel} stored in the database.
     */
    @Override
    public Channel apply(Row row, String prefix) {
        Channel entity = new Channel();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        return entity;
    }
}
