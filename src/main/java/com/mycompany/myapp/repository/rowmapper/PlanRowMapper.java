package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.Plan;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Plan}, with proper type conversions.
 */
@Service
public class PlanRowMapper implements BiFunction<Row, String, Plan> {

    private final ColumnConverter converter;

    public PlanRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Plan} stored in the database.
     */
    @Override
    public Plan apply(Row row, String prefix) {
        Plan entity = new Plan();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setMaxUsers(converter.fromRow(row, prefix + "_max_users", Long.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        return entity;
    }
}
