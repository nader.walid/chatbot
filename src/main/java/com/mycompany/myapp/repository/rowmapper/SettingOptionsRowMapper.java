package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.SettingOptions;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link SettingOptions}, with proper type conversions.
 */
@Service
public class SettingOptionsRowMapper implements BiFunction<Row, String, SettingOptions> {

    private final ColumnConverter converter;

    public SettingOptionsRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link SettingOptions} stored in the database.
     */
    @Override
    public SettingOptions apply(Row row, String prefix) {
        SettingOptions entity = new SettingOptions();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        return entity;
    }
}
