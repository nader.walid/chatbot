package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Customer}, with proper type conversions.
 */
@Service
public class CustomerRowMapper implements BiFunction<Row, String, Customer> {

    private final ColumnConverter converter;

    public CustomerRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Customer} stored in the database.
     */
    @Override
    public Customer apply(Row row, String prefix) {
        Customer entity = new Customer();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setPhone(converter.fromRow(row, prefix + "_phone", String.class));
        entity.setPhone2(converter.fromRow(row, prefix + "_phone_2", String.class));
        entity.setPhone3(converter.fromRow(row, prefix + "_phone_3", String.class));
        entity.setEmail(converter.fromRow(row, prefix + "_email", String.class));
        entity.setCompany(converter.fromRow(row, prefix + "_company", String.class));
        entity.setJobTitle(converter.fromRow(row, prefix + "_job_title", String.class));
        entity.setNextActivity(converter.fromRow(row, prefix + "_next_activity", ZonedDateTime.class));
        entity.setLastActivity(converter.fromRow(row, prefix + "_last_activity", ZonedDateTime.class));
        entity.setSalesNotes(converter.fromRow(row, prefix + "_sales_notes", String.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        entity.setChannelId(converter.fromRow(row, prefix + "_channel_id", Long.class));
        return entity;
    }
}
