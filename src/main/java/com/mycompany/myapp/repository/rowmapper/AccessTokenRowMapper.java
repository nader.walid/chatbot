package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.AccessToken;
import com.mycompany.myapp.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link AccessToken}, with proper type conversions.
 */
@Service
public class AccessTokenRowMapper implements BiFunction<Row, String, AccessToken> {

    private final ColumnConverter converter;

    public AccessTokenRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link AccessToken} stored in the database.
     */
    @Override
    public AccessToken apply(Row row, String prefix) {
        AccessToken entity = new AccessToken();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setAppName(converter.fromRow(row, prefix + "_app_name", String.class));
        entity.setToken(converter.fromRow(row, prefix + "_token", String.class));
        entity.setTokenCreation(converter.fromRow(row, prefix + "_token_creation", ZonedDateTime.class));
        entity.setTokenExpire(converter.fromRow(row, prefix + "_token_expire", ZonedDateTime.class));
        entity.setUserAlies(converter.fromRow(row, prefix + "_user_alies", String.class));
        entity.setCreatedAt(converter.fromRow(row, prefix + "_created_at", ZonedDateTime.class));
        entity.setUpdatedAt(converter.fromRow(row, prefix + "_updated_at", ZonedDateTime.class));
        return entity;
    }
}
