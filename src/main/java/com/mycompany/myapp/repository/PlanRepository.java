package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Plan;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Plan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlanRepository extends R2dbcRepository<Plan, Long>, PlanRepositoryInternal {
    // just to avoid having unambigous methods
    @Override
    Flux<Plan> findAll();

    @Override
    Mono<Plan> findById(Long id);

    @Override
    <S extends Plan> Mono<S> save(S entity);
}

interface PlanRepositoryInternal {
    <S extends Plan> Mono<S> insert(S entity);
    <S extends Plan> Mono<S> save(S entity);
    Mono<Integer> update(Plan entity);

    Flux<Plan> findAll();
    Mono<Plan> findById(Long id);
    Flux<Plan> findAllBy(Pageable pageable);
    Flux<Plan> findAllBy(Pageable pageable, Criteria criteria);
}
