package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A CustomerInquiry.
 */
@Table("customer_inquiry")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "customerinquiry")
public class CustomerInquiry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column("body")
    private String body;

    @NotNull(message = "must not be null")
    @Column("status")
    private String status;

    @NotNull(message = "must not be null")
    @Column("reason")
    private String reason;

    @Column("call_day")
    private ZonedDateTime callDay;

    @Column("call_time")
    private ZonedDateTime callTime;

    @Column("call_phone")
    private ZonedDateTime callPhone;

    @Column("status_notes")
    private String statusNotes;

    @Column("next_activity")
    private Long nextActivity;

    @Column("last_activity")
    private Long lastActivity;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @JsonIgnoreProperties(value = { "complains", "customerInquiries", "channel" }, allowSetters = true)
    @Transient
    private Customer customer;

    @Column("customer_id")
    private Long customerId;

    @JsonIgnoreProperties(
        value = {
            "products", "complains", "paymentHistories", "customerInquiries", "categories", "settingOptions", "appUser", "plan", "partners",
        },
        allowSetters = true
    )
    @Transient
    private Company company;

    @Column("company_id")
    private Long companyId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerInquiry id(Long id) {
        this.id = id;
        return this;
    }

    public String getBody() {
        return this.body;
    }

    public CustomerInquiry body(String body) {
        this.body = body;
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return this.status;
    }

    public CustomerInquiry status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return this.reason;
    }

    public CustomerInquiry reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ZonedDateTime getCallDay() {
        return this.callDay;
    }

    public CustomerInquiry callDay(ZonedDateTime callDay) {
        this.callDay = callDay;
        return this;
    }

    public void setCallDay(ZonedDateTime callDay) {
        this.callDay = callDay;
    }

    public ZonedDateTime getCallTime() {
        return this.callTime;
    }

    public CustomerInquiry callTime(ZonedDateTime callTime) {
        this.callTime = callTime;
        return this;
    }

    public void setCallTime(ZonedDateTime callTime) {
        this.callTime = callTime;
    }

    public ZonedDateTime getCallPhone() {
        return this.callPhone;
    }

    public CustomerInquiry callPhone(ZonedDateTime callPhone) {
        this.callPhone = callPhone;
        return this;
    }

    public void setCallPhone(ZonedDateTime callPhone) {
        this.callPhone = callPhone;
    }

    public String getStatusNotes() {
        return this.statusNotes;
    }

    public CustomerInquiry statusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
        return this;
    }

    public void setStatusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
    }

    public Long getNextActivity() {
        return this.nextActivity;
    }

    public CustomerInquiry nextActivity(Long nextActivity) {
        this.nextActivity = nextActivity;
        return this;
    }

    public void setNextActivity(Long nextActivity) {
        this.nextActivity = nextActivity;
    }

    public Long getLastActivity() {
        return this.lastActivity;
    }

    public CustomerInquiry lastActivity(Long lastActivity) {
        this.lastActivity = lastActivity;
        return this;
    }

    public void setLastActivity(Long lastActivity) {
        this.lastActivity = lastActivity;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public CustomerInquiry createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public CustomerInquiry updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public CustomerInquiry customer(Customer customer) {
        this.setCustomer(customer);
        this.customerId = customer != null ? customer.getId() : null;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer != null ? customer.getId() : null;
    }

    public Long getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(Long customer) {
        this.customerId = customer;
    }

    public Company getCompany() {
        return this.company;
    }

    public CustomerInquiry company(Company company) {
        this.setCompany(company);
        this.companyId = company != null ? company.getId() : null;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
        this.companyId = company != null ? company.getId() : null;
    }

    public Long getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(Long company) {
        this.companyId = company;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerInquiry)) {
            return false;
        }
        return id != null && id.equals(((CustomerInquiry) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerInquiry{" +
            "id=" + getId() +
            ", body='" + getBody() + "'" +
            ", status='" + getStatus() + "'" +
            ", reason='" + getReason() + "'" +
            ", callDay='" + getCallDay() + "'" +
            ", callTime='" + getCallTime() + "'" +
            ", callPhone='" + getCallPhone() + "'" +
            ", statusNotes='" + getStatusNotes() + "'" +
            ", nextActivity=" + getNextActivity() +
            ", lastActivity=" + getLastActivity() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
