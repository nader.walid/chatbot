package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Plan.
 */
@Table("plan")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "plan")
public class Plan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("name")
    private String name;

    @NotNull(message = "must not be null")
    @Column("max_users")
    private Long maxUsers;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @Transient
    @JsonIgnoreProperties(
        value = {
            "products", "complains", "paymentHistories", "customerInquiries", "categories", "settingOptions", "appUser", "plan", "partners",
        },
        allowSetters = true
    )
    private Set<Company> companies = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "company", "plan" }, allowSetters = true)
    private Set<PaymentHistory> paymentHistories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Plan id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Plan name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMaxUsers() {
        return this.maxUsers;
    }

    public Plan maxUsers(Long maxUsers) {
        this.maxUsers = maxUsers;
        return this;
    }

    public void setMaxUsers(Long maxUsers) {
        this.maxUsers = maxUsers;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public Plan createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public Plan updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<Company> getCompanies() {
        return this.companies;
    }

    public Plan companies(Set<Company> companies) {
        this.setCompanies(companies);
        return this;
    }

    public Plan addCompany(Company company) {
        this.companies.add(company);
        company.setPlan(this);
        return this;
    }

    public Plan removeCompany(Company company) {
        this.companies.remove(company);
        company.setPlan(null);
        return this;
    }

    public void setCompanies(Set<Company> companies) {
        if (this.companies != null) {
            this.companies.forEach(i -> i.setPlan(null));
        }
        if (companies != null) {
            companies.forEach(i -> i.setPlan(this));
        }
        this.companies = companies;
    }

    public Set<PaymentHistory> getPaymentHistories() {
        return this.paymentHistories;
    }

    public Plan paymentHistories(Set<PaymentHistory> paymentHistories) {
        this.setPaymentHistories(paymentHistories);
        return this;
    }

    public Plan addPaymentHistory(PaymentHistory paymentHistory) {
        this.paymentHistories.add(paymentHistory);
        paymentHistory.setPlan(this);
        return this;
    }

    public Plan removePaymentHistory(PaymentHistory paymentHistory) {
        this.paymentHistories.remove(paymentHistory);
        paymentHistory.setPlan(null);
        return this;
    }

    public void setPaymentHistories(Set<PaymentHistory> paymentHistories) {
        if (this.paymentHistories != null) {
            this.paymentHistories.forEach(i -> i.setPlan(null));
        }
        if (paymentHistories != null) {
            paymentHistories.forEach(i -> i.setPlan(this));
        }
        this.paymentHistories = paymentHistories;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Plan)) {
            return false;
        }
        return id != null && id.equals(((Plan) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Plan{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", maxUsers=" + getMaxUsers() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
