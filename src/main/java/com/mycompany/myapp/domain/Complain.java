package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Complain.
 */
@Table("complain")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "complain")
public class Complain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("title")
    private String title;

    @Column("data")
    private String data;

    @NotNull(message = "must not be null")
    @Column("status")
    private String status;

    @Column("call_phone")
    private String callPhone;

    @Column("sales_notes")
    private String salesNotes;

    @Column("next_activity")
    private ZonedDateTime nextActivity;

    @Column("last_activity")
    private ZonedDateTime lastActivity;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @JsonIgnoreProperties(value = { "complains", "customerInquiries", "channel" }, allowSetters = true)
    @Transient
    private Customer customer;

    @Column("customer_id")
    private Long customerId;

    @JsonIgnoreProperties(
        value = {
            "products", "complains", "paymentHistories", "customerInquiries", "categories", "settingOptions", "appUser", "plan", "partners",
        },
        allowSetters = true
    )
    @Transient
    private Company company;

    @Column("company_id")
    private Long companyId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Complain id(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public Complain title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return this.data;
    }

    public Complain data(String data) {
        this.data = data;
        return this;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return this.status;
    }

    public Complain status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCallPhone() {
        return this.callPhone;
    }

    public Complain callPhone(String callPhone) {
        this.callPhone = callPhone;
        return this;
    }

    public void setCallPhone(String callPhone) {
        this.callPhone = callPhone;
    }

    public String getSalesNotes() {
        return this.salesNotes;
    }

    public Complain salesNotes(String salesNotes) {
        this.salesNotes = salesNotes;
        return this;
    }

    public void setSalesNotes(String salesNotes) {
        this.salesNotes = salesNotes;
    }

    public ZonedDateTime getNextActivity() {
        return this.nextActivity;
    }

    public Complain nextActivity(ZonedDateTime nextActivity) {
        this.nextActivity = nextActivity;
        return this;
    }

    public void setNextActivity(ZonedDateTime nextActivity) {
        this.nextActivity = nextActivity;
    }

    public ZonedDateTime getLastActivity() {
        return this.lastActivity;
    }

    public Complain lastActivity(ZonedDateTime lastActivity) {
        this.lastActivity = lastActivity;
        return this;
    }

    public void setLastActivity(ZonedDateTime lastActivity) {
        this.lastActivity = lastActivity;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public Complain createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public Complain updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public Complain customer(Customer customer) {
        this.setCustomer(customer);
        this.customerId = customer != null ? customer.getId() : null;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer != null ? customer.getId() : null;
    }

    public Long getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(Long customer) {
        this.customerId = customer;
    }

    public Company getCompany() {
        return this.company;
    }

    public Complain company(Company company) {
        this.setCompany(company);
        this.companyId = company != null ? company.getId() : null;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
        this.companyId = company != null ? company.getId() : null;
    }

    public Long getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(Long company) {
        this.companyId = company;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Complain)) {
            return false;
        }
        return id != null && id.equals(((Complain) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Complain{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", data='" + getData() + "'" +
            ", status='" + getStatus() + "'" +
            ", callPhone='" + getCallPhone() + "'" +
            ", salesNotes='" + getSalesNotes() + "'" +
            ", nextActivity='" + getNextActivity() + "'" +
            ", lastActivity='" + getLastActivity() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
