package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A AccessToken.
 */
@Table("access_token")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "accesstoken")
public class AccessToken implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("app_name")
    private String appName;

    @Column("token")
    private String token;

    @Column("token_creation")
    private ZonedDateTime tokenCreation;

    @Column("token_expire")
    private ZonedDateTime tokenExpire;

    @Column("user_alies")
    private String userAlies;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @Transient
    private AppUser appUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccessToken id(Long id) {
        this.id = id;
        return this;
    }

    public String getAppName() {
        return this.appName;
    }

    public AccessToken appName(String appName) {
        this.appName = appName;
        return this;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getToken() {
        return this.token;
    }

    public AccessToken token(String token) {
        this.token = token;
        return this;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ZonedDateTime getTokenCreation() {
        return this.tokenCreation;
    }

    public AccessToken tokenCreation(ZonedDateTime tokenCreation) {
        this.tokenCreation = tokenCreation;
        return this;
    }

    public void setTokenCreation(ZonedDateTime tokenCreation) {
        this.tokenCreation = tokenCreation;
    }

    public ZonedDateTime getTokenExpire() {
        return this.tokenExpire;
    }

    public AccessToken tokenExpire(ZonedDateTime tokenExpire) {
        this.tokenExpire = tokenExpire;
        return this;
    }

    public void setTokenExpire(ZonedDateTime tokenExpire) {
        this.tokenExpire = tokenExpire;
    }

    public String getUserAlies() {
        return this.userAlies;
    }

    public AccessToken userAlies(String userAlies) {
        this.userAlies = userAlies;
        return this;
    }

    public void setUserAlies(String userAlies) {
        this.userAlies = userAlies;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public AccessToken createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public AccessToken updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public AppUser getAppUser() {
        return this.appUser;
    }

    public AccessToken appUser(AppUser appUser) {
        this.setAppUser(appUser);
        return this;
    }

    public void setAppUser(AppUser appUser) {
        if (this.appUser != null) {
            this.appUser.setAccessToken(null);
        }
        if (appUser != null) {
            appUser.setAccessToken(this);
        }
        this.appUser = appUser;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccessToken)) {
            return false;
        }
        return id != null && id.equals(((AccessToken) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccessToken{" +
            "id=" + getId() +
            ", appName='" + getAppName() + "'" +
            ", token='" + getToken() + "'" +
            ", tokenCreation='" + getTokenCreation() + "'" +
            ", tokenExpire='" + getTokenExpire() + "'" +
            ", userAlies='" + getUserAlies() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
