package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A PaymentHistory.
 */
@Table("payment_history")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "paymenthistory")
public class PaymentHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("plan_start_date")
    private ZonedDateTime planStartDate;

    @NotNull(message = "must not be null")
    @Column("plan_end_date")
    private ZonedDateTime planEndDate;

    @NotNull(message = "must not be null")
    @Column("used_session")
    private Long usedSession;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @JsonIgnoreProperties(
        value = {
            "products", "complains", "paymentHistories", "customerInquiries", "categories", "settingOptions", "appUser", "plan", "partners",
        },
        allowSetters = true
    )
    @Transient
    private Company company;

    @Column("company_id")
    private Long companyId;

    @JsonIgnoreProperties(value = { "companies", "paymentHistories" }, allowSetters = true)
    @Transient
    private Plan plan;

    @Column("plan_id")
    private Long planId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentHistory id(Long id) {
        this.id = id;
        return this;
    }

    public ZonedDateTime getPlanStartDate() {
        return this.planStartDate;
    }

    public PaymentHistory planStartDate(ZonedDateTime planStartDate) {
        this.planStartDate = planStartDate;
        return this;
    }

    public void setPlanStartDate(ZonedDateTime planStartDate) {
        this.planStartDate = planStartDate;
    }

    public ZonedDateTime getPlanEndDate() {
        return this.planEndDate;
    }

    public PaymentHistory planEndDate(ZonedDateTime planEndDate) {
        this.planEndDate = planEndDate;
        return this;
    }

    public void setPlanEndDate(ZonedDateTime planEndDate) {
        this.planEndDate = planEndDate;
    }

    public Long getUsedSession() {
        return this.usedSession;
    }

    public PaymentHistory usedSession(Long usedSession) {
        this.usedSession = usedSession;
        return this;
    }

    public void setUsedSession(Long usedSession) {
        this.usedSession = usedSession;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public PaymentHistory createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public PaymentHistory updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Company getCompany() {
        return this.company;
    }

    public PaymentHistory company(Company company) {
        this.setCompany(company);
        this.companyId = company != null ? company.getId() : null;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
        this.companyId = company != null ? company.getId() : null;
    }

    public Long getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(Long company) {
        this.companyId = company;
    }

    public Plan getPlan() {
        return this.plan;
    }

    public PaymentHistory plan(Plan plan) {
        this.setPlan(plan);
        this.planId = plan != null ? plan.getId() : null;
        return this;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
        this.planId = plan != null ? plan.getId() : null;
    }

    public Long getPlanId() {
        return this.planId;
    }

    public void setPlanId(Long plan) {
        this.planId = plan;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentHistory)) {
            return false;
        }
        return id != null && id.equals(((PaymentHistory) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentHistory{" +
            "id=" + getId() +
            ", planStartDate='" + getPlanStartDate() + "'" +
            ", planEndDate='" + getPlanEndDate() + "'" +
            ", usedSession=" + getUsedSession() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
