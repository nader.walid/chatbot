package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Company.
 */
@Table("company")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "company")
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("email")
    private String email;

    @Column("welcome_message")
    private String welcomeMessage;

    @Column("website")
    private String website;

    @NotNull(message = "must not be null")
    @Column("name")
    private String name;

    @Column("partner_text")
    private String partnerText;

    @Column("products_text")
    private String productsText;

    @Column("about")
    private String about;

    @Column("used_sessions")
    private Long usedSessions;

    @Column("plan_start_date")
    private ZonedDateTime planStartDate;

    @Column("plan_end_date")
    private ZonedDateTime planEndDate;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @Transient
    @JsonIgnoreProperties(value = { "categories", "company" }, allowSetters = true)
    private Set<Product> products = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "customer", "company" }, allowSetters = true)
    private Set<Complain> complains = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "company", "plan" }, allowSetters = true)
    private Set<PaymentHistory> paymentHistories = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "customer", "company" }, allowSetters = true)
    private Set<CustomerInquiry> customerInquiries = new HashSet<>();

    @JsonIgnoreProperties(value = { "products", "companies" }, allowSetters = true)
    @Transient
    private Set<Category> categories = new HashSet<>();

    @JsonIgnoreProperties(value = { "companies" }, allowSetters = true)
    @Transient
    private Set<SettingOptions> settingOptions = new HashSet<>();

    @JsonIgnoreProperties(value = { "accessToken", "companies", "roles" }, allowSetters = true)
    @Transient
    private AppUser appUser;

    @Column("app_user_id")
    private Long appUserId;

    @JsonIgnoreProperties(value = { "companies", "paymentHistories" }, allowSetters = true)
    @Transient
    private Plan plan;

    @Column("plan_id")
    private Long planId;

    @JsonIgnoreProperties(value = { "companies" }, allowSetters = true)
    @Transient
    private Set<Partner> partners = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company id(Long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return this.email;
    }

    public Company email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWelcomeMessage() {
        return this.welcomeMessage;
    }

    public Company welcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
        return this;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public String getWebsite() {
        return this.website;
    }

    public Company website(String website) {
        this.website = website;
        return this;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getName() {
        return this.name;
    }

    public Company name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartnerText() {
        return this.partnerText;
    }

    public Company partnerText(String partnerText) {
        this.partnerText = partnerText;
        return this;
    }

    public void setPartnerText(String partnerText) {
        this.partnerText = partnerText;
    }

    public String getProductsText() {
        return this.productsText;
    }

    public Company productsText(String productsText) {
        this.productsText = productsText;
        return this;
    }

    public void setProductsText(String productsText) {
        this.productsText = productsText;
    }

    public String getAbout() {
        return this.about;
    }

    public Company about(String about) {
        this.about = about;
        return this;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Long getUsedSessions() {
        return this.usedSessions;
    }

    public Company usedSessions(Long usedSessions) {
        this.usedSessions = usedSessions;
        return this;
    }

    public void setUsedSessions(Long usedSessions) {
        this.usedSessions = usedSessions;
    }

    public ZonedDateTime getPlanStartDate() {
        return this.planStartDate;
    }

    public Company planStartDate(ZonedDateTime planStartDate) {
        this.planStartDate = planStartDate;
        return this;
    }

    public void setPlanStartDate(ZonedDateTime planStartDate) {
        this.planStartDate = planStartDate;
    }

    public ZonedDateTime getPlanEndDate() {
        return this.planEndDate;
    }

    public Company planEndDate(ZonedDateTime planEndDate) {
        this.planEndDate = planEndDate;
        return this;
    }

    public void setPlanEndDate(ZonedDateTime planEndDate) {
        this.planEndDate = planEndDate;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public Company createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public Company updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public Company products(Set<Product> products) {
        this.setProducts(products);
        return this;
    }

    public Company addProduct(Product product) {
        this.products.add(product);
        product.setCompany(this);
        return this;
    }

    public Company removeProduct(Product product) {
        this.products.remove(product);
        product.setCompany(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        if (this.products != null) {
            this.products.forEach(i -> i.setCompany(null));
        }
        if (products != null) {
            products.forEach(i -> i.setCompany(this));
        }
        this.products = products;
    }

    public Set<Complain> getComplains() {
        return this.complains;
    }

    public Company complains(Set<Complain> complains) {
        this.setComplains(complains);
        return this;
    }

    public Company addComplain(Complain complain) {
        this.complains.add(complain);
        complain.setCompany(this);
        return this;
    }

    public Company removeComplain(Complain complain) {
        this.complains.remove(complain);
        complain.setCompany(null);
        return this;
    }

    public void setComplains(Set<Complain> complains) {
        if (this.complains != null) {
            this.complains.forEach(i -> i.setCompany(null));
        }
        if (complains != null) {
            complains.forEach(i -> i.setCompany(this));
        }
        this.complains = complains;
    }

    public Set<PaymentHistory> getPaymentHistories() {
        return this.paymentHistories;
    }

    public Company paymentHistories(Set<PaymentHistory> paymentHistories) {
        this.setPaymentHistories(paymentHistories);
        return this;
    }

    public Company addPaymentHistory(PaymentHistory paymentHistory) {
        this.paymentHistories.add(paymentHistory);
        paymentHistory.setCompany(this);
        return this;
    }

    public Company removePaymentHistory(PaymentHistory paymentHistory) {
        this.paymentHistories.remove(paymentHistory);
        paymentHistory.setCompany(null);
        return this;
    }

    public void setPaymentHistories(Set<PaymentHistory> paymentHistories) {
        if (this.paymentHistories != null) {
            this.paymentHistories.forEach(i -> i.setCompany(null));
        }
        if (paymentHistories != null) {
            paymentHistories.forEach(i -> i.setCompany(this));
        }
        this.paymentHistories = paymentHistories;
    }

    public Set<CustomerInquiry> getCustomerInquiries() {
        return this.customerInquiries;
    }

    public Company customerInquiries(Set<CustomerInquiry> customerInquiries) {
        this.setCustomerInquiries(customerInquiries);
        return this;
    }

    public Company addCustomerInquiry(CustomerInquiry customerInquiry) {
        this.customerInquiries.add(customerInquiry);
        customerInquiry.setCompany(this);
        return this;
    }

    public Company removeCustomerInquiry(CustomerInquiry customerInquiry) {
        this.customerInquiries.remove(customerInquiry);
        customerInquiry.setCompany(null);
        return this;
    }

    public void setCustomerInquiries(Set<CustomerInquiry> customerInquiries) {
        if (this.customerInquiries != null) {
            this.customerInquiries.forEach(i -> i.setCompany(null));
        }
        if (customerInquiries != null) {
            customerInquiries.forEach(i -> i.setCompany(this));
        }
        this.customerInquiries = customerInquiries;
    }

    public Set<Category> getCategories() {
        return this.categories;
    }

    public Company categories(Set<Category> categories) {
        this.setCategories(categories);
        return this;
    }

    public Company addCategory(Category category) {
        this.categories.add(category);
        category.getCompanies().add(this);
        return this;
    }

    public Company removeCategory(Category category) {
        this.categories.remove(category);
        category.getCompanies().remove(this);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<SettingOptions> getSettingOptions() {
        return this.settingOptions;
    }

    public Company settingOptions(Set<SettingOptions> settingOptions) {
        this.setSettingOptions(settingOptions);
        return this;
    }

    public Company addSettingOptions(SettingOptions settingOptions) {
        this.settingOptions.add(settingOptions);
        settingOptions.getCompanies().add(this);
        return this;
    }

    public Company removeSettingOptions(SettingOptions settingOptions) {
        this.settingOptions.remove(settingOptions);
        settingOptions.getCompanies().remove(this);
        return this;
    }

    public void setSettingOptions(Set<SettingOptions> settingOptions) {
        this.settingOptions = settingOptions;
    }

    public AppUser getAppUser() {
        return this.appUser;
    }

    public Company appUser(AppUser appUser) {
        this.setAppUser(appUser);
        this.appUserId = appUser != null ? appUser.getId() : null;
        return this;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
        this.appUserId = appUser != null ? appUser.getId() : null;
    }

    public Long getAppUserId() {
        return this.appUserId;
    }

    public void setAppUserId(Long appUser) {
        this.appUserId = appUser;
    }

    public Plan getPlan() {
        return this.plan;
    }

    public Company plan(Plan plan) {
        this.setPlan(plan);
        this.planId = plan != null ? plan.getId() : null;
        return this;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
        this.planId = plan != null ? plan.getId() : null;
    }

    public Long getPlanId() {
        return this.planId;
    }

    public void setPlanId(Long plan) {
        this.planId = plan;
    }

    public Set<Partner> getPartners() {
        return this.partners;
    }

    public Company partners(Set<Partner> partners) {
        this.setPartners(partners);
        return this;
    }

    public Company addPartner(Partner partner) {
        this.partners.add(partner);
        partner.getCompanies().add(this);
        return this;
    }

    public Company removePartner(Partner partner) {
        this.partners.remove(partner);
        partner.getCompanies().remove(this);
        return this;
    }

    public void setPartners(Set<Partner> partners) {
        if (this.partners != null) {
            this.partners.forEach(i -> i.removeCompany(this));
        }
        if (partners != null) {
            partners.forEach(i -> i.addCompany(this));
        }
        this.partners = partners;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Company)) {
            return false;
        }
        return id != null && id.equals(((Company) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Company{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", welcomeMessage='" + getWelcomeMessage() + "'" +
            ", website='" + getWebsite() + "'" +
            ", name='" + getName() + "'" +
            ", partnerText='" + getPartnerText() + "'" +
            ", productsText='" + getProductsText() + "'" +
            ", about='" + getAbout() + "'" +
            ", usedSessions=" + getUsedSessions() +
            ", planStartDate='" + getPlanStartDate() + "'" +
            ", planEndDate='" + getPlanEndDate() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
