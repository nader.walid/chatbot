package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Customer.
 */
@Table("customer")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("name")
    private String name;

    @Column("phone")
    private String phone;

    @Column("phone_2")
    private String phone2;

    @Column("phone_3")
    private String phone3;

    @NotNull(message = "must not be null")
    @Column("email")
    private String email;

    @Column("company")
    private String company;

    @Column("job_title")
    private String jobTitle;

    @Column("next_activity")
    private ZonedDateTime nextActivity;

    @Column("last_activity")
    private ZonedDateTime lastActivity;

    @Column("sales_notes")
    private String salesNotes;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    @Transient
    @JsonIgnoreProperties(value = { "customer", "company" }, allowSetters = true)
    private Set<Complain> complains = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "customer", "company" }, allowSetters = true)
    private Set<CustomerInquiry> customerInquiries = new HashSet<>();

    @JsonIgnoreProperties(value = { "customers" }, allowSetters = true)
    @Transient
    private Channel channel;

    @Column("channel_id")
    private Long channelId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Customer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return this.phone;
    }

    public Customer phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return this.phone2;
    }

    public Customer phone2(String phone2) {
        this.phone2 = phone2;
        return this;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone3() {
        return this.phone3;
    }

    public Customer phone3(String phone3) {
        this.phone3 = phone3;
        return this;
    }

    public void setPhone3(String phone3) {
        this.phone3 = phone3;
    }

    public String getEmail() {
        return this.email;
    }

    public Customer email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return this.company;
    }

    public Customer company(String company) {
        this.company = company;
        return this;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public Customer jobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public ZonedDateTime getNextActivity() {
        return this.nextActivity;
    }

    public Customer nextActivity(ZonedDateTime nextActivity) {
        this.nextActivity = nextActivity;
        return this;
    }

    public void setNextActivity(ZonedDateTime nextActivity) {
        this.nextActivity = nextActivity;
    }

    public ZonedDateTime getLastActivity() {
        return this.lastActivity;
    }

    public Customer lastActivity(ZonedDateTime lastActivity) {
        this.lastActivity = lastActivity;
        return this;
    }

    public void setLastActivity(ZonedDateTime lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getSalesNotes() {
        return this.salesNotes;
    }

    public Customer salesNotes(String salesNotes) {
        this.salesNotes = salesNotes;
        return this;
    }

    public void setSalesNotes(String salesNotes) {
        this.salesNotes = salesNotes;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public Customer createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public Customer updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<Complain> getComplains() {
        return this.complains;
    }

    public Customer complains(Set<Complain> complains) {
        this.setComplains(complains);
        return this;
    }

    public Customer addComplain(Complain complain) {
        this.complains.add(complain);
        complain.setCustomer(this);
        return this;
    }

    public Customer removeComplain(Complain complain) {
        this.complains.remove(complain);
        complain.setCustomer(null);
        return this;
    }

    public void setComplains(Set<Complain> complains) {
        if (this.complains != null) {
            this.complains.forEach(i -> i.setCustomer(null));
        }
        if (complains != null) {
            complains.forEach(i -> i.setCustomer(this));
        }
        this.complains = complains;
    }

    public Set<CustomerInquiry> getCustomerInquiries() {
        return this.customerInquiries;
    }

    public Customer customerInquiries(Set<CustomerInquiry> customerInquiries) {
        this.setCustomerInquiries(customerInquiries);
        return this;
    }

    public Customer addCustomerInquiry(CustomerInquiry customerInquiry) {
        this.customerInquiries.add(customerInquiry);
        customerInquiry.setCustomer(this);
        return this;
    }

    public Customer removeCustomerInquiry(CustomerInquiry customerInquiry) {
        this.customerInquiries.remove(customerInquiry);
        customerInquiry.setCustomer(null);
        return this;
    }

    public void setCustomerInquiries(Set<CustomerInquiry> customerInquiries) {
        if (this.customerInquiries != null) {
            this.customerInquiries.forEach(i -> i.setCustomer(null));
        }
        if (customerInquiries != null) {
            customerInquiries.forEach(i -> i.setCustomer(this));
        }
        this.customerInquiries = customerInquiries;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public Customer channel(Channel channel) {
        this.setChannel(channel);
        this.channelId = channel != null ? channel.getId() : null;
        return this;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
        this.channelId = channel != null ? channel.getId() : null;
    }

    public Long getChannelId() {
        return this.channelId;
    }

    public void setChannelId(Long channel) {
        this.channelId = channel;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customer)) {
            return false;
        }
        return id != null && id.equals(((Customer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Customer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", phone2='" + getPhone2() + "'" +
            ", phone3='" + getPhone3() + "'" +
            ", email='" + getEmail() + "'" +
            ", company='" + getCompany() + "'" +
            ", jobTitle='" + getJobTitle() + "'" +
            ", nextActivity='" + getNextActivity() + "'" +
            ", lastActivity='" + getLastActivity() + "'" +
            ", salesNotes='" + getSalesNotes() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
