package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A AppUser.
 */
@Table("app_user")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "appuser")
public class AppUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("name")
    private String name;

    @NotNull(message = "must not be null")
    @Column("email")
    private String email;

    @NotNull(message = "must not be null")
    @Column("password")
    private String password;

    @Column("avatar")
    private byte[] avatar;

    @Column("avatar_content_type")
    private String avatarContentType;

    @Column("email_verified_at")
    private ZonedDateTime emailVerifiedAt;

    @Column("remember_token")
    private String rememberToken;

    @Column("setting")
    private String setting;

    @Column("created_at")
    private ZonedDateTime createdAt;

    @Column("updated_at")
    private ZonedDateTime updatedAt;

    private Long accessTokenId;

    @Transient
    private AccessToken accessToken;

    @Transient
    @JsonIgnoreProperties(
        value = {
            "products", "complains", "paymentHistories", "customerInquiries", "categories", "settingOptions", "appUser", "plan", "partners",
        },
        allowSetters = true
    )
    private Set<Company> companies = new HashSet<>();

    @JsonIgnoreProperties(value = { "appUsers" }, allowSetters = true)
    @Transient
    private Set<Role> roles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppUser id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public AppUser name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public AppUser email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public AppUser password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getAvatar() {
        return this.avatar;
    }

    public AppUser avatar(byte[] avatar) {
        this.avatar = avatar;
        return this;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getAvatarContentType() {
        return this.avatarContentType;
    }

    public AppUser avatarContentType(String avatarContentType) {
        this.avatarContentType = avatarContentType;
        return this;
    }

    public void setAvatarContentType(String avatarContentType) {
        this.avatarContentType = avatarContentType;
    }

    public ZonedDateTime getEmailVerifiedAt() {
        return this.emailVerifiedAt;
    }

    public AppUser emailVerifiedAt(ZonedDateTime emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
        return this;
    }

    public void setEmailVerifiedAt(ZonedDateTime emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getRememberToken() {
        return this.rememberToken;
    }

    public AppUser rememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
        return this;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getSetting() {
        return this.setting;
    }

    public AppUser setting(String setting) {
        this.setting = setting;
        return this;
    }

    public void setSetting(String setting) {
        this.setting = setting;
    }

    public ZonedDateTime getCreatedAt() {
        return this.createdAt;
    }

    public AppUser createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public AppUser updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public AccessToken getAccessToken() {
        return this.accessToken;
    }

    public AppUser accessToken(AccessToken accessToken) {
        this.setAccessToken(accessToken);
        this.accessTokenId = accessToken != null ? accessToken.getId() : null;
        return this;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
        this.accessTokenId = accessToken != null ? accessToken.getId() : null;
    }

    public Long getAccessTokenId() {
        return this.accessTokenId;
    }

    public void setAccessTokenId(Long accessToken) {
        this.accessTokenId = accessToken;
    }

    public Set<Company> getCompanies() {
        return this.companies;
    }

    public AppUser companies(Set<Company> companies) {
        this.setCompanies(companies);
        return this;
    }

    public AppUser addCompany(Company company) {
        this.companies.add(company);
        company.setAppUser(this);
        return this;
    }

    public AppUser removeCompany(Company company) {
        this.companies.remove(company);
        company.setAppUser(null);
        return this;
    }

    public void setCompanies(Set<Company> companies) {
        if (this.companies != null) {
            this.companies.forEach(i -> i.setAppUser(null));
        }
        if (companies != null) {
            companies.forEach(i -> i.setAppUser(this));
        }
        this.companies = companies;
    }

    public Set<Role> getRoles() {
        return this.roles;
    }

    public AppUser roles(Set<Role> roles) {
        this.setRoles(roles);
        return this;
    }

    public AppUser addRole(Role role) {
        this.roles.add(role);
        role.getAppUsers().add(this);
        return this;
    }

    public AppUser removeRole(Role role) {
        this.roles.remove(role);
        role.getAppUsers().remove(this);
        return this;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AppUser)) {
            return false;
        }
        return id != null && id.equals(((AppUser) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AppUser{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", email='" + getEmail() + "'" +
            ", password='" + getPassword() + "'" +
            ", avatar='" + getAvatar() + "'" +
            ", avatarContentType='" + getAvatarContentType() + "'" +
            ", emailVerifiedAt='" + getEmailVerifiedAt() + "'" +
            ", rememberToken='" + getRememberToken() + "'" +
            ", setting='" + getSetting() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
