import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './customer.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CustomerDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const customerEntity = useAppSelector(state => state.customer.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="customerDetailsHeading">
          <Translate contentKey="chatbotApp.customer.detail.title">Customer</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.customer.id">Id</Translate>
            </span>
          </dt>
          <dd>{customerEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="chatbotApp.customer.name">Name</Translate>
            </span>
          </dt>
          <dd>{customerEntity.name}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="chatbotApp.customer.phone">Phone</Translate>
            </span>
          </dt>
          <dd>{customerEntity.phone}</dd>
          <dt>
            <span id="phone2">
              <Translate contentKey="chatbotApp.customer.phone2">Phone 2</Translate>
            </span>
          </dt>
          <dd>{customerEntity.phone2}</dd>
          <dt>
            <span id="phone3">
              <Translate contentKey="chatbotApp.customer.phone3">Phone 3</Translate>
            </span>
          </dt>
          <dd>{customerEntity.phone3}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="chatbotApp.customer.email">Email</Translate>
            </span>
          </dt>
          <dd>{customerEntity.email}</dd>
          <dt>
            <span id="company">
              <Translate contentKey="chatbotApp.customer.company">Company</Translate>
            </span>
          </dt>
          <dd>{customerEntity.company}</dd>
          <dt>
            <span id="jobTitle">
              <Translate contentKey="chatbotApp.customer.jobTitle">Job Title</Translate>
            </span>
          </dt>
          <dd>{customerEntity.jobTitle}</dd>
          <dt>
            <span id="nextActivity">
              <Translate contentKey="chatbotApp.customer.nextActivity">Next Activity</Translate>
            </span>
          </dt>
          <dd>
            {customerEntity.nextActivity ? <TextFormat value={customerEntity.nextActivity} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="lastActivity">
              <Translate contentKey="chatbotApp.customer.lastActivity">Last Activity</Translate>
            </span>
          </dt>
          <dd>
            {customerEntity.lastActivity ? <TextFormat value={customerEntity.lastActivity} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="salesNotes">
              <Translate contentKey="chatbotApp.customer.salesNotes">Sales Notes</Translate>
            </span>
          </dt>
          <dd>{customerEntity.salesNotes}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.customer.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{customerEntity.createdAt ? <TextFormat value={customerEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.customer.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{customerEntity.updatedAt ? <TextFormat value={customerEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="chatbotApp.customer.channel">Channel</Translate>
          </dt>
          <dd>{customerEntity.channel ? customerEntity.channel.name : ''}</dd>
        </dl>
        <Button tag={Link} to="/customer" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/customer/${customerEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default CustomerDetail;
