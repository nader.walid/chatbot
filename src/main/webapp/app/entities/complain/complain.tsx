import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Input, InputGroup, FormGroup, Form, Col, Row, Table } from 'reactstrap';
import { Translate, translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { searchEntities, getEntities } from './complain.reducer';
import { IComplain } from 'app/shared/model/complain.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const Complain = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState('');

  const complainList = useAppSelector(state => state.complain.entities);
  const loading = useAppSelector(state => state.complain.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const startSearching = e => {
    if (search) {
      dispatch(searchEntities({ query: search }));
    }
    e.preventDefault();
  };

  const clear = () => {
    setSearch('');
    dispatch(getEntities({}));
  };

  const handleSearch = event => setSearch(event.target.value);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="complain-heading" data-cy="ComplainHeading">
        <Translate contentKey="chatbotApp.complain.home.title">Complains</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="chatbotApp.complain.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="chatbotApp.complain.home.createLabel">Create new Complain</Translate>
          </Link>
        </div>
      </h2>
      <Row>
        <Col sm="12">
          <Form onSubmit={startSearching}>
            <FormGroup>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  defaultValue={search}
                  onChange={handleSearch}
                  placeholder={translate('chatbotApp.complain.home.search')}
                />
                <Button className="input-group-addon">
                  <FontAwesomeIcon icon="search" />
                </Button>
                <Button type="reset" className="input-group-addon" onClick={clear}>
                  <FontAwesomeIcon icon="trash" />
                </Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <div className="table-responsive">
        {complainList && complainList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="chatbotApp.complain.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.title">Title</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.data">Data</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.status">Status</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.callPhone">Call Phone</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.salesNotes">Sales Notes</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.customer">Customer</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.complain.company">Company</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {complainList.map((complain, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${complain.id}`} color="link" size="sm">
                      {complain.id}
                    </Button>
                  </td>
                  <td>{complain.title}</td>
                  <td>{complain.data}</td>
                  <td>{complain.status}</td>
                  <td>{complain.callPhone}</td>
                  <td>{complain.salesNotes}</td>
                  <td>{complain.customer ? <Link to={`customer/${complain.customer.id}`}>{complain.customer.name}</Link> : ''}</td>
                  <td>{complain.company ? <Link to={`company/${complain.company.id}`}>{complain.company.name}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${complain.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${complain.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${complain.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="chatbotApp.complain.home.notFound">No Complains found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Complain;
