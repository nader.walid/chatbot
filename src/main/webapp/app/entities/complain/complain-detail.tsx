import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './complain.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const ComplainDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const complainEntity = useAppSelector(state => state.complain.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="complainDetailsHeading">
          <Translate contentKey="chatbotApp.complain.detail.title">Complain</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.complain.id">Id</Translate>
            </span>
          </dt>
          <dd>{complainEntity.id}</dd>
          <dt>
            <span id="title">
              <Translate contentKey="chatbotApp.complain.title">Title</Translate>
            </span>
          </dt>
          <dd>{complainEntity.title}</dd>
          <dt>
            <span id="data">
              <Translate contentKey="chatbotApp.complain.data">Data</Translate>
            </span>
          </dt>
          <dd>{complainEntity.data}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="chatbotApp.complain.status">Status</Translate>
            </span>
          </dt>
          <dd>{complainEntity.status}</dd>
          <dt>
            <span id="callPhone">
              <Translate contentKey="chatbotApp.complain.callPhone">Call Phone</Translate>
            </span>
          </dt>
          <dd>{complainEntity.callPhone}</dd>
          <dt>
            <span id="salesNotes">
              <Translate contentKey="chatbotApp.complain.salesNotes">Sales Notes</Translate>
            </span>
          </dt>
          <dd>{complainEntity.salesNotes}</dd>
          <dt>
            <span id="nextActivity">
              <Translate contentKey="chatbotApp.complain.nextActivity">Next Activity</Translate>
            </span>
          </dt>
          <dd>
            {complainEntity.nextActivity ? <TextFormat value={complainEntity.nextActivity} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="lastActivity">
              <Translate contentKey="chatbotApp.complain.lastActivity">Last Activity</Translate>
            </span>
          </dt>
          <dd>
            {complainEntity.lastActivity ? <TextFormat value={complainEntity.lastActivity} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.complain.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{complainEntity.createdAt ? <TextFormat value={complainEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.complain.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{complainEntity.updatedAt ? <TextFormat value={complainEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="chatbotApp.complain.customer">Customer</Translate>
          </dt>
          <dd>{complainEntity.customer ? complainEntity.customer.name : ''}</dd>
          <dt>
            <Translate contentKey="chatbotApp.complain.company">Company</Translate>
          </dt>
          <dd>{complainEntity.company ? complainEntity.company.name : ''}</dd>
        </dl>
        <Button tag={Link} to="/complain" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/complain/${complainEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default ComplainDetail;
