import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ICustomer } from 'app/shared/model/customer.model';
import { getEntities as getCustomers } from 'app/entities/customer/customer.reducer';
import { ICompany } from 'app/shared/model/company.model';
import { getEntities as getCompanies } from 'app/entities/company/company.reducer';
import { getEntity, updateEntity, createEntity, reset } from './complain.reducer';
import { IComplain } from 'app/shared/model/complain.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const ComplainUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const customers = useAppSelector(state => state.customer.entities);
  const companies = useAppSelector(state => state.company.entities);
  const complainEntity = useAppSelector(state => state.complain.entity);
  const loading = useAppSelector(state => state.complain.loading);
  const updating = useAppSelector(state => state.complain.updating);
  const updateSuccess = useAppSelector(state => state.complain.updateSuccess);

  const handleClose = () => {
    props.history.push('/complain');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getCustomers({}));
    dispatch(getCompanies({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.nextActivity = convertDateTimeToServer(values.nextActivity);
    values.lastActivity = convertDateTimeToServer(values.lastActivity);
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.updatedAt = convertDateTimeToServer(values.updatedAt);

    const entity = {
      ...complainEntity,
      ...values,
      customer: customers.find(it => it.id.toString() === values.customerId.toString()),
      company: companies.find(it => it.id.toString() === values.companyId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          nextActivity: displayDefaultDateTime(),
          lastActivity: displayDefaultDateTime(),
          createdAt: displayDefaultDateTime(),
          updatedAt: displayDefaultDateTime(),
        }
      : {
          ...complainEntity,
          nextActivity: convertDateTimeFromServer(complainEntity.nextActivity),
          lastActivity: convertDateTimeFromServer(complainEntity.lastActivity),
          createdAt: convertDateTimeFromServer(complainEntity.createdAt),
          updatedAt: convertDateTimeFromServer(complainEntity.updatedAt),
          customerId: complainEntity?.customer?.id,
          companyId: complainEntity?.company?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="chatbotApp.complain.home.createOrEditLabel" data-cy="ComplainCreateUpdateHeading">
            <Translate contentKey="chatbotApp.complain.home.createOrEditLabel">Create or edit a Complain</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="complain-id"
                  label={translate('chatbotApp.complain.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('chatbotApp.complain.title')}
                id="complain-title"
                name="title"
                data-cy="title"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField label={translate('chatbotApp.complain.data')} id="complain-data" name="data" data-cy="data" type="text" />
              <ValidatedField
                label={translate('chatbotApp.complain.status')}
                id="complain-status"
                name="status"
                data-cy="status"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.complain.callPhone')}
                id="complain-callPhone"
                name="callPhone"
                data-cy="callPhone"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.complain.salesNotes')}
                id="complain-salesNotes"
                name="salesNotes"
                data-cy="salesNotes"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.complain.nextActivity')}
                id="complain-nextActivity"
                name="nextActivity"
                data-cy="nextActivity"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.complain.lastActivity')}
                id="complain-lastActivity"
                name="lastActivity"
                data-cy="lastActivity"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.complain.createdAt')}
                id="complain-createdAt"
                name="createdAt"
                data-cy="createdAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.complain.updatedAt')}
                id="complain-updatedAt"
                name="updatedAt"
                data-cy="updatedAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                id="complain-customer"
                name="customerId"
                data-cy="customer"
                label={translate('chatbotApp.complain.customer')}
                type="select"
              >
                <option value="" key="0" />
                {customers
                  ? customers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="complain-company"
                name="companyId"
                data-cy="company"
                label={translate('chatbotApp.complain.company')}
                type="select"
              >
                <option value="" key="0" />
                {companies
                  ? companies.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/complain" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default ComplainUpdate;
