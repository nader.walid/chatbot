import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IAppUser } from 'app/shared/model/app-user.model';
import { getEntities as getAppUsers } from 'app/entities/app-user/app-user.reducer';
import { getEntity, updateEntity, createEntity, reset } from './access-token.reducer';
import { IAccessToken } from 'app/shared/model/access-token.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const AccessTokenUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const appUsers = useAppSelector(state => state.appUser.entities);
  const accessTokenEntity = useAppSelector(state => state.accessToken.entity);
  const loading = useAppSelector(state => state.accessToken.loading);
  const updating = useAppSelector(state => state.accessToken.updating);
  const updateSuccess = useAppSelector(state => state.accessToken.updateSuccess);

  const handleClose = () => {
    props.history.push('/access-token');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getAppUsers({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.tokenCreation = convertDateTimeToServer(values.tokenCreation);
    values.tokenExpire = convertDateTimeToServer(values.tokenExpire);
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.updatedAt = convertDateTimeToServer(values.updatedAt);

    const entity = {
      ...accessTokenEntity,
      ...values,
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          tokenCreation: displayDefaultDateTime(),
          tokenExpire: displayDefaultDateTime(),
          createdAt: displayDefaultDateTime(),
          updatedAt: displayDefaultDateTime(),
        }
      : {
          ...accessTokenEntity,
          tokenCreation: convertDateTimeFromServer(accessTokenEntity.tokenCreation),
          tokenExpire: convertDateTimeFromServer(accessTokenEntity.tokenExpire),
          createdAt: convertDateTimeFromServer(accessTokenEntity.createdAt),
          updatedAt: convertDateTimeFromServer(accessTokenEntity.updatedAt),
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="chatbotApp.accessToken.home.createOrEditLabel" data-cy="AccessTokenCreateUpdateHeading">
            <Translate contentKey="chatbotApp.accessToken.home.createOrEditLabel">Create or edit a AccessToken</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="access-token-id"
                  label={translate('chatbotApp.accessToken.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('chatbotApp.accessToken.appName')}
                id="access-token-appName"
                name="appName"
                data-cy="appName"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.accessToken.token')}
                id="access-token-token"
                name="token"
                data-cy="token"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.accessToken.tokenCreation')}
                id="access-token-tokenCreation"
                name="tokenCreation"
                data-cy="tokenCreation"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.accessToken.tokenExpire')}
                id="access-token-tokenExpire"
                name="tokenExpire"
                data-cy="tokenExpire"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.accessToken.userAlies')}
                id="access-token-userAlies"
                name="userAlies"
                data-cy="userAlies"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.accessToken.createdAt')}
                id="access-token-createdAt"
                name="createdAt"
                data-cy="createdAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.accessToken.updatedAt')}
                id="access-token-updatedAt"
                name="updatedAt"
                data-cy="updatedAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/access-token" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default AccessTokenUpdate;
