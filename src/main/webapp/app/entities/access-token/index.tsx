import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import AccessToken from './access-token';
import AccessTokenDetail from './access-token-detail';
import AccessTokenUpdate from './access-token-update';
import AccessTokenDeleteDialog from './access-token-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={AccessTokenUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={AccessTokenUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={AccessTokenDetail} />
      <ErrorBoundaryRoute path={match.url} component={AccessToken} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={AccessTokenDeleteDialog} />
  </>
);

export default Routes;
