import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Input, InputGroup, FormGroup, Form, Col, Row, Table } from 'reactstrap';
import { Translate, translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { searchEntities, getEntities } from './access-token.reducer';
import { IAccessToken } from 'app/shared/model/access-token.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const AccessToken = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState('');

  const accessTokenList = useAppSelector(state => state.accessToken.entities);
  const loading = useAppSelector(state => state.accessToken.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const startSearching = e => {
    if (search) {
      dispatch(searchEntities({ query: search }));
    }
    e.preventDefault();
  };

  const clear = () => {
    setSearch('');
    dispatch(getEntities({}));
  };

  const handleSearch = event => setSearch(event.target.value);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="access-token-heading" data-cy="AccessTokenHeading">
        <Translate contentKey="chatbotApp.accessToken.home.title">Access Tokens</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="chatbotApp.accessToken.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="chatbotApp.accessToken.home.createLabel">Create new Access Token</Translate>
          </Link>
        </div>
      </h2>
      <Row>
        <Col sm="12">
          <Form onSubmit={startSearching}>
            <FormGroup>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  defaultValue={search}
                  onChange={handleSearch}
                  placeholder={translate('chatbotApp.accessToken.home.search')}
                />
                <Button className="input-group-addon">
                  <FontAwesomeIcon icon="search" />
                </Button>
                <Button type="reset" className="input-group-addon" onClick={clear}>
                  <FontAwesomeIcon icon="trash" />
                </Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <div className="table-responsive">
        {accessTokenList && accessTokenList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="chatbotApp.accessToken.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.accessToken.appName">App Name</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.accessToken.token">Token</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.accessToken.tokenCreation">Token Creation</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.accessToken.tokenExpire">Token Expire</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.accessToken.userAlies">User Alies</Translate>
                </th>
              </tr>
            </thead>
            <tbody>
              {accessTokenList.map((accessToken, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${accessToken.id}`} color="link" size="sm">
                      {accessToken.id}
                    </Button>
                  </td>
                  <td>{accessToken.appName}</td>
                  <td>{accessToken.token}</td>
                  <td>
                    {accessToken.tokenCreation ? (
                      <TextFormat type="date" value={accessToken.tokenCreation} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {accessToken.tokenExpire ? <TextFormat type="date" value={accessToken.tokenExpire} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>{accessToken.userAlies}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${accessToken.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${accessToken.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${accessToken.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="chatbotApp.accessToken.home.notFound">No Access Tokens found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default AccessToken;
