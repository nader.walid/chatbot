import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './access-token.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const AccessTokenDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const accessTokenEntity = useAppSelector(state => state.accessToken.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="accessTokenDetailsHeading">
          <Translate contentKey="chatbotApp.accessToken.detail.title">AccessToken</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.accessToken.id">Id</Translate>
            </span>
          </dt>
          <dd>{accessTokenEntity.id}</dd>
          <dt>
            <span id="appName">
              <Translate contentKey="chatbotApp.accessToken.appName">App Name</Translate>
            </span>
          </dt>
          <dd>{accessTokenEntity.appName}</dd>
          <dt>
            <span id="token">
              <Translate contentKey="chatbotApp.accessToken.token">Token</Translate>
            </span>
          </dt>
          <dd>{accessTokenEntity.token}</dd>
          <dt>
            <span id="tokenCreation">
              <Translate contentKey="chatbotApp.accessToken.tokenCreation">Token Creation</Translate>
            </span>
          </dt>
          <dd>
            {accessTokenEntity.tokenCreation ? (
              <TextFormat value={accessTokenEntity.tokenCreation} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="tokenExpire">
              <Translate contentKey="chatbotApp.accessToken.tokenExpire">Token Expire</Translate>
            </span>
          </dt>
          <dd>
            {accessTokenEntity.tokenExpire ? (
              <TextFormat value={accessTokenEntity.tokenExpire} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="userAlies">
              <Translate contentKey="chatbotApp.accessToken.userAlies">User Alies</Translate>
            </span>
          </dt>
          <dd>{accessTokenEntity.userAlies}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.accessToken.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>
            {accessTokenEntity.createdAt ? <TextFormat value={accessTokenEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.accessToken.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>
            {accessTokenEntity.updatedAt ? <TextFormat value={accessTokenEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
        </dl>
        <Button tag={Link} to="/access-token" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/access-token/${accessTokenEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default AccessTokenDetail;
