import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './category.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CategoryDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const categoryEntity = useAppSelector(state => state.category.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="categoryDetailsHeading">
          <Translate contentKey="chatbotApp.category.detail.title">Category</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.category.id">Id</Translate>
            </span>
          </dt>
          <dd>{categoryEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="chatbotApp.category.name">Name</Translate>
            </span>
          </dt>
          <dd>{categoryEntity.name}</dd>
          <dt>
            <span id="slug">
              <Translate contentKey="chatbotApp.category.slug">Slug</Translate>
            </span>
          </dt>
          <dd>{categoryEntity.slug}</dd>
          <dt>
            <span id="order">
              <Translate contentKey="chatbotApp.category.order">Order</Translate>
            </span>
          </dt>
          <dd>{categoryEntity.order}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.category.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{categoryEntity.createdAt ? <TextFormat value={categoryEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.category.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{categoryEntity.updatedAt ? <TextFormat value={categoryEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="image">
              <Translate contentKey="chatbotApp.category.image">Image</Translate>
            </span>
          </dt>
          <dd>
            {categoryEntity.image ? (
              <div>
                {categoryEntity.imageContentType ? (
                  <a onClick={openFile(categoryEntity.imageContentType, categoryEntity.image)}>
                    <img src={`data:${categoryEntity.imageContentType};base64,${categoryEntity.image}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {categoryEntity.imageContentType}, {byteSize(categoryEntity.image)}
                </span>
              </div>
            ) : null}
          </dd>
        </dl>
        <Button tag={Link} to="/category" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/category/${categoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default CategoryDetail;
