import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './plan.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const PlanDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const planEntity = useAppSelector(state => state.plan.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="planDetailsHeading">
          <Translate contentKey="chatbotApp.plan.detail.title">Plan</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.plan.id">Id</Translate>
            </span>
          </dt>
          <dd>{planEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="chatbotApp.plan.name">Name</Translate>
            </span>
          </dt>
          <dd>{planEntity.name}</dd>
          <dt>
            <span id="maxUsers">
              <Translate contentKey="chatbotApp.plan.maxUsers">Max Users</Translate>
            </span>
          </dt>
          <dd>{planEntity.maxUsers}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.plan.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{planEntity.createdAt ? <TextFormat value={planEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.plan.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{planEntity.updatedAt ? <TextFormat value={planEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
        </dl>
        <Button tag={Link} to="/plan" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/plan/${planEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default PlanDetail;
