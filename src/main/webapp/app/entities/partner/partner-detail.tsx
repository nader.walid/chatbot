import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './partner.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const PartnerDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const partnerEntity = useAppSelector(state => state.partner.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="partnerDetailsHeading">
          <Translate contentKey="chatbotApp.partner.detail.title">Partner</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.partner.id">Id</Translate>
            </span>
          </dt>
          <dd>{partnerEntity.id}</dd>
          <dt>
            <span id="image">
              <Translate contentKey="chatbotApp.partner.image">Image</Translate>
            </span>
          </dt>
          <dd>
            {partnerEntity.image ? (
              <div>
                {partnerEntity.imageContentType ? (
                  <a onClick={openFile(partnerEntity.imageContentType, partnerEntity.image)}>
                    <img src={`data:${partnerEntity.imageContentType};base64,${partnerEntity.image}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {partnerEntity.imageContentType}, {byteSize(partnerEntity.image)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <span id="nextActivity">
              <Translate contentKey="chatbotApp.partner.nextActivity">Next Activity</Translate>
            </span>
          </dt>
          <dd>
            {partnerEntity.nextActivity ? <TextFormat value={partnerEntity.nextActivity} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="lastActivity">
              <Translate contentKey="chatbotApp.partner.lastActivity">Last Activity</Translate>
            </span>
          </dt>
          <dd>
            {partnerEntity.lastActivity ? <TextFormat value={partnerEntity.lastActivity} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.partner.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{partnerEntity.createdAt ? <TextFormat value={partnerEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.partner.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{partnerEntity.updatedAt ? <TextFormat value={partnerEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="chatbotApp.partner.company">Company</Translate>
          </dt>
          <dd>
            {partnerEntity.companies
              ? partnerEntity.companies.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {partnerEntity.companies && i === partnerEntity.companies.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/partner" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/partner/${partnerEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default PartnerDetail;
