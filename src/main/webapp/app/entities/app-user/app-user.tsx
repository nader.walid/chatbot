import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Input, InputGroup, FormGroup, Form, Col, Row, Table } from 'reactstrap';
import { openFile, byteSize, Translate, translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { searchEntities, getEntities } from './app-user.reducer';
import { IAppUser } from 'app/shared/model/app-user.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const AppUser = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState('');

  const appUserList = useAppSelector(state => state.appUser.entities);
  const loading = useAppSelector(state => state.appUser.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const startSearching = e => {
    if (search) {
      dispatch(searchEntities({ query: search }));
    }
    e.preventDefault();
  };

  const clear = () => {
    setSearch('');
    dispatch(getEntities({}));
  };

  const handleSearch = event => setSearch(event.target.value);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="app-user-heading" data-cy="AppUserHeading">
        <Translate contentKey="chatbotApp.appUser.home.title">App Users</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="chatbotApp.appUser.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="chatbotApp.appUser.home.createLabel">Create new App User</Translate>
          </Link>
        </div>
      </h2>
      <Row>
        <Col sm="12">
          <Form onSubmit={startSearching}>
            <FormGroup>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  defaultValue={search}
                  onChange={handleSearch}
                  placeholder={translate('chatbotApp.appUser.home.search')}
                />
                <Button className="input-group-addon">
                  <FontAwesomeIcon icon="search" />
                </Button>
                <Button type="reset" className="input-group-addon" onClick={clear}>
                  <FontAwesomeIcon icon="trash" />
                </Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <div className="table-responsive">
        {appUserList && appUserList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="chatbotApp.appUser.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.name">Name</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.email">Email</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.password">Password</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.avatar">Avatar</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.emailVerifiedAt">Email Verified At</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.rememberToken">Remember Token</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.setting">Setting</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.accessToken">Access Token</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.appUser.role">Role</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {appUserList.map((appUser, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${appUser.id}`} color="link" size="sm">
                      {appUser.id}
                    </Button>
                  </td>
                  <td>{appUser.name}</td>
                  <td>{appUser.email}</td>
                  <td>{appUser.password}</td>
                  <td>
                    {appUser.avatar ? (
                      <div>
                        {appUser.avatarContentType ? (
                          <a onClick={openFile(appUser.avatarContentType, appUser.avatar)}>
                            <img src={`data:${appUser.avatarContentType};base64,${appUser.avatar}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {appUser.avatarContentType}, {byteSize(appUser.avatar)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>
                    {appUser.emailVerifiedAt ? <TextFormat type="date" value={appUser.emailVerifiedAt} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>{appUser.rememberToken}</td>
                  <td>{appUser.setting}</td>
                  <td>{appUser.accessToken ? <Link to={`access-token/${appUser.accessToken.id}`}>{appUser.accessToken.id}</Link> : ''}</td>
                  <td>
                    {appUser.roles
                      ? appUser.roles.map((val, j) => (
                          <span key={j}>
                            <Link to={`role/${val.id}`}>{val.displayName}</Link>
                            {j === appUser.roles.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${appUser.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${appUser.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${appUser.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="chatbotApp.appUser.home.notFound">No App Users found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default AppUser;
