import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './app-user.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const AppUserDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const appUserEntity = useAppSelector(state => state.appUser.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="appUserDetailsHeading">
          <Translate contentKey="chatbotApp.appUser.detail.title">AppUser</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.appUser.id">Id</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="chatbotApp.appUser.name">Name</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.name}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="chatbotApp.appUser.email">Email</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.email}</dd>
          <dt>
            <span id="password">
              <Translate contentKey="chatbotApp.appUser.password">Password</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.password}</dd>
          <dt>
            <span id="avatar">
              <Translate contentKey="chatbotApp.appUser.avatar">Avatar</Translate>
            </span>
          </dt>
          <dd>
            {appUserEntity.avatar ? (
              <div>
                {appUserEntity.avatarContentType ? (
                  <a onClick={openFile(appUserEntity.avatarContentType, appUserEntity.avatar)}>
                    <img src={`data:${appUserEntity.avatarContentType};base64,${appUserEntity.avatar}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {appUserEntity.avatarContentType}, {byteSize(appUserEntity.avatar)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <span id="emailVerifiedAt">
              <Translate contentKey="chatbotApp.appUser.emailVerifiedAt">Email Verified At</Translate>
            </span>
          </dt>
          <dd>
            {appUserEntity.emailVerifiedAt ? (
              <TextFormat value={appUserEntity.emailVerifiedAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="rememberToken">
              <Translate contentKey="chatbotApp.appUser.rememberToken">Remember Token</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.rememberToken}</dd>
          <dt>
            <span id="setting">
              <Translate contentKey="chatbotApp.appUser.setting">Setting</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.setting}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.appUser.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.createdAt ? <TextFormat value={appUserEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.appUser.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{appUserEntity.updatedAt ? <TextFormat value={appUserEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="chatbotApp.appUser.accessToken">Access Token</Translate>
          </dt>
          <dd>{appUserEntity.accessToken ? appUserEntity.accessToken.id : ''}</dd>
          <dt>
            <Translate contentKey="chatbotApp.appUser.role">Role</Translate>
          </dt>
          <dd>
            {appUserEntity.roles
              ? appUserEntity.roles.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.displayName}</a>
                    {appUserEntity.roles && i === appUserEntity.roles.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/app-user" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/app-user/${appUserEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default AppUserDetail;
