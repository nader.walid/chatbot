import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './customer-inquiry.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CustomerInquiryDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const customerInquiryEntity = useAppSelector(state => state.customerInquiry.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="customerInquiryDetailsHeading">
          <Translate contentKey="chatbotApp.customerInquiry.detail.title">CustomerInquiry</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.customerInquiry.id">Id</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.id}</dd>
          <dt>
            <span id="body">
              <Translate contentKey="chatbotApp.customerInquiry.body">Body</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.body}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="chatbotApp.customerInquiry.status">Status</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.status}</dd>
          <dt>
            <span id="reason">
              <Translate contentKey="chatbotApp.customerInquiry.reason">Reason</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.reason}</dd>
          <dt>
            <span id="callDay">
              <Translate contentKey="chatbotApp.customerInquiry.callDay">Call Day</Translate>
            </span>
          </dt>
          <dd>
            {customerInquiryEntity.callDay ? (
              <TextFormat value={customerInquiryEntity.callDay} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="callTime">
              <Translate contentKey="chatbotApp.customerInquiry.callTime">Call Time</Translate>
            </span>
          </dt>
          <dd>
            {customerInquiryEntity.callTime ? (
              <TextFormat value={customerInquiryEntity.callTime} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="callPhone">
              <Translate contentKey="chatbotApp.customerInquiry.callPhone">Call Phone</Translate>
            </span>
          </dt>
          <dd>
            {customerInquiryEntity.callPhone ? (
              <TextFormat value={customerInquiryEntity.callPhone} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="statusNotes">
              <Translate contentKey="chatbotApp.customerInquiry.statusNotes">Status Notes</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.statusNotes}</dd>
          <dt>
            <span id="nextActivity">
              <Translate contentKey="chatbotApp.customerInquiry.nextActivity">Next Activity</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.nextActivity}</dd>
          <dt>
            <span id="lastActivity">
              <Translate contentKey="chatbotApp.customerInquiry.lastActivity">Last Activity</Translate>
            </span>
          </dt>
          <dd>{customerInquiryEntity.lastActivity}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.customerInquiry.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>
            {customerInquiryEntity.createdAt ? (
              <TextFormat value={customerInquiryEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.customerInquiry.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>
            {customerInquiryEntity.updatedAt ? (
              <TextFormat value={customerInquiryEntity.updatedAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="chatbotApp.customerInquiry.customer">Customer</Translate>
          </dt>
          <dd>{customerInquiryEntity.customer ? customerInquiryEntity.customer.name : ''}</dd>
          <dt>
            <Translate contentKey="chatbotApp.customerInquiry.company">Company</Translate>
          </dt>
          <dd>{customerInquiryEntity.company ? customerInquiryEntity.company.name : ''}</dd>
        </dl>
        <Button tag={Link} to="/customer-inquiry" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/customer-inquiry/${customerInquiryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default CustomerInquiryDetail;
