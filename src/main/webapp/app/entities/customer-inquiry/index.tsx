import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CustomerInquiry from './customer-inquiry';
import CustomerInquiryDetail from './customer-inquiry-detail';
import CustomerInquiryUpdate from './customer-inquiry-update';
import CustomerInquiryDeleteDialog from './customer-inquiry-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CustomerInquiryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CustomerInquiryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CustomerInquiryDetail} />
      <ErrorBoundaryRoute path={match.url} component={CustomerInquiry} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CustomerInquiryDeleteDialog} />
  </>
);

export default Routes;
