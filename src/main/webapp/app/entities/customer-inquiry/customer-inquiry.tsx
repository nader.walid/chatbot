import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Input, InputGroup, FormGroup, Form, Col, Row, Table } from 'reactstrap';
import { Translate, translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { searchEntities, getEntities } from './customer-inquiry.reducer';
import { ICustomerInquiry } from 'app/shared/model/customer-inquiry.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CustomerInquiry = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState('');

  const customerInquiryList = useAppSelector(state => state.customerInquiry.entities);
  const loading = useAppSelector(state => state.customerInquiry.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const startSearching = e => {
    if (search) {
      dispatch(searchEntities({ query: search }));
    }
    e.preventDefault();
  };

  const clear = () => {
    setSearch('');
    dispatch(getEntities({}));
  };

  const handleSearch = event => setSearch(event.target.value);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="customer-inquiry-heading" data-cy="CustomerInquiryHeading">
        <Translate contentKey="chatbotApp.customerInquiry.home.title">Customer Inquiries</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="chatbotApp.customerInquiry.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="chatbotApp.customerInquiry.home.createLabel">Create new Customer Inquiry</Translate>
          </Link>
        </div>
      </h2>
      <Row>
        <Col sm="12">
          <Form onSubmit={startSearching}>
            <FormGroup>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  defaultValue={search}
                  onChange={handleSearch}
                  placeholder={translate('chatbotApp.customerInquiry.home.search')}
                />
                <Button className="input-group-addon">
                  <FontAwesomeIcon icon="search" />
                </Button>
                <Button type="reset" className="input-group-addon" onClick={clear}>
                  <FontAwesomeIcon icon="trash" />
                </Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <div className="table-responsive">
        {customerInquiryList && customerInquiryList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.body">Body</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.status">Status</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.reason">Reason</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.callDay">Call Day</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.callTime">Call Time</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.callPhone">Call Phone</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.statusNotes">Status Notes</Translate>
                </th>

                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.customer">Customer</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.customerInquiry.company">Company</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {customerInquiryList.map((customerInquiry, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${customerInquiry.id}`} color="link" size="sm">
                      {customerInquiry.id}
                    </Button>
                  </td>
                  <td>{customerInquiry.body}</td>
                  <td>{customerInquiry.status}</td>
                  <td>{customerInquiry.reason}</td>
                  <td>
                    {customerInquiry.callDay ? <TextFormat type="date" value={customerInquiry.callDay} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {customerInquiry.callTime ? <TextFormat type="date" value={customerInquiry.callTime} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {customerInquiry.callPhone ? (
                      <TextFormat type="date" value={customerInquiry.callPhone} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{customerInquiry.statusNotes}</td>
                  <td>
                    {customerInquiry.customer ? (
                      <Link to={`customer/${customerInquiry.customer.id}`}>{customerInquiry.customer.name}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {customerInquiry.company ? (
                      <Link to={`company/${customerInquiry.company.id}`}>{customerInquiry.company.name}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${customerInquiry.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${customerInquiry.id}/edit`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${customerInquiry.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="chatbotApp.customerInquiry.home.notFound">No Customer Inquiries found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default CustomerInquiry;
