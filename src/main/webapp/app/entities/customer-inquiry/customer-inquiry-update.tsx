import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ICustomer } from 'app/shared/model/customer.model';
import { getEntities as getCustomers } from 'app/entities/customer/customer.reducer';
import { ICompany } from 'app/shared/model/company.model';
import { getEntities as getCompanies } from 'app/entities/company/company.reducer';
import { getEntity, updateEntity, createEntity, reset } from './customer-inquiry.reducer';
import { ICustomerInquiry } from 'app/shared/model/customer-inquiry.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CustomerInquiryUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const customers = useAppSelector(state => state.customer.entities);
  const companies = useAppSelector(state => state.company.entities);
  const customerInquiryEntity = useAppSelector(state => state.customerInquiry.entity);
  const loading = useAppSelector(state => state.customerInquiry.loading);
  const updating = useAppSelector(state => state.customerInquiry.updating);
  const updateSuccess = useAppSelector(state => state.customerInquiry.updateSuccess);

  const handleClose = () => {
    props.history.push('/customer-inquiry');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getCustomers({}));
    dispatch(getCompanies({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.callDay = convertDateTimeToServer(values.callDay);
    values.callTime = convertDateTimeToServer(values.callTime);
    values.callPhone = convertDateTimeToServer(values.callPhone);
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.updatedAt = convertDateTimeToServer(values.updatedAt);

    const entity = {
      ...customerInquiryEntity,
      ...values,
      customer: customers.find(it => it.id.toString() === values.customerId.toString()),
      company: companies.find(it => it.id.toString() === values.companyId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          callDay: displayDefaultDateTime(),
          callTime: displayDefaultDateTime(),
          callPhone: displayDefaultDateTime(),
          createdAt: displayDefaultDateTime(),
          updatedAt: displayDefaultDateTime(),
        }
      : {
          ...customerInquiryEntity,
          callDay: convertDateTimeFromServer(customerInquiryEntity.callDay),
          callTime: convertDateTimeFromServer(customerInquiryEntity.callTime),
          callPhone: convertDateTimeFromServer(customerInquiryEntity.callPhone),
          createdAt: convertDateTimeFromServer(customerInquiryEntity.createdAt),
          updatedAt: convertDateTimeFromServer(customerInquiryEntity.updatedAt),
          customerId: customerInquiryEntity?.customer?.id,
          companyId: customerInquiryEntity?.company?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="chatbotApp.customerInquiry.home.createOrEditLabel" data-cy="CustomerInquiryCreateUpdateHeading">
            <Translate contentKey="chatbotApp.customerInquiry.home.createOrEditLabel">Create or edit a CustomerInquiry</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="customer-inquiry-id"
                  label={translate('chatbotApp.customerInquiry.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.body')}
                id="customer-inquiry-body"
                name="body"
                data-cy="body"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.status')}
                id="customer-inquiry-status"
                name="status"
                data-cy="status"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.reason')}
                id="customer-inquiry-reason"
                name="reason"
                data-cy="reason"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.callDay')}
                id="customer-inquiry-callDay"
                name="callDay"
                data-cy="callDay"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.callTime')}
                id="customer-inquiry-callTime"
                name="callTime"
                data-cy="callTime"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.callPhone')}
                id="customer-inquiry-callPhone"
                name="callPhone"
                data-cy="callPhone"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.statusNotes')}
                id="customer-inquiry-statusNotes"
                name="statusNotes"
                data-cy="statusNotes"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.nextActivity')}
                id="customer-inquiry-nextActivity"
                name="nextActivity"
                data-cy="nextActivity"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.lastActivity')}
                id="customer-inquiry-lastActivity"
                name="lastActivity"
                data-cy="lastActivity"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.createdAt')}
                id="customer-inquiry-createdAt"
                name="createdAt"
                data-cy="createdAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.customerInquiry.updatedAt')}
                id="customer-inquiry-updatedAt"
                name="updatedAt"
                data-cy="updatedAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                id="customer-inquiry-customer"
                name="customerId"
                data-cy="customer"
                label={translate('chatbotApp.customerInquiry.customer')}
                type="select"
              >
                <option value="" key="0" />
                {customers
                  ? customers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="customer-inquiry-company"
                name="companyId"
                data-cy="company"
                label={translate('chatbotApp.customerInquiry.company')}
                type="select"
              >
                <option value="" key="0" />
                {companies
                  ? companies.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/customer-inquiry" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CustomerInquiryUpdate;
