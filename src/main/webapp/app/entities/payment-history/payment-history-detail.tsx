import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './payment-history.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const PaymentHistoryDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const paymentHistoryEntity = useAppSelector(state => state.paymentHistory.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="paymentHistoryDetailsHeading">
          <Translate contentKey="chatbotApp.paymentHistory.detail.title">PaymentHistory</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.paymentHistory.id">Id</Translate>
            </span>
          </dt>
          <dd>{paymentHistoryEntity.id}</dd>
          <dt>
            <span id="planStartDate">
              <Translate contentKey="chatbotApp.paymentHistory.planStartDate">Plan Start Date</Translate>
            </span>
          </dt>
          <dd>
            {paymentHistoryEntity.planStartDate ? (
              <TextFormat value={paymentHistoryEntity.planStartDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="planEndDate">
              <Translate contentKey="chatbotApp.paymentHistory.planEndDate">Plan End Date</Translate>
            </span>
          </dt>
          <dd>
            {paymentHistoryEntity.planEndDate ? (
              <TextFormat value={paymentHistoryEntity.planEndDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="usedSession">
              <Translate contentKey="chatbotApp.paymentHistory.usedSession">Used Session</Translate>
            </span>
          </dt>
          <dd>{paymentHistoryEntity.usedSession}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.paymentHistory.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>
            {paymentHistoryEntity.createdAt ? (
              <TextFormat value={paymentHistoryEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.paymentHistory.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>
            {paymentHistoryEntity.updatedAt ? (
              <TextFormat value={paymentHistoryEntity.updatedAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="chatbotApp.paymentHistory.company">Company</Translate>
          </dt>
          <dd>{paymentHistoryEntity.company ? paymentHistoryEntity.company.name : ''}</dd>
          <dt>
            <Translate contentKey="chatbotApp.paymentHistory.plan">Plan</Translate>
          </dt>
          <dd>{paymentHistoryEntity.plan ? paymentHistoryEntity.plan.name : ''}</dd>
        </dl>
        <Button tag={Link} to="/payment-history" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/payment-history/${paymentHistoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default PaymentHistoryDetail;
