import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Input, InputGroup, FormGroup, Form, Col, Row, Table } from 'reactstrap';
import { Translate, translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { searchEntities, getEntities } from './payment-history.reducer';
import { IPaymentHistory } from 'app/shared/model/payment-history.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const PaymentHistory = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState('');

  const paymentHistoryList = useAppSelector(state => state.paymentHistory.entities);
  const loading = useAppSelector(state => state.paymentHistory.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const startSearching = e => {
    if (search) {
      dispatch(searchEntities({ query: search }));
    }
    e.preventDefault();
  };

  const clear = () => {
    setSearch('');
    dispatch(getEntities({}));
  };

  const handleSearch = event => setSearch(event.target.value);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="payment-history-heading" data-cy="PaymentHistoryHeading">
        <Translate contentKey="chatbotApp.paymentHistory.home.title">Payment Histories</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="chatbotApp.paymentHistory.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="chatbotApp.paymentHistory.home.createLabel">Create new Payment History</Translate>
          </Link>
        </div>
      </h2>
      <Row>
        <Col sm="12">
          <Form onSubmit={startSearching}>
            <FormGroup>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  defaultValue={search}
                  onChange={handleSearch}
                  placeholder={translate('chatbotApp.paymentHistory.home.search')}
                />
                <Button className="input-group-addon">
                  <FontAwesomeIcon icon="search" />
                </Button>
                <Button type="reset" className="input-group-addon" onClick={clear}>
                  <FontAwesomeIcon icon="trash" />
                </Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <div className="table-responsive">
        {paymentHistoryList && paymentHistoryList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="chatbotApp.paymentHistory.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.paymentHistory.planStartDate">Plan Start Date</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.paymentHistory.planEndDate">Plan End Date</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.paymentHistory.usedSession">Used Session</Translate>
                </th>

                <th>
                  <Translate contentKey="chatbotApp.paymentHistory.company">Company</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.paymentHistory.plan">Plan</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {paymentHistoryList.map((paymentHistory, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${paymentHistory.id}`} color="link" size="sm">
                      {paymentHistory.id}
                    </Button>
                  </td>
                  <td>
                    {paymentHistory.planStartDate ? (
                      <TextFormat type="date" value={paymentHistory.planStartDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {paymentHistory.planEndDate ? (
                      <TextFormat type="date" value={paymentHistory.planEndDate} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{paymentHistory.usedSession}</td>

                  <td>
                    {paymentHistory.company ? <Link to={`company/${paymentHistory.company.id}`}>{paymentHistory.company.name}</Link> : ''}
                  </td>
                  <td>{paymentHistory.plan ? <Link to={`plan/${paymentHistory.plan.id}`}>{paymentHistory.plan.name}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${paymentHistory.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${paymentHistory.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${paymentHistory.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="chatbotApp.paymentHistory.home.notFound">No Payment Histories found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default PaymentHistory;
