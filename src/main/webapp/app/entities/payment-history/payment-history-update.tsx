import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ICompany } from 'app/shared/model/company.model';
import { getEntities as getCompanies } from 'app/entities/company/company.reducer';
import { IPlan } from 'app/shared/model/plan.model';
import { getEntities as getPlans } from 'app/entities/plan/plan.reducer';
import { getEntity, updateEntity, createEntity, reset } from './payment-history.reducer';
import { IPaymentHistory } from 'app/shared/model/payment-history.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const PaymentHistoryUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const companies = useAppSelector(state => state.company.entities);
  const plans = useAppSelector(state => state.plan.entities);
  const paymentHistoryEntity = useAppSelector(state => state.paymentHistory.entity);
  const loading = useAppSelector(state => state.paymentHistory.loading);
  const updating = useAppSelector(state => state.paymentHistory.updating);
  const updateSuccess = useAppSelector(state => state.paymentHistory.updateSuccess);

  const handleClose = () => {
    props.history.push('/payment-history');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getCompanies({}));
    dispatch(getPlans({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.planStartDate = convertDateTimeToServer(values.planStartDate);
    values.planEndDate = convertDateTimeToServer(values.planEndDate);
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.updatedAt = convertDateTimeToServer(values.updatedAt);

    const entity = {
      ...paymentHistoryEntity,
      ...values,
      company: companies.find(it => it.id.toString() === values.companyId.toString()),
      plan: plans.find(it => it.id.toString() === values.planId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          planStartDate: displayDefaultDateTime(),
          planEndDate: displayDefaultDateTime(),
          createdAt: displayDefaultDateTime(),
          updatedAt: displayDefaultDateTime(),
        }
      : {
          ...paymentHistoryEntity,
          planStartDate: convertDateTimeFromServer(paymentHistoryEntity.planStartDate),
          planEndDate: convertDateTimeFromServer(paymentHistoryEntity.planEndDate),
          createdAt: convertDateTimeFromServer(paymentHistoryEntity.createdAt),
          updatedAt: convertDateTimeFromServer(paymentHistoryEntity.updatedAt),
          companyId: paymentHistoryEntity?.company?.id,
          planId: paymentHistoryEntity?.plan?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="chatbotApp.paymentHistory.home.createOrEditLabel" data-cy="PaymentHistoryCreateUpdateHeading">
            <Translate contentKey="chatbotApp.paymentHistory.home.createOrEditLabel">Create or edit a PaymentHistory</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="payment-history-id"
                  label={translate('chatbotApp.paymentHistory.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('chatbotApp.paymentHistory.planStartDate')}
                id="payment-history-planStartDate"
                name="planStartDate"
                data-cy="planStartDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.paymentHistory.planEndDate')}
                id="payment-history-planEndDate"
                name="planEndDate"
                data-cy="planEndDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.paymentHistory.usedSession')}
                id="payment-history-usedSession"
                name="usedSession"
                data-cy="usedSession"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.paymentHistory.createdAt')}
                id="payment-history-createdAt"
                name="createdAt"
                data-cy="createdAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.paymentHistory.updatedAt')}
                id="payment-history-updatedAt"
                name="updatedAt"
                data-cy="updatedAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                id="payment-history-company"
                name="companyId"
                data-cy="company"
                label={translate('chatbotApp.paymentHistory.company')}
                type="select"
              >
                <option value="" key="0" />
                {companies
                  ? companies.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="payment-history-plan"
                name="planId"
                data-cy="plan"
                label={translate('chatbotApp.paymentHistory.plan')}
                type="select"
              >
                <option value="" key="0" />
                {plans
                  ? plans.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/payment-history" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default PaymentHistoryUpdate;
