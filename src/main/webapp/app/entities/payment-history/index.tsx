import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PaymentHistory from './payment-history';
import PaymentHistoryDetail from './payment-history-detail';
import PaymentHistoryUpdate from './payment-history-update';
import PaymentHistoryDeleteDialog from './payment-history-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PaymentHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PaymentHistoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PaymentHistoryDetail} />
      <ErrorBoundaryRoute path={match.url} component={PaymentHistory} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PaymentHistoryDeleteDialog} />
  </>
);

export default Routes;
