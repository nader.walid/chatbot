import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import AccessToken from './access-token';
import AppUser from './app-user';
import Role from './role';
import Company from './company';
import Plan from './plan';
import Product from './product';
import Category from './category';
import Channel from './channel';
import Customer from './customer';
import Complain from './complain';
import Partner from './partner';
import PaymentHistory from './payment-history';
import SettingOptions from './setting-options';
import CustomerInquiry from './customer-inquiry';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}access-token`} component={AccessToken} />
      <ErrorBoundaryRoute path={`${match.url}app-user`} component={AppUser} />
      <ErrorBoundaryRoute path={`${match.url}role`} component={Role} />
      <ErrorBoundaryRoute path={`${match.url}company`} component={Company} />
      <ErrorBoundaryRoute path={`${match.url}plan`} component={Plan} />
      <ErrorBoundaryRoute path={`${match.url}product`} component={Product} />
      <ErrorBoundaryRoute path={`${match.url}category`} component={Category} />
      <ErrorBoundaryRoute path={`${match.url}channel`} component={Channel} />
      <ErrorBoundaryRoute path={`${match.url}customer`} component={Customer} />
      <ErrorBoundaryRoute path={`${match.url}complain`} component={Complain} />
      <ErrorBoundaryRoute path={`${match.url}partner`} component={Partner} />
      <ErrorBoundaryRoute path={`${match.url}payment-history`} component={PaymentHistory} />
      <ErrorBoundaryRoute path={`${match.url}setting-options`} component={SettingOptions} />
      <ErrorBoundaryRoute path={`${match.url}customer-inquiry`} component={CustomerInquiry} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
