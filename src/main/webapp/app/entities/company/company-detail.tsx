import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './company.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CompanyDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const companyEntity = useAppSelector(state => state.company.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="companyDetailsHeading">
          <Translate contentKey="chatbotApp.company.detail.title">Company</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.company.id">Id</Translate>
            </span>
          </dt>
          <dd>{companyEntity.id}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="chatbotApp.company.email">Email</Translate>
            </span>
          </dt>
          <dd>{companyEntity.email}</dd>
          <dt>
            <span id="welcomeMessage">
              <Translate contentKey="chatbotApp.company.welcomeMessage">Welcome Message</Translate>
            </span>
          </dt>
          <dd>{companyEntity.welcomeMessage}</dd>
          <dt>
            <span id="website">
              <Translate contentKey="chatbotApp.company.website">Website</Translate>
            </span>
          </dt>
          <dd>{companyEntity.website}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="chatbotApp.company.name">Name</Translate>
            </span>
          </dt>
          <dd>{companyEntity.name}</dd>
          <dt>
            <span id="partnerText">
              <Translate contentKey="chatbotApp.company.partnerText">Partner Text</Translate>
            </span>
          </dt>
          <dd>{companyEntity.partnerText}</dd>
          <dt>
            <span id="productsText">
              <Translate contentKey="chatbotApp.company.productsText">Products Text</Translate>
            </span>
          </dt>
          <dd>{companyEntity.productsText}</dd>
          <dt>
            <span id="about">
              <Translate contentKey="chatbotApp.company.about">About</Translate>
            </span>
          </dt>
          <dd>{companyEntity.about}</dd>
          <dt>
            <span id="usedSessions">
              <Translate contentKey="chatbotApp.company.usedSessions">Used Sessions</Translate>
            </span>
          </dt>
          <dd>{companyEntity.usedSessions}</dd>
          <dt>
            <span id="planStartDate">
              <Translate contentKey="chatbotApp.company.planStartDate">Plan Start Date</Translate>
            </span>
          </dt>
          <dd>
            {companyEntity.planStartDate ? <TextFormat value={companyEntity.planStartDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="planEndDate">
              <Translate contentKey="chatbotApp.company.planEndDate">Plan End Date</Translate>
            </span>
          </dt>
          <dd>
            {companyEntity.planEndDate ? <TextFormat value={companyEntity.planEndDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.company.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>{companyEntity.createdAt ? <TextFormat value={companyEntity.createdAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.company.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>{companyEntity.updatedAt ? <TextFormat value={companyEntity.updatedAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="chatbotApp.company.category">Category</Translate>
          </dt>
          <dd>
            {companyEntity.categories
              ? companyEntity.categories.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.name}</a>
                    {companyEntity.categories && i === companyEntity.categories.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>
            <Translate contentKey="chatbotApp.company.settingOptions">Setting Options</Translate>
          </dt>
          <dd>
            {companyEntity.settingOptions
              ? companyEntity.settingOptions.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.name}</a>
                    {companyEntity.settingOptions && i === companyEntity.settingOptions.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>
            <Translate contentKey="chatbotApp.company.appUser">App User</Translate>
          </dt>
          <dd>{companyEntity.appUser ? companyEntity.appUser.name : ''}</dd>
          <dt>
            <Translate contentKey="chatbotApp.company.plan">Plan</Translate>
          </dt>
          <dd>{companyEntity.plan ? companyEntity.plan.name : ''}</dd>
        </dl>
        <Button tag={Link} to="/company" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/company/${companyEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default CompanyDetail;
