import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Input, InputGroup, FormGroup, Form, Col, Row, Table } from 'reactstrap';
import { Translate, translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { searchEntities, getEntities } from './company.reducer';
import { ICompany } from 'app/shared/model/company.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const Company = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState('');

  const companyList = useAppSelector(state => state.company.entities);
  const loading = useAppSelector(state => state.company.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const startSearching = e => {
    if (search) {
      dispatch(searchEntities({ query: search }));
    }
    e.preventDefault();
  };

  const clear = () => {
    setSearch('');
    dispatch(getEntities({}));
  };

  const handleSearch = event => setSearch(event.target.value);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="company-heading" data-cy="CompanyHeading">
        <Translate contentKey="chatbotApp.company.home.title">Companies</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="chatbotApp.company.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="chatbotApp.company.home.createLabel">Create new Company</Translate>
          </Link>
        </div>
      </h2>
      <Row>
        <Col sm="12">
          <Form onSubmit={startSearching}>
            <FormGroup>
              <InputGroup>
                <Input
                  type="text"
                  name="search"
                  defaultValue={search}
                  onChange={handleSearch}
                  placeholder={translate('chatbotApp.company.home.search')}
                />
                <Button className="input-group-addon">
                  <FontAwesomeIcon icon="search" />
                </Button>
                <Button type="reset" className="input-group-addon" onClick={clear}>
                  <FontAwesomeIcon icon="trash" />
                </Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <div className="table-responsive">
        {companyList && companyList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="chatbotApp.company.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.email">Email</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.welcomeMessage">Welcome Message</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.website">Website</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.name">Name</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.partnerText">Partner Text</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.productsText">Products Text</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.about">About</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.usedSessions">Used Sessions</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.planStartDate">Plan Start Date</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.planEndDate">Plan End Date</Translate>
                </th>

                <th>
                  <Translate contentKey="chatbotApp.company.category">Category</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.settingOptions">Setting Options</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.appUser">App User</Translate>
                </th>
                <th>
                  <Translate contentKey="chatbotApp.company.plan">Plan</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {companyList.map((company, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${company.id}`} color="link" size="sm">
                      {company.id}
                    </Button>
                  </td>
                  <td>{company.email}</td>
                  <td>{company.welcomeMessage}</td>
                  <td>{company.website}</td>
                  <td>{company.name}</td>
                  <td>{company.partnerText}</td>
                  <td>{company.productsText}</td>
                  <td>{company.about}</td>
                  <td>{company.usedSessions}</td>
                  <td>
                    {company.planStartDate ? <TextFormat type="date" value={company.planStartDate} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>{company.planEndDate ? <TextFormat type="date" value={company.planEndDate} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>
                    {company.categories
                      ? company.categories.map((val, j) => (
                          <span key={j}>
                            <Link to={`category/${val.id}`}>{val.name}</Link>
                            {j === company.categories.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td>
                    {company.settingOptions
                      ? company.settingOptions.map((val, j) => (
                          <span key={j}>
                            <Link to={`setting-options/${val.id}`}>{val.name}</Link>
                            {j === company.settingOptions.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td>{company.appUser ? <Link to={`app-user/${company.appUser.id}`}>{company.appUser.name}</Link> : ''}</td>
                  <td>{company.plan ? <Link to={`plan/${company.plan.id}`}>{company.plan.name}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${company.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${company.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${company.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="chatbotApp.company.home.notFound">No Companies found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Company;
