import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ICategory } from 'app/shared/model/category.model';
import { getEntities as getCategories } from 'app/entities/category/category.reducer';
import { ISettingOptions } from 'app/shared/model/setting-options.model';
import { getEntities as getSettingOptions } from 'app/entities/setting-options/setting-options.reducer';
import { IAppUser } from 'app/shared/model/app-user.model';
import { getEntities as getAppUsers } from 'app/entities/app-user/app-user.reducer';
import { IPlan } from 'app/shared/model/plan.model';
import { getEntities as getPlans } from 'app/entities/plan/plan.reducer';
import { IPartner } from 'app/shared/model/partner.model';
import { getEntities as getPartners } from 'app/entities/partner/partner.reducer';
import { getEntity, updateEntity, createEntity, reset } from './company.reducer';
import { ICompany } from 'app/shared/model/company.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CompanyUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const categories = useAppSelector(state => state.category.entities);
  const settingOptions = useAppSelector(state => state.settingOptions.entities);
  const appUsers = useAppSelector(state => state.appUser.entities);
  const plans = useAppSelector(state => state.plan.entities);
  const partners = useAppSelector(state => state.partner.entities);
  const companyEntity = useAppSelector(state => state.company.entity);
  const loading = useAppSelector(state => state.company.loading);
  const updating = useAppSelector(state => state.company.updating);
  const updateSuccess = useAppSelector(state => state.company.updateSuccess);

  const handleClose = () => {
    props.history.push('/company');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getCategories({}));
    dispatch(getSettingOptions({}));
    dispatch(getAppUsers({}));
    dispatch(getPlans({}));
    dispatch(getPartners({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.planStartDate = convertDateTimeToServer(values.planStartDate);
    values.planEndDate = convertDateTimeToServer(values.planEndDate);
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.updatedAt = convertDateTimeToServer(values.updatedAt);

    const entity = {
      ...companyEntity,
      ...values,
      categories: mapIdList(values.categories),
      settingOptions: mapIdList(values.settingOptions),
      appUser: appUsers.find(it => it.id.toString() === values.appUserId.toString()),
      plan: plans.find(it => it.id.toString() === values.planId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          planStartDate: displayDefaultDateTime(),
          planEndDate: displayDefaultDateTime(),
          createdAt: displayDefaultDateTime(),
          updatedAt: displayDefaultDateTime(),
        }
      : {
          ...companyEntity,
          planStartDate: convertDateTimeFromServer(companyEntity.planStartDate),
          planEndDate: convertDateTimeFromServer(companyEntity.planEndDate),
          createdAt: convertDateTimeFromServer(companyEntity.createdAt),
          updatedAt: convertDateTimeFromServer(companyEntity.updatedAt),
          categories: companyEntity?.categories?.map(e => e.id.toString()),
          settingOptions: companyEntity?.settingOptions?.map(e => e.id.toString()),
          appUserId: companyEntity?.appUser?.id,
          planId: companyEntity?.plan?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="chatbotApp.company.home.createOrEditLabel" data-cy="CompanyCreateUpdateHeading">
            <Translate contentKey="chatbotApp.company.home.createOrEditLabel">Create or edit a Company</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="company-id"
                  label={translate('chatbotApp.company.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('chatbotApp.company.email')}
                id="company-email"
                name="email"
                data-cy="email"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.company.welcomeMessage')}
                id="company-welcomeMessage"
                name="welcomeMessage"
                data-cy="welcomeMessage"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.company.website')}
                id="company-website"
                name="website"
                data-cy="website"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.company.name')}
                id="company-name"
                name="name"
                data-cy="name"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('chatbotApp.company.partnerText')}
                id="company-partnerText"
                name="partnerText"
                data-cy="partnerText"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.company.productsText')}
                id="company-productsText"
                name="productsText"
                data-cy="productsText"
                type="text"
              />
              <ValidatedField label={translate('chatbotApp.company.about')} id="company-about" name="about" data-cy="about" type="text" />
              <ValidatedField
                label={translate('chatbotApp.company.usedSessions')}
                id="company-usedSessions"
                name="usedSessions"
                data-cy="usedSessions"
                type="text"
              />
              <ValidatedField
                label={translate('chatbotApp.company.planStartDate')}
                id="company-planStartDate"
                name="planStartDate"
                data-cy="planStartDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.company.planEndDate')}
                id="company-planEndDate"
                name="planEndDate"
                data-cy="planEndDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.company.createdAt')}
                id="company-createdAt"
                name="createdAt"
                data-cy="createdAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.company.updatedAt')}
                id="company-updatedAt"
                name="updatedAt"
                data-cy="updatedAt"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('chatbotApp.company.category')}
                id="company-category"
                data-cy="category"
                type="select"
                multiple
                name="categories"
              >
                <option value="" key="0" />
                {categories
                  ? categories.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                label={translate('chatbotApp.company.settingOptions')}
                id="company-settingOptions"
                data-cy="settingOptions"
                type="select"
                multiple
                name="settingOptions"
              >
                <option value="" key="0" />
                {settingOptions
                  ? settingOptions.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="company-appUser"
                name="appUserId"
                data-cy="appUser"
                label={translate('chatbotApp.company.appUser')}
                type="select"
              >
                <option value="" key="0" />
                {appUsers
                  ? appUsers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="company-plan" name="planId" data-cy="plan" label={translate('chatbotApp.company.plan')} type="select">
                <option value="" key="0" />
                {plans
                  ? plans.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/company" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CompanyUpdate;
