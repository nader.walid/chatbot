import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SettingOptions from './setting-options';
import SettingOptionsDetail from './setting-options-detail';
import SettingOptionsUpdate from './setting-options-update';
import SettingOptionsDeleteDialog from './setting-options-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SettingOptionsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SettingOptionsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SettingOptionsDetail} />
      <ErrorBoundaryRoute path={match.url} component={SettingOptions} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={SettingOptionsDeleteDialog} />
  </>
);

export default Routes;
