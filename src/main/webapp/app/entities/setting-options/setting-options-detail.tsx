import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './setting-options.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const SettingOptionsDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const settingOptionsEntity = useAppSelector(state => state.settingOptions.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="settingOptionsDetailsHeading">
          <Translate contentKey="chatbotApp.settingOptions.detail.title">SettingOptions</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="chatbotApp.settingOptions.id">Id</Translate>
            </span>
          </dt>
          <dd>{settingOptionsEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="chatbotApp.settingOptions.name">Name</Translate>
            </span>
          </dt>
          <dd>{settingOptionsEntity.name}</dd>
          <dt>
            <span id="createdAt">
              <Translate contentKey="chatbotApp.settingOptions.createdAt">Created At</Translate>
            </span>
          </dt>
          <dd>
            {settingOptionsEntity.createdAt ? (
              <TextFormat value={settingOptionsEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="updatedAt">
              <Translate contentKey="chatbotApp.settingOptions.updatedAt">Updated At</Translate>
            </span>
          </dt>
          <dd>
            {settingOptionsEntity.updatedAt ? (
              <TextFormat value={settingOptionsEntity.updatedAt} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
        </dl>
        <Button tag={Link} to="/setting-options" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/setting-options/${settingOptionsEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default SettingOptionsDetail;
