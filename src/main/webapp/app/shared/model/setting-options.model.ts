import dayjs from 'dayjs';
import { ICompany } from 'app/shared/model/company.model';

export interface ISettingOptions {
  id?: number;
  name?: string;
  createdAt?: string | null;
  updatedAt?: string | null;
  companies?: ICompany[] | null;
}

export const defaultValue: Readonly<ISettingOptions> = {};
