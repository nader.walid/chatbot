import dayjs from 'dayjs';
import { IAppUser } from 'app/shared/model/app-user.model';

export interface IAccessToken {
  id?: number;
  appName?: string;
  token?: string | null;
  tokenCreation?: string | null;
  tokenExpire?: string | null;
  userAlies?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  appUser?: IAppUser | null;
}

export const defaultValue: Readonly<IAccessToken> = {};
