import dayjs from 'dayjs';
import { ICustomer } from 'app/shared/model/customer.model';
import { ICompany } from 'app/shared/model/company.model';

export interface ICustomerInquiry {
  id?: number;
  body?: string | null;
  status?: string;
  reason?: string;
  callDay?: string | null;
  callTime?: string | null;
  callPhone?: string | null;
  statusNotes?: string | null;
  nextActivity?: number | null;
  lastActivity?: number | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  customer?: ICustomer | null;
  company?: ICompany | null;
}

export const defaultValue: Readonly<ICustomerInquiry> = {};
