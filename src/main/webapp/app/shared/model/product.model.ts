import dayjs from 'dayjs';
import { ICategory } from 'app/shared/model/category.model';
import { ICompany } from 'app/shared/model/company.model';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string | null;
  imageContentType?: string | null;
  image?: string | null;
  nextActivity?: string | null;
  lastActivity?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  link?: string | null;
  price?: number | null;
  salePrice?: number | null;
  currency?: string | null;
  categories?: ICategory[] | null;
  company?: ICompany | null;
}

export const defaultValue: Readonly<IProduct> = {};
