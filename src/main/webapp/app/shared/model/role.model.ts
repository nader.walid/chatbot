import dayjs from 'dayjs';
import { IAppUser } from 'app/shared/model/app-user.model';

export interface IRole {
  id?: number;
  name?: string;
  displayName?: string;
  createdAt?: string | null;
  updatedAt?: string | null;
  appUsers?: IAppUser[] | null;
}

export const defaultValue: Readonly<IRole> = {};
