import dayjs from 'dayjs';
import { ICompany } from 'app/shared/model/company.model';
import { IPlan } from 'app/shared/model/plan.model';

export interface IPaymentHistory {
  id?: number;
  planStartDate?: string;
  planEndDate?: string;
  usedSession?: number;
  createdAt?: string | null;
  updatedAt?: string | null;
  company?: ICompany | null;
  plan?: IPlan | null;
}

export const defaultValue: Readonly<IPaymentHistory> = {};
