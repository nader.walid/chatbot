import dayjs from 'dayjs';
import { ICompany } from 'app/shared/model/company.model';

export interface IPartner {
  id?: number;
  imageContentType?: string | null;
  image?: string | null;
  nextActivity?: string | null;
  lastActivity?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  companies?: ICompany[] | null;
}

export const defaultValue: Readonly<IPartner> = {};
