import dayjs from 'dayjs';
import { ICustomer } from 'app/shared/model/customer.model';
import { ICompany } from 'app/shared/model/company.model';

export interface IComplain {
  id?: number;
  title?: string;
  data?: string | null;
  status?: string;
  callPhone?: string | null;
  salesNotes?: string | null;
  nextActivity?: string | null;
  lastActivity?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  customer?: ICustomer | null;
  company?: ICompany | null;
}

export const defaultValue: Readonly<IComplain> = {};
