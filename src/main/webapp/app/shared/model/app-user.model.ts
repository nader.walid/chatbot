import dayjs from 'dayjs';
import { IAccessToken } from 'app/shared/model/access-token.model';
import { ICompany } from 'app/shared/model/company.model';
import { IRole } from 'app/shared/model/role.model';

export interface IAppUser {
  id?: number;
  name?: string;
  email?: string;
  password?: string;
  avatarContentType?: string | null;
  avatar?: string | null;
  emailVerifiedAt?: string | null;
  rememberToken?: string | null;
  setting?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  accessToken?: IAccessToken | null;
  companies?: ICompany[] | null;
  roles?: IRole[] | null;
}

export const defaultValue: Readonly<IAppUser> = {};
