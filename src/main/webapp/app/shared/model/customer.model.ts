import dayjs from 'dayjs';
import { IComplain } from 'app/shared/model/complain.model';
import { ICustomerInquiry } from 'app/shared/model/customer-inquiry.model';
import { IChannel } from 'app/shared/model/channel.model';

export interface ICustomer {
  id?: number;
  name?: string;
  phone?: string | null;
  phone2?: string | null;
  phone3?: string | null;
  email?: string;
  company?: string | null;
  jobTitle?: string | null;
  nextActivity?: string | null;
  lastActivity?: string | null;
  salesNotes?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  complains?: IComplain[] | null;
  customerInquiries?: ICustomerInquiry[] | null;
  channel?: IChannel | null;
}

export const defaultValue: Readonly<ICustomer> = {};
