import dayjs from 'dayjs';
import { ICustomer } from 'app/shared/model/customer.model';

export interface IChannel {
  id?: number;
  name?: string;
  createdAt?: string | null;
  updatedAt?: string | null;
  customers?: ICustomer[] | null;
}

export const defaultValue: Readonly<IChannel> = {};
