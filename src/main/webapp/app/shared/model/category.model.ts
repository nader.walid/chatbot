import dayjs from 'dayjs';
import { IProduct } from 'app/shared/model/product.model';
import { ICompany } from 'app/shared/model/company.model';

export interface ICategory {
  id?: number;
  name?: string;
  slug?: string;
  order?: number;
  createdAt?: string | null;
  updatedAt?: string | null;
  imageContentType?: string | null;
  image?: string | null;
  products?: IProduct[] | null;
  companies?: ICompany[] | null;
}

export const defaultValue: Readonly<ICategory> = {};
