import dayjs from 'dayjs';
import { IProduct } from 'app/shared/model/product.model';
import { IComplain } from 'app/shared/model/complain.model';
import { IPaymentHistory } from 'app/shared/model/payment-history.model';
import { ICustomerInquiry } from 'app/shared/model/customer-inquiry.model';
import { ICategory } from 'app/shared/model/category.model';
import { ISettingOptions } from 'app/shared/model/setting-options.model';
import { IAppUser } from 'app/shared/model/app-user.model';
import { IPlan } from 'app/shared/model/plan.model';
import { IPartner } from 'app/shared/model/partner.model';

export interface ICompany {
  id?: number;
  email?: string;
  welcomeMessage?: string | null;
  website?: string | null;
  name?: string;
  partnerText?: string | null;
  productsText?: string | null;
  about?: string | null;
  usedSessions?: number | null;
  planStartDate?: string | null;
  planEndDate?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  products?: IProduct[] | null;
  complains?: IComplain[] | null;
  paymentHistories?: IPaymentHistory[] | null;
  customerInquiries?: ICustomerInquiry[] | null;
  categories?: ICategory[] | null;
  settingOptions?: ISettingOptions[] | null;
  appUser?: IAppUser | null;
  plan?: IPlan | null;
  partners?: IPartner[] | null;
}

export const defaultValue: Readonly<ICompany> = {};
