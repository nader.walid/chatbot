import dayjs from 'dayjs';
import { ICompany } from 'app/shared/model/company.model';
import { IPaymentHistory } from 'app/shared/model/payment-history.model';

export interface IPlan {
  id?: number;
  name?: string;
  maxUsers?: number;
  createdAt?: string | null;
  updatedAt?: string | null;
  companies?: ICompany[] | null;
  paymentHistories?: IPaymentHistory[] | null;
}

export const defaultValue: Readonly<IPlan> = {};
