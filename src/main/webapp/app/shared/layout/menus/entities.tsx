import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    data-cy="entity"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <>{/* to avoid warnings when empty */}</>
    <MenuItem icon="asterisk" to="/access-token">
      <Translate contentKey="global.menu.entities.accessToken" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/app-user">
      <Translate contentKey="global.menu.entities.appUser" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/role">
      <Translate contentKey="global.menu.entities.role" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/company">
      <Translate contentKey="global.menu.entities.company" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/plan">
      <Translate contentKey="global.menu.entities.plan" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/product">
      <Translate contentKey="global.menu.entities.product" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/category">
      <Translate contentKey="global.menu.entities.category" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/channel">
      <Translate contentKey="global.menu.entities.channel" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/customer">
      <Translate contentKey="global.menu.entities.customer" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/complain">
      <Translate contentKey="global.menu.entities.complain" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/partner">
      <Translate contentKey="global.menu.entities.partner" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/payment-history">
      <Translate contentKey="global.menu.entities.paymentHistory" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/setting-options">
      <Translate contentKey="global.menu.entities.settingOptions" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/customer-inquiry">
      <Translate contentKey="global.menu.entities.customerInquiry" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
