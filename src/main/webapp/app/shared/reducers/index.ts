import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import accessToken from 'app/entities/access-token/access-token.reducer';
// prettier-ignore
import appUser from 'app/entities/app-user/app-user.reducer';
// prettier-ignore
import role from 'app/entities/role/role.reducer';
// prettier-ignore
import company from 'app/entities/company/company.reducer';
// prettier-ignore
import plan from 'app/entities/plan/plan.reducer';
// prettier-ignore
import product from 'app/entities/product/product.reducer';
// prettier-ignore
import category from 'app/entities/category/category.reducer';
// prettier-ignore
import channel from 'app/entities/channel/channel.reducer';
// prettier-ignore
import customer from 'app/entities/customer/customer.reducer';
// prettier-ignore
import complain from 'app/entities/complain/complain.reducer';
// prettier-ignore
import partner from 'app/entities/partner/partner.reducer';
// prettier-ignore
import paymentHistory from 'app/entities/payment-history/payment-history.reducer';
// prettier-ignore
import settingOptions from 'app/entities/setting-options/setting-options.reducer';
// prettier-ignore
import customerInquiry from 'app/entities/customer-inquiry/customer-inquiry.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const rootReducer = {
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  accessToken,
  appUser,
  role,
  company,
  plan,
  product,
  category,
  channel,
  customer,
  complain,
  partner,
  paymentHistory,
  settingOptions,
  customerInquiry,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
};

export default rootReducer;
